import React from 'react';
import {render} from 'react-dom'
import App from './App';
import {Provider} from 'react-redux'
import {BrowserRouter, Router} from 'react-router-dom'
import {Route, Routes} from 'react-router'
import * as serviceWorker from './serviceWorker';
import {store} from "./store/ConfigureStore";
import './css/index.css';
import '../node_modules/rpg-awesome/css/rpg-awesome.min.css'
// import '../public/favicon.ico';
// import '../public/logo192.png';
// import '../public/logo512.png';

render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
