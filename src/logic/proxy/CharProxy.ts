import {CharHdModel, CharModel} from "../dao/CharModel";
import {dispatch} from "../../store/ConfigureStore";
import {OverviewReducerAction, SetFieldPayload} from "../../store/reducers/OverviewReducer";
import {proxyService} from "../ProxyService";

export class CharProxy {
    model: CharModel
    private context: CharProxyContext = {propagate: false}

    constructor(model: CharModel) {this.model = model;}

    set hpCurrent(value: number) {this.setField("hpCurrent", value, "hpInfo.")}
    get hpCurrent() {return this.model.hpCurrent}

    set hpMax(value: number) {this.setField("hpMax", value, "hpInfo.")}
    get hpMax() {return this.model.hpMax}

    set hpTmpCurrent(value: number) {this.setField("hpTmpCurrent", value,"hpInfo.")}
    get hpTmpCurrent() {return this.model.hpTmpCurrent}

    set ac(value: number) {this.setField("ac", value, "genericInfo.")}
    get ac() {return this.model.ac}

    set initiative(value: number) {this.setField("initiative", value, "genericInfo.")}
    get initiative() {return this.model.initiative}

    set speed(value: number) {this.setField("speed", value, "genericInfo.")}
    get speed() {return this.model.speed}

    set lvl(value: number) {this.setField("lvl", value, "genericInfo.")}
    get lvl() {return this.model.lvl}

    set exp(value: number) {this.setField("exp", value, "genericInfo.")}
    get exp() {return this.model.exp}

    get id() {return this.model.id}
    get name() {return this.model.name}
    get raceName() {return this.model.raceName}
    get playerName() {return this.model.playerName}
    get backgroundName() {return this.model.backgroundName}

    bind(context: CharProxyContext): CharProxy {this.context = {...this.context, ...context}; return this}

    private setField = (name: string, value: any, path: string = "") => {
        (this.context.propagate ? proxyService.call({
            proxy: "charDao",
            expr:`
        def c = target.findById(${this.model.id}L).get()
        c.${path}${name} = ${value}
        target.save(c)
        "OK"
        `
        }) : new Promise<any>(() => {})).then((res: any) => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload(name, value)
            })
        }).then(() => {
            this.context && this.context.onSet && this.context.onSet()
        })
    }

    async saveToBack(name: string, value: any, path: string = ""){
        return proxyService.call({
            proxy: "charDao",
            expr:
                `
                def c = target.findById(${this.model.id}L).get()
                c.${path}${name} = ${value}
                target.save(c)
                "OK"
                `
        });
    }

    async saveToRedux(name: string, value: any){
        try {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload(name, value)
            })
        } catch (e) {
            return Promise.reject(e)
        }
    }
}

interface CharProxyContext {
    propagate?: boolean;
    onSet?: () => void;
}
