import {DictEntry} from "./Common";
import {proxyService} from "../ProxyService";

class ItemDaoProxy {
    findById(id: Number): Promise<Item> {return proxyService.call({
        proxy: "itemRemoteProxyService",
        expr: `target.getItemTrans(${id}L)`
    })}
}

export interface Item {
    id: Number | undefined
    name: String | undefined
    descr: String | undefined
    value: Number | undefined
    weight: Number | undefined
    base: Boolean | undefined
    generic: Boolean | undefined
    concrete: Boolean | undefined
    catalogue: Boolean | undefined

    type: ItemType | undefined
    baseItem: Item | undefined

    weaponInfo: ItemWeaponInfo | undefined
    armorInfo: ItemArmorInfo | undefined
    magicInfo: ItemMagicInfo | undefined
}

interface ItemType {
    id: Number | undefined
    name: String | undefined
    page: Number | undefined
    source: DictEntry | undefined
    entries: any | undefined
}

interface ItemWeaponInfo {
    weapon: Boolean | undefined
    damage: String | undefined
    reload: Number | undefined
    rangeEffective: Number | undefined
    rangeMax: Number | undefined
    ammoId: Number | undefined
    category: DictEntry | undefined
    distance: DictEntry | undefined
    damageType: DictEntry | undefined
    props: any | undefined
}

interface ItemArmorInfo {
    armor: Boolean | undefined
    stealth: Boolean | undefined
    strength: Number | undefined
    ac: Number | undefined
}

interface ItemMagicInfo {
    magical: Boolean | undefined
    attunement: Boolean | undefined
    wondrous: Boolean | undefined
    curse: Boolean | undefined
    sentient: Boolean | undefined
    attuneCondition: String | undefined
    bonusBase: Number | undefined
    bonusAc: Number | undefined
    bonusAttack: Number | undefined
    bonusSave: Number | undefined
    bonusSpell: Number | undefined
    bonusDamage: Number | undefined

    scf: DictEntry | undefined
    rarity: DictEntry | undefined
    resistType: DictEntry | undefined
    recharge: DictEntry | undefined
    attachedSpells: any | undefined
    focusForClasses: any | undefined
}

const itemDaoProxy = new ItemDaoProxy()

export default itemDaoProxy