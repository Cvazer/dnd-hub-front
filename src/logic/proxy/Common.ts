export interface Source {
    id: Number | undefined
    name: String | undefined
}

export interface DictEntry {
    id: Number | undefined
    name: String | undefined
}