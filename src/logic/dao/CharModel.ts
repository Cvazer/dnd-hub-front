// noinspection DuplicatedCode
export class CharModel {
    id: number;
    name: string;
    hpCurrent: number;
    hpMax: number;
    hpTmpCurrent: number;
    hd: Array<CharHdModel>;

    ac: number;
    initiative: number;
    speed: number;

    lvl: number;
    exp: number;
    expNext: number;
    expPrev: number;

    classes: Array<CharClassModel>;
    attacks: Array<any>;
    stats: Array<any>;

    raceName: string;
    backgroundName: string;
    playerName: string;

    pp: number;
    gp: number;
    sp: number;
    cp: number;


    constructor(id: number, name: string, hpCurrent: number, hpMax: number,
                hpTmpCurrent: number, hd: Array<CharHdModel>, ac: number,
                initiative: number, speed: number, lvl: number, exp: number,
                expNext: number, expPrev: number, classes: Array<CharClassModel>,
                attacks: Array<any>, raceName: string, backgroundName: string,
                playerName: string, pp: number, gp: number, sp: number,
                cp: number, stats: Array<any>) {
        this.id = id;
        this.name = name
        this.hpCurrent = hpCurrent;
        this.hpMax = hpMax;
        this.hpTmpCurrent = hpTmpCurrent;
        this.hd = hd;
        this.ac = ac;
        this.initiative = initiative;
        this.speed = speed;
        this.lvl = lvl;
        this.exp = exp;
        this.expNext = expNext;
        this.expPrev = expPrev;
        this.classes = classes;
        this.attacks = attacks;
        this.raceName = raceName;
        this.backgroundName = backgroundName;
        this.playerName = playerName;
        this.pp = pp;
        this.gp = gp;
        this.sp = sp;
        this.cp = cp;
        this.stats = stats;
    }
}

export class CharClassModel {
    id: number;
    name: string;
    lvl: string;

    constructor(id: number, name: string, lvl: string) {
        this.id = id;
        this.name = name;
        this.lvl = lvl;
    }
}

export class CharHdModel {
    die: string;
    current: number;
    max: number;

    constructor(die: string, current: number, max: number) {
        this.die = die;
        this.current = current;
        this.max = max;
    }
}