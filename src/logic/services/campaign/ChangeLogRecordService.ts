import {proxyService} from "../../ProxyService";

const setText = (text: string, id: number) => {
    return proxyService.call({
        proxy: "gameEventLogDao",
        expr: `def rec = target.findById(${id}L).get()
        rec.setTextData(data)
        target.save(rec)
        service.notifyUpdate(${id}L)
        `,
        objects: [{name: "data", className: "java.lang.String", json: JSON.stringify(text)}],
        additionalProxies: {service: "gameEventService"}
    })
}

const changeLogRecordService = {
    setText: setText
}

export default changeLogRecordService