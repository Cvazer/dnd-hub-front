import {dispatch, store} from "../../../store/ConfigureStore";
import {proxyService} from "../../ProxyService";
import campaignReducerActionsFactory from "../../../store/reducers/CampaignReducer";

const getActiveChar = async () => {
    let {key} = store.getState().account
    let {id} = store.getState().campaign.campaign
    if (!key || !id) return undefined
    let res = await proxyService.call({
        proxy: "accountCampaignLinkDao",
        lang: "JS",
        // language=JavaScript
        expr: `
        var link = target.getByIdentity_AccountKeyAndIdentity_CampaignId("${key}", ${id})
        var activeChar = link && link.getActiveChar();
        activeChar && activeChar.getId()
        `
    });
    if (res !== "OK") { await dispatch(campaignReducerActionsFactory.setActiveCharId(+res)) }
    return res
}

const getCharsList = async () => {
    let {key} = store.getState().account
    let {id} = store.getState().campaign.campaign
    if (!key || !id) return undefined
    return await proxyService.call({
        proxy: "campaignService",
        lang: "JS",
        expr: `target.getCampaignCharsForAccount("${key}", ${id})`
    })
}

const getAllActiveChars = async () => {
    let {id} = store.getState().campaign.campaign
    if (!id) return
    return proxyService.call({
        proxy: "campaignService",
        expr: `target.getAllActiveChars(${id}L)`
    })
}

const setActiveChar = async (char: any) => {
    let {key} = store.getState().account
    let {id} = store.getState().campaign.campaign
    if (!key || !id) return
    char.id && dispatch(campaignReducerActionsFactory.setActiveCharId(+char.id))
    return await proxyService.call({
        proxy: "accountCampaignLinkDao",
        additionalProxies: {charDao: "charDao"},
        expr: `
        def link = target.getByIdentity_AccountKeyAndIdentity_CampaignId("${key}", ${id}L)
        link.setActiveChar(charDao.getOne(${char.id}L)) 
        target.save(link)
        "OK"
        `
    })
}

const charManagerService = {
    getActiveChar: getActiveChar,
    getCharsList: getCharsList,
    setActiveChar: setActiveChar,
    getAllActiveChars: getAllActiveChars
}

export default charManagerService