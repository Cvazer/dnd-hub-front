import ICUDateTime from "../../../model/ICUDateTime";
import {proxyService} from "../../ProxyService";
import {dispatch, store} from "../../../store/ConfigureStore";
import campaignReducerActionsFactory, {Campaign} from "../../../store/reducers/CampaignReducer";

const changeTime = async (date: ICUDateTime, src: string, direction: number) => {
    let {id} = store.getState().campaign.campaign
    if (!id || !src) return
    let res = await proxyService.call({
        proxy: "campaignDao",
        expr: `
        def campaign = target.findById(${id}L).get()
        campaign.setCurrentDateTime(date.${direction>=0?"add":"sub"}("${src}"))
        target.save(campaign)
        `,
        objects: [{
            name: "date",
            className: "com.gitlab.cvazer.model.icu.ICUDateTime",
            json: JSON.stringify(date)
        }]
    })
    await dispatch(campaignReducerActionsFactory.setCampaign(res as Campaign))
}

const campaignTimeManagerService = {
    changeTime: changeTime
}

export default campaignTimeManagerService