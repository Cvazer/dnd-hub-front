import {dispatch, store} from "../../../store/ConfigureStore";
import {proxyService} from "../../ProxyService";
import campaignReducerActionsFactory from "../../../store/reducers/CampaignReducer";

const logCharAction = async (text: string, cId?: number) => {
    let state = store.getState();
    let {key} = state.account
    let charId = cId || state.campaign.activeCharId
    let campaignId = state.campaign.campaign.id
    if (!key || !charId || !campaignId || !text || text === "") return

    return proxyService.call({
        proxy: "gameEventService",
        expr: `target.logCharAction(${charId}L, ${campaignId}L, text, "${key}")`,
        objects: [{
            name: "text",
            className: "java.lang.String",
            json: JSON.stringify(text)
        }]
    })
}

const logGenericEvent = async (text: string) : Promise<any> => {
    let state = store.getState();
    let campaignId = state.campaign.campaign.id
    let {key} = state.account

    if (!key || !campaignId || !text || text === "") return
    return await proxyService.call({
        proxy: "gameEventService",
        expr: `target.logGenericEvent(${campaignId}L, text, "${key}")`,
        objects: [{
            name: "text",
            className: "java.lang.String",
            json: JSON.stringify(text)
        }]
    })
}

const getRecords = async (page: number) => {
    let campaignId = store.getState().campaign.campaign.id
    if (!campaignId) return

    return await proxyService.call({
        proxy: "gameEventService",
        expr: `target.getRecords(${page}, ${15}, ${campaignId}L)`
    })
}

const update = async (obj: any) => {
    let list = store.getState().campaign.log
    if (!list.map((e: any) => e.id).includes(obj.id)) return
    return dispatch(campaignReducerActionsFactory.update(obj))
}

const append = async (rec: Array<any>) =>
    dispatch(campaignReducerActionsFactory.appendLog(rec))

const tail = async (rec: Array<any>) =>
    dispatch(campaignReducerActionsFactory.tailLog(rec))

const eventLogService = {
    logCharAction: logCharAction,
    getRecords: getRecords,
    append: append,
    tail: tail,
    logGenericEvent: logGenericEvent,
    update: update
}

export default eventLogService
