import {proxyService} from "../../ProxyService";

const load = (search: string, pageNum: number, pageSize: number) => {
    return proxyService.call({
        proxy: "glossaryService",
        expr: `target.findByName(${pageSize}, ${pageNum}, "${search}")`
    }).catch(() => {})
}

const glossaryService = {
    load: load
}

export default glossaryService