import {proxyService} from "../ProxyService";

class CharMagicService {
    getCharMagic(charId: number): Promise<any> {
        return proxyService.call({
            proxy: "charDao",
            expr: `target.findById(${charId}L).get().getMiscInfo().getMagic()`
        })
    }
    setCharMagic(charId: number, total: any, current: any, level: any): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: `target.setMagicSlots(${charId}L, ${level}, ${current}, ${total})`
        })
    }
    useSlot(charId: number, slots: any): Promise<any> {
        if (slots.current === 0) return Promise.resolve()
        return this.setCharMagic(charId, slots.total, (+slots.current)-1, slots.level)
    }
    restoreSlot(charId: number, slots: any): Promise<any> {
        if (slots.current === slots.total) return Promise.resolve()
        return this.setCharMagic(charId, slots.total, (+slots.current)+1, slots.level)
    }
    resetAll(charId: number): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: `target.resetAllMagicSlots(${charId}L)`
        })
    }
    deleteMagicSlots(charId: number, level: number): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: `target.deleteMagicSlots(${charId}L, ${level})`
        })
    }
    addSpell(charId: number, spell: any): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: `target.addSpell(${charId}L, spell)`,
            objects: [{
                name: "spell",
                className: "com.gitlab.cvazer.model.view.SpellView",
                json: JSON.stringify(spell)
            }]
        })
    }
    removeSpell(charId: number, spellId: number): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: `target.removeSpell(${charId}L, ${spellId}L)`
        })
    }
    setPrepared(charId: number, spellId: number, value: boolean): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: `target.setPrepared(${charId}L, ${spellId}L, ${value})`
        })
    }
}

export const charMagicService = new CharMagicService()