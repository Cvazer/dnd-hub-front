import {proxyService} from "../ProxyService";
import {dispatch} from "../../store/ConfigureStore";
import {OverviewReducerAction} from "../../store/reducers/OverviewReducer";

class InvService {
    async load(charId: number){
        let res = await proxyService.call({proxy: "inventoryService", expr: `target.getForChar(${charId}L)`})
        dispatch({
            type: OverviewReducerAction.ItemsOverviewSet,
            payload: res.inventories
        })
        return res.inventories
    }
}

const invService = new InvService()

export default invService