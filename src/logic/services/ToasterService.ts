import {AppToaster} from "../../App";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";

class ToasterService {
    error(text: string){
        AppToaster.show({
            message: text,
            icon: "warning-sign",
            intent: Intent.DANGER,
            timeout: 3000
        })
    }
    ok(text: string){
        AppToaster.show({
            message: text,
            icon: "tick",
            intent: Intent.SUCCESS,
            timeout: 3000
        })
    }
}

const toasterService = new ToasterService()

export default toasterService