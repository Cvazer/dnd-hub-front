import {proxyService} from "../ProxyService";
import toasterService from "./ToasterService";
import {store} from "../../store/ConfigureStore";

class CreateCharService {
    async fetchBaseCls() {
        return await proxyService.call({
            proxy: "clsDao",
            expr: `target.findAllCls()`
        })
    }

    async fetchBaseRaces() {
        return await proxyService.call({
            proxy: "raceDao",
            expr: `target.findAllBase()`
        })
    }

    async fetchBGs() {
        return await proxyService.call({
            proxy: "backgroundDao",
            expr: `target.findAll()`
        })
    }

    setGeneralData(
        data: any, stack: Array<any>, setStack: (a: Array<any>) => void,
        cls: any, subCls: any, race: any, subRace: any,
        bg: any, name: string, lvl: number, ready: () => void) {

        if (!cls) {
            toasterService.error("Нужно выбрать класс")
            return
        }
        if (!race) {
            toasterService.error("Нужно выбрать расу")
            return
        }
        if (!bg) {
            toasterService.error("Нужно выбрать предыстроию")
            return
        }
        data.cls = cls; data.subCls = subCls; data.race = race; data.subRace = subRace
        data.bg = bg; data.name = name; data.lvl = lvl
        ready()
    }

    setFeaturesData(data: any, stack: Array<any>, setStack: (a: Array<any>) => void,
                    selectedSkills: any[], selectedWeapons: any[],
                    selectedLangs: any[], selectedTools: any[],
                    selectedArmor: any[], ready: () => void) {
        data.skills = selectedSkills; data.weapons = selectedWeapons;
        data.langs = selectedLangs; data.tools = selectedTools;
        data.armor = selectedArmor;
        ready()
    }

    setMiscData(str: number, dex: number, con: number, int: number,
                wis: number, cha: number, data: any, hpMax: number, ready: () => void) {
        data.hpMax = hpMax
        data.stats = [
            {id: "STR", val: str},
            {id: "DEX", val: dex},
            {id: "CON", val: con},
            {id: "INT", val: int},
            {id: "WIS", val: wis},
            {id: "CHA", val: cha},
        ]
        ready()
    }

    async send(data: any) {
        let rq = {
            key: (store.getState().account as any).key,
            name: data.name,
            lvl: data.lvl,
            cls: data.cls.id,
            subCls: data.subCls && data.subCls.id,
            race: data.race.id,
            subRace: data.subRace && data.subRace.id,
            bg: data.bg.id,
            skills: (data.skills || []).map((e: any) => e.id),
            weapons: (data.weapons || []).map((e: any) => e.id),
            langs: (data.langs || []).map((e: any) => e.id),
            tools: (data.tools || []).map((e: any) => {return {id: e.id, cat: e.cat}}),
            armor: (data.armor || []).map((e: any) => e.id),
            hpMax: data.hpMax,
            stats: data.stats
        }
        return proxyService.call({
            proxy: "charService",
            expr: `target.create(rq)`,
            objects: [{
                name: "rq",
                className: "com.gitlab.cvazer.service.CharService$NewCharRq",
                json: JSON.stringify(rq)
            }]
        })
    }
}

export const createCharService = new CreateCharService()

export default createCharService