import {proxyService} from "../ProxyService";
import {dispatch, store} from '../../store/ConfigureStore'
import _ from "lodash";
import {DictReducerAction, DictSetAllPayload} from "../../store/reducers/DictReducer";

class AppService {
    loadDict() {
        let rq = {
            expr: `target`,
            lang: "GROOVY",
            proxy: "dict",
            params: {}
        }
        return proxyService.call(rq).then((res: any) => {
            dispatch({
                type: DictReducerAction.DictSetAll,
                payload: new DictSetAllPayload(res)
            })
        })
    }

    dict(dict: string) {
        return _.chain(store.getState().dict[dict]).keyBy('id').value()
    }

    dictValues(dict: string) {
        return Object.values(this.dict(dict))
    }

    text(text: string) {
        let res = text
            .replaceAll(/{@dice [0-9]*d[0-9]+}/g, str => {return (str.match(/{@dice ([0-9]*d[0-9]+)}/) || ["", ""])[1]})
            .replaceAll(/{@damage [0-9]+d[0-9]+}/g, str => {return (str.match(/{@damage ([0-9]d[0-9]+)}/) || ["", ""])[1]})
            .replaceAll(/{@skill [^}]+}/g, str => {return (str.match(/{@skill ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@item [^}]+}/g, str => {return (str.match(/{@item ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@spell [^}]+}/g, str => {return (str.match(/{@spell ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@i [^}]+}/g, str => {return (str.match(/{@i ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@action [^}]+}/g, str => {return (str.match(/{@action ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@sense [^}]+}/g, str => {return (str.match(/{@sense ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@note [^}]+}/g, str => {return (str.match(/{@note ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@condition [^}]+}/g, str => {return (str.match(/{@condition ([^}]+)}/) || ["", ""])[1]})
            .replaceAll(/{@5etools [^|]+\|[^}]*}/g, str => {return (str.match(/{@5etools ([^|]+)\|[^}]*}/) || ["", ""])[1]})
            .replaceAll(/{@item [^|]+\|[^}]*}/g, str => {return (str.match(/{@item ([^|]+)\|[^}]+}/) || ["", ""])[1]})
            .replaceAll(/{@filter [^}]+}/g, str => {return (str.match(/{@filter ([^|]+)\|[^}]+}/) || ["", ""])[1]})
            .replaceAll(/{@variantrule [^|]+\|[^ ]+.*}/g, (str: string, args: any[]) => {
                let res = str.match(/{@variantrule ([^|]+)\|[^ ]+(.*)}/) || ["", "", ""]
                return res[1]+res[2]
            })
        return res
    }
}

export const appService = new AppService()