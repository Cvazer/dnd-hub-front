import {proxyService} from "../../../ProxyService";
import charService from "../../CharService";
import traitService from "../../TraitService";

class AddProfDialogService {
    async add(profId: string, catId: string) {
        let charId = charService.proxy().id
        await proxyService.call({proxy: "profsService", expr: `target.addToChar(${charId}L, "${profId}", "${catId}")`})
        return charId
    }
}

const addProfDialogService = new AddProfDialogService()

export default addProfDialogService