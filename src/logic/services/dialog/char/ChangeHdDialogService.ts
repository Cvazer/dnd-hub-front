import charService from "../../CharService";
import {proxyService} from "../../../ProxyService";
import {validateIntPositive} from "../../../misc/ValidationUtils";
import dialogueService, {Dialog} from "../../DialogService";
import toasterService from "../../ToasterService";

class ChangeHdDialogService {
    checkMax(max: number, setter: (val: any) => void){
        return (val: any) => {let t = +val; if (val > max) {setter(max)} else {setter(val)}}
    }

    save(val: string, die: string, loading: (v: boolean) => void){
        loading(true)
        if (!validateIntPositive(val)) {
            loading(false)
            toasterService.error("Неверный формат значения")
            return
        }
        let char = charService.proxy()
        proxyService.call({proxy: "charDao", expr: `target.findById(${char.id}L).get().hpInfo.hd`})
            .then((hd: any) => {
                hd.find((e: any) => e.die === die).current = +val
                proxyService.call({proxy: "charDao", expr: `
                    import groovy.json.JsonSlurper
                    def jsonSlurper = new JsonSlurper()
                    def c = target.findById(${char.id}L).get()
                    c.hpInfo.hd = jsonSlurper.parseText(
                        '${JSON.stringify(hd)}'
                    )
                    target.save(c)
                    "OK"`
                }).then(() => {
                    char.saveToRedux("hd", hd).catch(() => {loading(false)})
                }).then(() => {
                    loading(false)
                    dialogueService.setState(Dialog.OverviewHd, {open: false, context: undefined})
                }).catch(() => {
                    loading(false)
                })
            })
    }
}

export const changeHdDialogService = new ChangeHdDialogService()