import {Money, unitsToMoney} from "../../../../Utils";
import charService from "../../CharService";
import {proxyService} from "../../../ProxyService";
import {dispatch} from "../../../../store/ConfigureStore";
import {OverviewReducerAction, SetFieldPayload} from "../../../../store/reducers/OverviewReducer";
import dialogueService, {Dialog} from "../../DialogService";

class ChangeMoneyDialogService {
    get(val: string, loading: (v: boolean) => void) {this.change(val, 1, loading)}
    spend(val: string, loading: (v: boolean) => void) {this.change(val, -1, loading)}
    change(val: string, direction: number, loading: (v: boolean) => void){
        loading(true)
        let char = charService.proxy()
        proxyService.call({proxy: "charService", expr: `target.changeMoney("${val}", ${direction}, ${char.id}L)`})
        .then((res: any) => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("pp", res.pp)
            })
            return res
        }).then((res: any) => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("gp", res.gp)
            })
            return res
        }).then((res: any) => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("sp", res.sp)
            })
            return res
        }).then((res: any) => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("cp", res.cp)
            })
            return res
        }).then(() => {
            loading(false)
            dialogueService.close(Dialog.OverviewMoney)
        }).catch(() => {
            loading(false)
        })
    }
    save(pp: number, gp: number, sp: number, cp: number, loading: (v: boolean) => void) {
        let char = charService.proxy()
        let money = new Money(+pp, +gp, +sp, +cp)
        money = unitsToMoney(money.units())
        let expr = `
        def c = target.findById(${char.id}L).get()
        c.moneyInfo.pp = ${money.platinum}
        c.moneyInfo.gp = ${money.gold}
        c.moneyInfo.sp = ${money.silver}
        c.moneyInfo.cp = ${money.copper}
        target.save(c)
        "OK"`
        proxyService.call({proxy: "charDao", expr: expr}).then(() => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("pp", money.platinum)
            })
        }).then(() => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("gp", money.gold)
            })
        }).then(() => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("sp", money.silver)
            })
        }).then(() => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("cp", money.copper)
            })
        }).then(() => {
            loading(false)
            dialogueService.close(Dialog.OverviewMoney)
        }).catch(() => {
            loading(false)
        })
    }
}

export const changeMoneyDialogService = new ChangeMoneyDialogService()