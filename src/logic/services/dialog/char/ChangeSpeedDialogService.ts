import charService from "../../CharService";
import toasterService from "../../ToasterService";
import {validateIntPositive} from "../../../misc/ValidationUtils";
import dialogueService, {Dialog} from "../../DialogService";

class ChangeSpedDialogService {
    save(val: string, loading: (val: boolean) => void) {
        loading(true)
        if (!validateIntPositive(val)) {
            toasterService.error("Скорость, неверный формат значения");
            loading(false); return;
        }
        let char = charService.proxy()
        char.saveToBack("speed", val, "genericInfo.").then(() => {
            char.saveToRedux("speed", val).then(() => {
                loading(false)
                dialogueService.close(Dialog.OverviewSpeed)
            }).catch(() => loading(false))
        }).catch(() => loading(false))
    }
}

export const changeSpeedDialogService = new ChangeSpedDialogService()