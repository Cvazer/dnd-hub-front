import dialogueService, {Dialog} from "../../DialogService";
import charService from "../../CharService";
import toasterService from "../../ToasterService";
import {proxyService} from "../../../ProxyService";
import {dispatch} from "../../../../store/ConfigureStore";
import {OverviewReducerAction, SetFieldPayload} from "../../../../store/reducers/OverviewReducer";
import {validateIntPositive} from "../../../misc/ValidationUtils";

class ChangeHpDialogService {
    damage(value: string, loading: (val: boolean) => void) {
        loading(true)
        if (!validateIntPositive(value)) {toasterService.error("Неверный формат поля 'Количество'"); return}
        let char = charService.proxy().bind({propagate: false})
        let dmg = +value - char.hpTmpCurrent
        let newTmp: number; if (dmg >= 0) {newTmp = 0} else {newTmp = Math.abs(dmg); dmg = 0}
        let newHp = char.hpCurrent - dmg
        if (newHp < 0) newHp = 0;

        let expr = `
            def c = target.findById(${char.id}L).get()
            c.hpInfo.hpTmpCurrent = ${newTmp}
            c.hpInfo.hpCurrent = ${newHp}
            target.save(c)
            "OK"
        `
        proxyService.call({proxy: "charDao", expr: expr}).then(() => {
            return char.saveToRedux("hpTmpCurrent", newTmp)
                .catch(() => {return Promise.reject()})
        }).then(() => {
            return char.saveToRedux("hpCurrent", newHp)
                .catch(() => {return Promise.reject()})
        }).then(() => {
            loading(false); dialogueService.close(Dialog.OverviewHp)
        }).catch(() => {loading(false)})
    }

    heal(value: string, loading: (val: boolean) => void) {
        loading(true)
        if (!validateIntPositive(value)) {toasterService.error("Неверный формат поля 'Количество'"); return}
        let char = charService.proxy().bind({propagate: false})
        let hp = char.hpCurrent + +value
        if (hp > char.hpMax) hp = char.hpMax;
        char.saveToBack("hpCurrent", hp, "hpInfo.").then(() => {
            char.saveToRedux("hpCurrent", hp).then(() => {
                loading(false)
                dialogueService.close(Dialog.OverviewHp)
            }).catch(() => loading(false))
        }).catch(() => loading(false))
    }

    save(current: string, max: string, tmp: string, loading: (val: boolean) => void) {
        loading(true)
        let error = false;
        if (!validateIntPositive(current)) {
            toasterService.error("Хиты, неверный формат поля 'Текущее значение'");
            error = true;
        }
        if (!validateIntPositive(max)) {
            toasterService.error("Хиты, неверный формат поля 'Максимальное значение'");
            error = true;
        }
        if (!validateIntPositive(tmp)) {
            toasterService.error("Временные хиты, неверный формат поля 'Текущее значение'");
            error = true;
        }
        if (error) return
        let char = charService.proxy().bind({propagate: false})
        let exp = `
        def c = target.findById(${char.id}L).get()
        c.hpInfo.hpCurrent = ${current}
        c.hpInfo.hpMax = ${max}
        c.hpInfo.hpTmpCurrent = ${tmp}
        target.save(c)
        "OK"
        `
        proxyService.call({proxy: "charDao", expr: exp}).then(() => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("hpCurrent", current)
            })
        }).then(() => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("hpMax", max)
            })
        }).then(() => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("hpTmpCurrent", tmp)
            })
        }).then(() => {
            loading(false)
            dialogueService.close(Dialog.OverviewHp)
        }).catch(() => {
            loading(false)
        })
    }
}

export const changeHpDialogService = new ChangeHpDialogService()