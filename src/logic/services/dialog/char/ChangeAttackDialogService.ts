import {validateInt, validateNotEmpty} from "../../../misc/ValidationUtils";
import toasterService from "../../ToasterService";
import charService from "../../CharService";
import {proxyService} from "../../../ProxyService";
import {dispatch} from "../../../../store/ConfigureStore";
import {OverviewReducerAction, SetFieldPayload} from "../../../../store/reducers/OverviewReducer";
import dialogueService, {Dialog as DG} from "../../DialogService";
import {uuid} from "../../../../Utils";

class ChangeAttackDialogService {
    save(attack: any, list: Array<any>,
         attackBonus: string, damageBase: string,
         damageBonus: string, name: string,
         loading: (val: boolean) => void) {
        let error = false;
        if (!validateInt(attackBonus)) {
            toasterService.error("Бонус попадания, неверный формат поля")
            error = true;
        }
        if (!validateNotEmpty(name)) {
            toasterService.error("Имя, поле обязательно к заполнению")
            error = true;
        }
        if (!validateNotEmpty(damageBase)) {
            toasterService.error("Урон, поле обязательно к заполнению")
            error = true;
        }
        if (!validateInt(damageBonus)) {
            toasterService.error("Бонусный урон, неверный формат поля")
            error = true;
        }
        if (error) return
        loading(true)
        let char = charService.proxy()
        attack.attackBonus = +attackBonus
        attack.damageBonus = +damageBonus
        attack.damageBase = damageBase
        attack.name = name
        if (!attack.id) {
            attack.id = uuid()
            list.push(attack)
        }
        let expr = `
        import groovy.json.JsonSlurper
        def jsonSlurper = new JsonSlurper()
        def c = target.findById(${char.id}L).get()
        c.miscInfo.attacks = jsonSlurper.parseText(
            '${JSON.stringify(list)}'
        )
        target.save(c).miscInfo.attacks`
        proxyService.call({proxy: "charDao", expr: expr}).then((res: any) => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("attacks", res)
            })
            loading(false)
            dialogueService.setState(DG.OverviewAttack, {open: false, context: undefined})
        }).catch(() => {loading(false)})
    }
}

export const changeAttackDialogService = new ChangeAttackDialogService()