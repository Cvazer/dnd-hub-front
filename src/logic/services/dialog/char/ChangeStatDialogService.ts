import charService from "../../CharService";
import dialogueService, {Dialog} from "../../DialogService";
import {proxyService} from "../../../ProxyService";
import {dispatch} from "../../../../store/ConfigureStore";
import {OverviewReducerAction, SetFieldPayload} from "../../../../store/reducers/OverviewReducer";
import toasterService from "../../ToasterService";
import {validateIntPositive} from "../../../misc/ValidationUtils";
import {log} from "util";

class ChangeStatDialogService {
    setSave(val: string, stat: string, loading: (val: boolean) => void) {
        loading(true)
        let char = charService.proxy()
        proxyService.call({
            proxy: "charService",
            expr: `target.setStatSave("${stat}", ${val}, ${char.id}L)`
        }).then((res: any) => {
            let s = char.model.stats.filter(e => e.id != res.id).concat([res]);
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("stats", s)
            })
        }).then(() => {loading(false)}).catch(() => loading(false))
    }
    setProf(val: string, skillId: string, loading: (val: boolean) => void) {
        loading(true)
        let char = charService.proxy()
        proxyService.call({
            proxy: "charService",
            expr: `target.setSkillProf("${skillId}", ${val}, ${char.id}L)`
        }).then((res: any) => {
            let s = char.model.stats.filter(e => e.id != res.id).concat([res]);
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("stats", s)
            })
        }).then(() => {loading(false)}).catch(() => loading(false))
    }
    setStat(val: string, stat: string, loading: (val: boolean) => void) {
        loading(true)
        if (!validateIntPositive(val)) {
            toasterService.error("Неверный формат значения");
            loading(false); return
        }
        let char = charService.proxy()
        proxyService.call({
            proxy: "charService",
            expr: `target.setStatValue("${stat}", ${+val}, ${char.id}L)`
        }).then((res: any) => {
            let s = char.model.stats.filter(e => e.id != res.id).concat([res]);
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("stats", s)
            })
        }).then(() => {
            loading(false)
            dialogueService.setState(Dialog.OverviewStat, {open: false, context: undefined})
        }).catch(() => loading(false));
    }
}

export const changeStatDialogService = new ChangeStatDialogService()