import charService from "../../CharService";
import toasterService from "../../ToasterService";
import {validateIntPositive} from "../../../misc/ValidationUtils";
import dialogueService, {Dialog} from "../../DialogService";

class ChangeAcDialogService {
    save(val: string, loading: (val: boolean) => void) {
        loading(true)
        if (!validateIntPositive(val)) {
            toasterService.error("Класс брони, неверный формат значения");
            loading(false); return
        }
        let char = charService.proxy()
        char.saveToBack("ac", val, "genericInfo.").then(() => {
            char.saveToRedux("ac", val).then(() => {
                loading(false)
                dialogueService.close(Dialog.OverviewAc)
            }).catch(() => loading(false))
        }).catch(() => loading(false))
    }
}

export const changeAcDialogService = new ChangeAcDialogService()