import charService from "../../CharService";
import toasterService from "../../ToasterService";
import {validateInt} from "../../../misc/ValidationUtils";
import dialogueService, {Dialog} from "../../DialogService";
import {proxyService} from "../../../ProxyService";
import {dispatch} from "../../../../store/ConfigureStore";
import {OverviewReducerAction, SetFieldPayload} from "../../../../store/reducers/OverviewReducer";

class ChangeExpDialogService {
    save(val: string, loading: (val: boolean) => void) {
        loading(true)
        if (!validateInt(val)) {
            toasterService.error("Опыт, неверный формат значения");
            loading(false); return
        }
        let char = charService.proxy()
        proxyService.call({proxy: "charService", expr: `target.addExp(${+val}L, ${char.id}L)`}).then((res: any) => {
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("exp", res.exp)
            })
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("expNext", res.next)
            })
            dispatch({
                type: OverviewReducerAction.CharOverviewSetField,
                payload: new SetFieldPayload("expPrev", res.prev)
            })
        }).then(() => {
            loading(false)
            dialogueService.close(Dialog.OverviewExp)
        }).catch(() => loading(false))
    }
}

export const changeExpDialogService = new ChangeExpDialogService()