import charService from "../../CharService";
import toasterService from "../../ToasterService";
import {validateIntPositive} from "../../../misc/ValidationUtils";
import dialogueService, {Dialog} from "../../DialogService";

class ChangeInitDialogService {
    save(val: string, loading: (val: boolean) => void) {
        loading(true)
        if (!validateIntPositive(val)) {
            toasterService.error("Инициатива, неверный формат значения");
            loading(false); return;
        }
        let char = charService.proxy()
        char.saveToBack("initiative", val, "genericInfo.").then(() => {
            char.saveToRedux("initiative", val).then(() => {
                loading(false)
                dialogueService.close(Dialog.OverviewInit)
            }).catch(() => loading(false))
        }).catch(() => loading(false))
    }
}

export const changeInitDialogService = new ChangeInitDialogService()