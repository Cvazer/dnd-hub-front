import {proxyService} from "../../../ProxyService";
import charService from "../../CharService";
import traitService from "../../TraitService";

class AddFeatureDialogService {
    async list() {return await proxyService.call({proxy: "featService", expr: `target.list()`})}
    async add(featId: number) {
        let charId = charService.proxy().id
        await proxyService.call({proxy: "featService", expr: `target.addFeature(${charId}L, ${featId}L)`})
        return charId
    }
}

export const addFeatureDialogService = new AddFeatureDialogService()