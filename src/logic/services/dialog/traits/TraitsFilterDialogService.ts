import {dispatch} from "../../../../store/ConfigureStore";
import {AccountReducerAction} from "../../../../store/reducers/AccountReducer";

class TraitsFilterDialogService {
    async set(origin: any, showHidden: boolean) {
        dispatch({
            type: AccountReducerAction.TraitsFilterSet,
            payload: {
                origin: origin,
                showHidden: showHidden || false
            }
        })
    }
}

export const traitsFilterDialogService: TraitsFilterDialogService = new TraitsFilterDialogService()