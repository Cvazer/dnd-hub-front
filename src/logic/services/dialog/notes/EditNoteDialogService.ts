import notesService from "../../NotesService";

class EditNoteDialogService {

    async addNote(name: string, text: string, uuid: string) {
        let data = notesService.getData()
        data.notes.push({uuid: uuid, text: text, name: name})
        return notesService.saveData(data)
    }

    async updateNote(name: string, text: string, uuid: string) {
        let data = notesService.getData()
        data.notes = data.notes.map((note: any) => {
            if (note.uuid !== uuid) return note
            note.text = text; note.name = name
            return note
        })
        return notesService.saveData(data)
    }

    addCounter(name: string, max: number, min: number, value: number, step: number, uuid: string) {
        let data = notesService.getData()
        data.counters.push({
            uuid: uuid,
            name: name,
            max: max,
            min: min,
            value: value,
            step: step
        })
        return notesService.saveData(data)
    }

    updateCounter(name: string, max: number, min: number, value: number, step: number, uuid: string) {
        let data = notesService.getData()
        data.counters = data.counters.map((counter: any) => {
            if (counter.uuid !== uuid) return counter
            counter.name = name
            counter.min = min
            counter.max = max
            counter.value = value
            counter.step = step
            return counter
        })
        return notesService.saveData(data)
    }
}

const editNoteDialogService = new EditNoteDialogService()

export default editNoteDialogService