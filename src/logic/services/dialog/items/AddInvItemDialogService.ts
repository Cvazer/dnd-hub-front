import {proxyService} from "../../../ProxyService";
import dialogueService, {Dialog, Dialog as DG} from "../../DialogService";
import toasterService from "../../ToasterService";
import charService from "../../CharService";

class AddInvItemDialogService {
    async getPage(key: string, page: number,
            loading: (v: boolean) => void){
        loading(true)
        let res = await proxyService.call({
            proxy: "itemService",
            expr: `target.findItemsByName(20, ${page}, "${key}")`
        });
        loading(false)
        return res;
    }

    async add(itemId: number, invId: number, loading: (v: boolean) => void) {
        loading(true)
        if (!itemId) {
            toasterService.error("Нужно выбрать предмет")
            loading(false)
            return
        }
        if (!invId || invId && +invId === -1) {
            toasterService.error("Нужно выбрать инвентарь")
            loading(false)
            return
        }

        let res = await proxyService.call({
            proxy: "inventoryService",
            expr: `target.getItemView(${itemId})`
        });

        loading(false)
        dialogueService.close(DG.InvItemAdd)

        dialogueService.setState(Dialog.InvItemChange, {
            open: true,
            context: res,
            invId: invId,
            del: false})
    }
}

export const addInvItemDialogService = new AddInvItemDialogService()