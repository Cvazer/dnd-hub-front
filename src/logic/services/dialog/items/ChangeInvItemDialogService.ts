import {proxyService} from "../../../ProxyService";
import dialogueService, {Dialog as DG} from "../../DialogService";
import invService from "../../InvService";
import charService from "../../CharService";

class ChangeInvItemDialogService {
    add(invId: number, itemId: number, count: string, com: string, alias: string, loading: (v: boolean) => void){
        let comment = com && com !== "" ? com : undefined
        let al = alias && alias !== "" ? alias : undefined
        loading(true)
        proxyService.call({
            proxy: "inventoryService",
            expr: `target.addItem(${itemId}L, ${invId}L, ${+count}, ${comment || "null"}, ${al || "null"})`
        }).then(() => {
            invService.load(charService.proxy().id).catch(() => loading(false))
        }).then(() => {
            loading(false)
            dialogueService.setState(DG.InvItemChange, {
                open: false,
                context: undefined,
                invId: undefined,
                del: undefined
            })
        }).catch(() => {loading(false)})
    }

    // fix this shit
    set(id: number, count: string, com: string, alias: string, loading: (v: boolean) => void){
        let comment = com && com !== "" ? com : undefined
        let al = alias && alias !== "" ? alias : undefined
        loading(true)
        proxyService.call({
            proxy: "inventoryService",
            expr: `target.setItem(${id}L, ${+count}, ${comment || "null"}, ${al || "null"})`
        }).then(() => {
            invService.load(charService.proxy().id).catch(() => loading(false))
        }).then(() => {
            loading(false)
            dialogueService.setState(DG.InvItemChange, {
                open: false,
                context: undefined,
                invId: undefined,
                del: undefined
            })
        }).catch(() => {loading(false)})
    }

    remove(id: number, invId: number, loading: (v: boolean) => void) {
        loading(true)
        proxyService.call({
            proxy: "inventoryService",
            expr: `target.deleteItem(${id}L, ${invId}L)`
        }).then(() => {
            invService.load(charService.proxy().id).catch(() => loading(false))
        }).then(() => {
            loading(false)
            dialogueService.setState(DG.InvItemChange, {
                open: false,
                context: undefined,
                invId: undefined,
                del: undefined
            })
        }).catch(() => {loading(false)})
    }
}

export const changeInvItemDialogService = new ChangeInvItemDialogService()