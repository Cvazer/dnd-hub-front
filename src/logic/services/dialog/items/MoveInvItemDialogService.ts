import dialogueService, {Dialog as DG} from "../../DialogService";
import toasterService from "../../ToasterService";
import {proxyService} from "../../../ProxyService";
import {dispatch, store} from "../../../../store/ConfigureStore";
import {OverviewReducerAction} from "../../../../store/reducers/OverviewReducer";
import invService from "../../InvService";
import charService from "../../CharService";

class MoveInvItemDialogService {
    async move(id: number, targetInvId: number, loading: (v: boolean) => void) {
        loading(true)
        if (!targetInvId || targetInvId && +targetInvId === -1) {
            toasterService.error("Нужно выбрать инвентарь")
            loading(false)
            return
        }

        proxyService.call({
            proxy: "inventoryService",
            expr: `target.moveItem(${id}L, ${targetInvId}L)`
        }).then(() => {
            invService.load(charService.proxy().id).catch(() => loading(false))
        }).then(() => {
            dialogueService.setState(DG.InvItemMove, {open: false, context: undefined})
            loading(false)
        }).catch(() => loading(false));
    }
}

export const moveInvItemDialogService = new MoveInvItemDialogService()