import {proxyService} from "../ProxyService";
import {dispatch, store} from "../../store/ConfigureStore";
import campaignReducerActionsFactory from "../../store/reducers/CampaignReducer";
import {Campaign} from "../../store/reducers/CampaignReducer";
import ICUDateTime from "../../model/ICUDateTime";

const list = (key: string) => proxyService.call({proxy: "campaignService", expr: `target.list("${key}")`})

const getActiveCharId = async (campaignId: number, key: string) => {
    await setActiveCharId(await proxyService.call({
        proxy: "accountCampaignLinkDao",
        expr: `
        def rq = target.getByIdentity_AccountKeyAndIdentity_CampaignId("${key}", ${campaignId}L)
        def res = "NONE"
        if (rq != null && rq.getActiveChar() != null) {res = rq.getActiveChar().getId()}
        res
        `
    }))
}

const setCampaign = async (c?: Campaign) => dispatch(campaignReducerActionsFactory.setCampaign(c))
const setActiveCharId = async (id: any) => id === "NONE"
    ? dispatch(campaignReducerActionsFactory.setActiveCharId(undefined))
    : dispatch(campaignReducerActionsFactory.setActiveCharId(+id))

const setCurrentDate = async (date: ICUDateTime, id: number) => {
    if (!date) return
    let res = await proxyService.call({
        proxy: "campaignDao",
        expr: `
        def campaign = target.findById(${id}L).get()
        campaign.setCurrentDateTime(date)
        target.save(campaign)
        `,
        objects: [
            {
                name: "date",
                className: "com.gitlab.cvazer.model.icu.ICUDateTime",
                json: JSON.stringify(date)
            }
        ]
    })
    await setCampaign(res)
}

const setRole = async (id: number) => {
    let key = store.getState().account.key
    if (!key) return
    dispatch(campaignReducerActionsFactory.setRole(await proxyService.call({
        proxy: "campaignDao",
        expr: `target.getRoleForKeyAndCampaign("${key}", ${id}L)`
    })))
}

const getCampaign = async (id: number) => {
    await setRole(id)
    await setCampaign(await proxyService.call({
        proxy: "campaignDao",
        expr: `target.findById(${id}L).get()`
    }))
}

const clear = async () => {dispatch(campaignReducerActionsFactory.setCampaign(undefined))}

const campaignService = {
    list: list,
    getActiveCharId: getActiveCharId,
    getCampaign: getCampaign,
    setCampaign: setCampaign,
    setActiveCharId: setActiveCharId,
    setCurrentDate: setCurrentDate,
    clear: clear
}

export default campaignService