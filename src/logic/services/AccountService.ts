import {proxyService} from "../ProxyService";
import {dispatch} from '../../store/ConfigureStore'
import accountReducerActionFactory, {AccountReducerAction, AccountSetPayload} from "../../store/reducers/AccountReducer";

const setAccount = async (key: string) => {
    if (!key) return
    let rq = {
        expr: `target.getAccount("${key}")`,
        proxy: "accountService",
    }
    return proxyService.call(rq).then((res: any) => {
        localStorage.setItem("dnd-hub:key", key)
        dispatch(accountReducerActionFactory.accountSet(key, res.name, res.role))
    })
}

const setSpacer = async (spacer: number) => {
    localStorage.setItem("dnd-hub:spacer", spacer+"")
    dispatch({
        type: AccountReducerAction.SpacerSet,
        payload: spacer
    })
}

const accountService = {
    setAccount: setAccount,
    setSpacer: setSpacer
}

export default accountService;