import {proxyService} from "../../ProxyService";
import toasterService from "../ToasterService";

class AdminAccountsViewService {
    getAll(): Promise<any> {
        return proxyService.call({
            proxy: "accountService",
            expr: `target.getAll()`
        })
    }

    getChars(): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: "target.getAllCard()"
        })
    }

    deleteAccount(key: String) {
        return proxyService.call({
            proxy: "accountService",
            expr: `target.delete("${key}")`
        })
    }

    save(rq: SaveRq) {
        let validate = (rq: SaveRq) => {
            if (!rq.key || !rq.name || !rq.role || !rq.chars) return false;
            // maybe more checks later
            // noinspection RedundantIfStatementJS
            if (!["user", "admin"].includes(rq.role)) return false;
            return true;
        }
        if (!validate(rq)) {
            toasterService.error("Invalid fields");
            return Promise.reject("Invalid fields")
        } else {
            return proxyService.call({
                proxy: "accountService",
                expr: "target.save(dto)",
                objects: [{
                        name: "dto",
                        className: "com.gitlab.cvazer.service.AccountService$AccountSaveDto",
                        json: JSON.stringify(rq)
                    }]
            })
        }
    }
}

interface SaveRq {
    key: string,
    name: string,
    role: string,
    chars: Array<number>
}

const adminAccountsViewService = new AdminAccountsViewService()

export default adminAccountsViewService