import {proxyService} from "../../../../ProxyService";

class ResolveResourceConflictDialogService {
    resolve(
        action: String, problem: any, resolverName: string,
        newData: any, newGeneric: any,
        persistedData: any, persistedGeneric: any,
        mergedData: any, mergedGeneric: any) {
        return proxyService.call({
            proxy: "scrappingService",
            expr: `target.resolve("${resolverName}", resolution)`,
            objects: [{
                name: "resolution",
                className: "com.gitlab.cvazer.system.scrapping.model.ResourceConflictProblemResolution",
                json: JSON.stringify({
                    problemId: problem.id,
                    type: "RESOURCE_CONFLICT",
                    resolution: action,
                    newRes: {...newGeneric, data: JSON.stringify(newData)},
                    persisted: {...persistedGeneric, data: JSON.stringify(persistedData)},
                    consensus: {...mergedGeneric, data: JSON.stringify(mergedData)},
                })
            }]
        })
    }
}

export const resolveResourceConflictDialogService = new ResolveResourceConflictDialogService()