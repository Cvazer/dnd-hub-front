import {proxyService} from "../../ProxyService";

const online = () => {
    return proxyService.call({
        proxy: "userService",
        expr: `target.getOnlineUsers()`
    })
}

const userService = {
    online: online
}

export default userService