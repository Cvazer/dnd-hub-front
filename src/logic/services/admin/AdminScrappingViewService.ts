import {proxyService} from "../../ProxyService";

class AdminScrappingViewService {
    listScrappers(): Promise<String[]> {
        return proxyService.call({
            proxy: "scrappingService",
            expr: `target.listScrappers()`
        })
    }

    status(scrapper: string) {
        return proxyService.call({
            proxy: "scrappingService",
            expr: `target.status("${scrapper}")`
        })
    }

    startScrapping(scrapper: string) {
        return proxyService.call({
            proxy: "scrappingService",
            expr: `target.startScrapping("${scrapper}")`
        })
    }

    reset(scrapper: string) {
        return proxyService.call({
            proxy: "scrappingService",
            expr: `target.reset("${scrapper}")`
        })
    }
}

export const adminScrappingViewService = new AdminScrappingViewService()
