import {proxyService} from "../../ProxyService";
import toasterService from "../ToasterService";

class AdminCampaignsViewService {
    getAll(): Promise<any> {
        return proxyService.call({
            proxy: "campaignService",
            expr: `target.getAll()`
        })
    }

    getChars(): Promise<any> {
        return proxyService.call({
            proxy: "charService",
            expr: "target.getAllCard()"
        })
    }

    // deleteAccount(key: String) {
    //     return proxyService.call({
    //         proxy: "accountService",
    //         expr: `target.delete("${key}")`
    //     })
    // }
    //
    save(campaign: any) {
        let validate = (rq: any) => {
            if ((!rq.name || rq.name === "") || !rq.accounts || !rq.chars || !rq.currentDateTime) return false;
            // maybe more checks later
            // noinspection RedundantIfStatementJS
            return true;
        }
        if (!validate(campaign)) {
            toasterService.error("Invalid fields");
            return Promise.reject("Invalid fields")
        } else {
            return proxyService.call({
                proxy: "campaignService",
                expr: "target.save(dto)",
                objects: [{
                        name: "dto",
                        className: "com.gitlab.cvazer.service.CampaignService$CampaignSaveDto",
                        json: JSON.stringify(campaign)
                    }]
            })
        }
    }
}

// interface SaveRq {
//     key: string,
//     name: string,
//     role: string,
//     chars: Array<number>
// }

const adminCampaignsViewService = new AdminCampaignsViewService()

export default adminCampaignsViewService