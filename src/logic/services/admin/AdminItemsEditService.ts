import itemDaoProxy, {Item} from "../../proxy/ItemDaoProxy";
import {validateIntPositive} from "../../misc/ValidationUtils";
import toasterService from "../ToasterService";
import {dict, dictValues} from "../../../App";
import {CallServiceResponse, proxyService} from "../../ProxyService";
import {host, onError, proto} from "../../config/BackendConfig";
import axios from "axios";

class AdminItemsEditService {
    setData(data: any, setData: (d: any) => void, key: string, value: any) {setData({...data, [key]: value === "" ? undefined : value})}
    async getItemFromView(item: any): Promise<Item> {return itemDaoProxy.findById(item.id)}

    setBase(id: string, data: any, setData: (d: any) => void){
        if (!validateIntPositive(id)) {
            toasterService.error("Base ID должен быть позитивным интом")
            return
        }
        itemDaoProxy.findById(+id).then((res: Item) => {
            setData({...data, baseItem: res})
        })
    }

    setBool(data: any, setData: (d: any) => void, name: string) {
        setData({...data, [name]: !data[name]})
    }

    setInfoDictVal(data: any, setData: (d: any) => void, name: string, val: any, info: string, dictN: string){
        if (val === "") {
            delete data[info][name]
        } else {
            data[info][name] = dict(dictN)[val]
        }
        setData(data)
    }

    setInfoData(data: any, setData: (d: any) => void, name: string, val: any, info: string){
        if (val === "") {
            delete data[info][name]
        } else {
            data[info][name] = val
        }
        setData(data)
    }

    getDictSet(name: String): any {
        return dictValues(name).map(it => {
            return {label: it.name.capitalize(), value: it.id}
        }).concat([{label: "Нет", value: ""}])
    }

    save(data: any, loading: (b: boolean) => void) {
        loading(true)
        return proxyService.call({
            proxy: "itemRemoteProxyService",
            expr: `target.save(item)`,
            objects: [{
                name: "item",
                className: "com.gitlab.cvazer.dao.entity.ItemEntity",
                json: JSON.stringify(data)
            }]
        }).then((res: any) => {loading(false); return res})
            .catch(() => loading(false))
    }

    delete(id: any, setItem: any) {
        return proxyService.call({proxy: "itemRemoteProxyService", expr: `target.delItemByIdTrans(${+id}L)`})
            .then(() => setItem(undefined))
    }
}


export const adminItemsEditService = new AdminItemsEditService()