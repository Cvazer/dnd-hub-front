import itemDaoProxy, {Item} from "../../proxy/ItemDaoProxy";
import {proxyService} from "../../ProxyService";

class AdminItemsViewService {
    getItemFromView(item: any): Promise<Item> {return itemDaoProxy.findById(item.id)}
    getNew(): Promise<Item> {
        return proxyService.call({
            proxy: "itemRemoteProxyService",
            expr: `target.getBlankItemTrans()`
        })
    }
}


export const adminItemsViewService = new AdminItemsViewService()