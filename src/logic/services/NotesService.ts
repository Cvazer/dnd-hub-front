import {proxyService} from "../ProxyService";
import {dispatch, store} from "../../store/ConfigureStore";
import {OverviewReducerAction} from "../../store/reducers/OverviewReducer";
import charService from "./CharService";
import {DialogReducerAction, SetStatePayload} from "../../store/reducers/DialogReducer";

class NotesService {
    async load() {
        let charId = charService.proxy().id
        let res = await proxyService.call({proxy: "charDao", expr: `target.getOne(${charId}L).miscInfo.notes`})
        dispatch({
            type: OverviewReducerAction.NotesSet,
            payload: res
        })
    }

    getData() {
        let data = {...((store.getState().overview as any).notes || {})}
        data.notes = data.notes || []
        data.counters = data.counters || []
        data.lists = data.lists || []
        return data
    }

    async saveData(data: any) {
        let id = charService.proxy().id
        await proxyService.call({
            proxy: "charService",
            expr: `target.setNotes(rq, ${id}L)`,
            objects: [{
                name: "rq",
                className: "com.gitlab.cvazer.model.character.CharNotes",
                json: JSON.stringify(data)
            }]
        })
        dispatch({
            type: OverviewReducerAction.NotesSet,
            payload: data
        })
    }

    async setCounter(value: number, uuid: string) {
        let data = this.getData()
        data.counters = data.counters.map((counter: any) => {
            if (counter.uuid !== uuid) return counter
            counter.value = value; return counter
        })
        return notesService.saveData(data)
    }

    async deleteNote(note: any) {
        let data = this.getData()
        data.notes = data.notes.filter((n: any) => n.uuid !== note.uuid)
        return notesService.saveData(data)
    }

    async deleteCounter(counter: any) {
        let data = this.getData();
        data.counters = data.counters.filter((c: any) => c.uuid !== counter.uuid)
        return notesService.saveData(data)
    }
}

const notesService = new NotesService()

export default notesService