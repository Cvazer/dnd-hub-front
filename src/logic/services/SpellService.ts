import {proxyService} from "../ProxyService";

class SpellService {
    getAllSpellTags(): Promise<any> {
        return proxyService.call({
            proxy: "spellDao",
            expr: `target.getAllSpellTags()`
        })
    }
    findPage(query: string, size: number, page: number): Promise<any> {
        return proxyService.call({
            proxy: "spellService",
            expr: `target.findPage("${query}", ${size}, ${page})`
        })
    }
}

export const spellService = new SpellService()