import {proxyService} from "../ProxyService";
import {dispatch} from "../../store/ConfigureStore";
import {OverviewReducerAction} from "../../store/reducers/OverviewReducer";

class ProfsService {
    async load(charId: number) {
        let res = await proxyService.call({proxy: "profsService", expr: `target.list(${charId}L)`})
        dispatch({
            type: OverviewReducerAction.ProfsOverviewSet,
            payload: res
        })
        return res
    }

    async delete(charId: number, profId: string, catId: string){
        await proxyService
            .call({proxy: "profsService", expr: `target.deleteFromChar(${charId}L, "${profId}", "${catId}")`})
        return profsService.load(charId)
    }

    async all() {return proxyService.call({proxy: "profsService", expr: `target.all()`})}
}

const profsService = new ProfsService()

export default profsService