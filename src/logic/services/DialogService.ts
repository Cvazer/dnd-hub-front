import {dispatch, store} from "../../store/ConfigureStore";
import {DialogReducerAction, SetStatePayload} from "../../store/reducers/DialogReducer";

class DialogService {
    setState(dialog: Dialog, state: any) {
        let st = {...store.getState().dialog[dialog], ...state}
        dispatch({
            type: DialogReducerAction.SetDialogState,
            payload: new SetStatePayload(dialog.toString(), st)
        })
    }
    close(dialog: Dialog) {this.setState(dialog, {open: false})}
    open(dialog: Dialog) {this.openState(dialog, undefined)}
    openState(dialog: Dialog, context: any) {this.setState(dialog, {open: true, context: context})}
}

export enum Dialog {
    OverviewHp = "overview/hp",
    OverviewAc = "overview/ac",
    OverviewInit = "overview/init",
    OverviewSpeed = "overview/speed",
    OverviewHd = "overview/hd",
    OverviewExp = "overview/exp",
    OverviewMoney = "overview/money",
    OverviewAttack = "overview/attack",
    OverviewStat = "overview/stat",
    OverviewNoteView = "overview/note/view",
    OverviewNoteEdit = "overview/note/edit",
    OverviewCounterEdit = "overview/counter/edit",
    InvItemChange = "inventory/item/change",
    InvItemAdd = "inventory/item/add",
    InvItemMove = "inventory/item/move",
    TraitsFilter = "traits/filter",
    TraitsAddFeature = "traits/feature/add",
    CampaignChangeRecord = "campaign/log/record/change",
    ProfAdd = "prof/add"
}

const dialogueService = new DialogService()

export default dialogueService