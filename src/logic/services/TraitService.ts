import {proxyService} from "../ProxyService";
import {dispatch} from "../../store/ConfigureStore";
import {OverviewReducerAction} from "../../store/reducers/OverviewReducer";

class TraitService {
    async load(charId: number){
        let res = await proxyService.call({proxy: "traitsService", expr: `target.getCharTraits(${charId}L)`})
        dispatch({
            type: OverviewReducerAction.TraitsOverviewSet,
            payload: res
        })
        return res
    }

    get(traitId: number){
        return proxyService.call({proxy: "traitDao", expr: `target.findById(${traitId}L).get()`})
    }

    hide(charId: number, traitId: number){
        return proxyService.call({proxy: "traitsService", expr: `target.hideTrait(${charId}L, ${traitId}L)`})
    }

    show(charId: number, traitId: number){
        return proxyService.call({proxy: "traitsService", expr: `target.showTrait(${charId}L, ${traitId}L)`})
    }
}

const traitService = new TraitService()

export default traitService