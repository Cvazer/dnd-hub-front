import {proxyService} from "../ProxyService";
import {dispatch, store} from "../../store/ConfigureStore";
import {CharModel} from "../dao/CharModel";
import {CharProxy} from "../proxy/CharProxy";
import {OverviewReducerAction} from "../../store/reducers/OverviewReducer";

const load = async (id: number) => {
    if (id === null) return Promise.reject()
    let rq = {expr: `target.getView(${id}L)`, lang: "GROOVY", proxy: "charService", params: {}}
    await proxyService.call(rq)
        .then((res: any) => {return Object.setPrototypeOf({...res}, CharModel.prototype)})
        .then((res: CharModel) => {dispatch({type: OverviewReducerAction.CharOverviewSet, payload: res})})
        .then((res: CharModel) => {return new CharProxy(res)})
}

const clear = () => {dispatch({type: OverviewReducerAction.CharOverviewSet, payload: undefined})}

const proxy = (): CharProxy => {return new CharProxy(((store.getState().overview as any).char as CharModel))}

const getAllCardsForAccount = async (key: string) => {
    if (key === null) return
    let idRq = {expr: `target.getCharIds("${key}")`, lang: "GROOVY", proxy: "accountService", params: {}}
    let ids = (await proxyService.call(idRq))
    let cards: any = []
    for (let i = 0; i < ids.length; i++){ cards.push(await getCard(ids[i])) }
    return cards.map((c: any) => {return {...c}})
}

const getGroups = async (key: string) => {
    if (key === null) return
    return proxyService.call({
        proxy: "charService",
        expr: `target.getGroups("${key}")`
    })
}

const getCardsForGroup = async (key: string, group: string) => {
    if (key === null) return;
    return proxyService.call({
        proxy: "charService",
        expr: `target.getCardsForGroup("${key}", "${group}")`
    })
}

const getCard = async (id: number) => {
    let cardRq = {expr: `target.getCard(${id}L)`, lang: "GROOVY", proxy: "charService", params: {}}
    return (await proxyService.call(cardRq))
}

const del = async (id: number) => {
    return await proxyService.call({
        proxy: "charService",
        expr: `target.delete(${id}L)`
    })
}

const charService = {
    getAllCardsForAccount: getAllCardsForAccount,
    getCard: getCard,
    getGroups: getGroups,
    load: load,
    clear: clear,
    proxy: proxy,
    del: del,
    getCardsForGroup: getCardsForGroup
}

export default charService;