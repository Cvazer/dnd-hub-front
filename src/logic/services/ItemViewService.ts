import {proxyService} from "../ProxyService";

class ItemViewService {
    load(itemId: number){
        return proxyService.call({proxy: "itemDao", expr: `target.findById(${itemId}L).get()`})
    }
}

export const itemViewService = new ItemViewService()