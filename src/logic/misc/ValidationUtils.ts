export function validateIntPositive(val: string){
    if (val === '0' || +val === 0) return true;
    if (val === '+0') return true;
    if (!val) return false;
    if (val === "") return false;
    return /^[1-9]+[0-9]*$/.test(val)
}

export function validateInt(val: string){
    if (!val) return false;
    if (val === "") return false;
    if (val === '0') return true;
    if (val === '+0') return true;
    if (val === '-0') return false;
    return /^[-+]*[1-9]+[0-9]*$/.test(val)
}

export function validateNotEmpty(val: string){
    if (!val) return false;
    if (val.trim() === "") return false;
    return true
}