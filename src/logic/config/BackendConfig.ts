import {AxiosPromise} from "axios";
import {AppToaster} from "../../App";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";

// export const host = "localhost:8080"
export const host = "178.124.210.248:8080"
// export const proto = "http"
export const proto = "http"
// export const wsProto = "ws"
export const wsProto = "ws"

export interface ErrorInfo {
    code: string,
    descr: string
}

export interface ServiceResponse {
    errorInfo: ErrorInfo
}

export const onError = (promise: AxiosPromise) => {
    return promise.then(res => {
        if (
            !res ||
            (res && !res.data) ||
            (res && res.data && !res.data.errorInfo) ||
            (res && res.data && res.data.errorInfo && res.data.errorInfo.code !== "0")
        ) {
            AppToaster.show({
                message: res.data.errorInfo.descr,
                icon: "warning-sign",
                intent: Intent.DANGER,
                timeout: 3000
            });
            return Promise.reject("Got error from server: "+res.data.errorInfo.descr)
        }
        return res.data
    })
}
export const serviceUrl = {
    api: {
        proxy: {
            eval: {url: "api/proxy/eval", method: 'post'}
        }
    }
}