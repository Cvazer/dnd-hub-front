import axios, {Method} from 'axios'
import {ErrorInfo, onError, ServiceResponse, serviceUrl} from './config/BackendConfig'
import {proto, host} from './config/BackendConfig'

export interface RemoteProxyObject {
    name: string;
    className: string;
    json: string;
}

export interface ProxyCallRq {
    proxy: string;
    expr: string;
    lang?: string;
    params?: Record<string, string>;
    additionalProxies?: any;
    objects?: Array<RemoteProxyObject>;
}

export interface RestCallRq {
    url: string;
    body?: any;
    method?: string;
}

const rest = (rq: RestCallRq): any => {
    if (!rq) return;
    return onError(axios({
        method: rq.method as Method,
        url: `${proto}://${host}/${rq.url}`,
        data: rq.body
    })).then((res: any) => {
        if (Object.keys(res).includes("response")) {
            return res.response
        } else {
            return res
        }
    })
}

const call = (rq: ProxyCallRq): any => {
    if (!rq) return;
    rq.lang = rq.lang || "GROOVY"
    rq.params = rq.params || {}
    rq.additionalProxies = rq.additionalProxies || {}
    return onError(axios({
        method: serviceUrl.api.proxy.eval.method as Method,
        url: `${proto}://${host}/${serviceUrl.api.proxy.eval.url}/${rq.proxy}`,
        data: rq
    })).then((res: CallServiceResponse) => res.response)
}

export class CallServiceResponse implements ServiceResponse{
    errorInfo: ErrorInfo;
    response: any

    constructor(errorInfo: ErrorInfo, response: any) {
        this.errorInfo = errorInfo;
        this.response = response;
    }
}

export const proxyService = {
    call: call,
    rest: rest
}


