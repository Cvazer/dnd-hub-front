import WS, {Client} from 'webstomp-client';
import {host, wsProto} from "../config/BackendConfig";

let client: Client

const connect = (key: string, onConnect: (f: any) => any = () => {}): any => {
    client && client.connected && client.disconnect()
    client = WS.client(wsProto+"://"+host+"/ws", {debug: true})
    client && client.connect(key, "PROTECTED", onConnect, () => {})
};

const send = (dest: string, obj: any): void => {
    client.send(dest, obj)
}

const subscribe = (dest: string, callback: (frame: any) => {}) => {
    return client.subscribe(dest, callback)
}

const ws = {
    connect: connect,
    send: send,
    subscribe: subscribe
}

export default ws