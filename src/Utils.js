import {store} from './store/ConfigureStore'

export const uuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    })};

export const charClassToString = charClass => {
    let oc = store.getState().dict.classes.find(c => c.enumName === charClass);
    return oc === undefined ? "UNDEF" : oc.name;
};

export class Money {
    platinum = 0;
    gold = 0;
    silver = 0;
    copper = 0;

    constructor(platinum, gold, silver, copper) {
        this.platinum = platinum;
        this.gold = gold;
        this.silver = silver;
        this.copper = copper;
    }

    string = () => {
        let s = "";
        s += this.platinum !== 0 ? this.platinum+"p " : "";
        s += this.gold !== 0 ? this.gold+"g " : "";
        s += this.silver !== 0 ? this.silver+"s " : "";
        s += this.copper !== 0 ? this.copper+"c" : "";
        return s;
    };

    units = () => {
        return this.platinum * 100_000 + this.gold * 100 + this.silver * 10 + this.copper
    }
}

export const unitsToMoney = (units) => {
    let pp = Math.floor(units/100_000)
    let gp = Math.floor((units - pp * 100_000)/100)
    let sp = Math.floor((units - (pp * 100_000) - (gp * 100))/10)
    let cp = Math.floor((units - (pp * 100_000) - (gp * 100) - (sp * 10)))
    return new Money(pp, gp, sp, cp);
};

export const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop);

export const saveNumber = (value, setter) => {
    if (!value.match(/^(|0|[1-9][0-9]{0,8})$/)) {}
    else setter(value)
};