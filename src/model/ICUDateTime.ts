export const date = (d: any) => d && Object.assign(new ICUDateTime(0, 1, 1, 0, 0, 0), d)

export default class ICUDateTime {
    private readonly age: number;
    private readonly year: number;
    private readonly dayOfYear: number;
    private readonly hour: number;
    private readonly minute: number;
    private readonly second: number;

    constructor(age: number, year: number,
                dayOfYear: number, hour: number,
                minute: number, second: number) {
        this.age = age;
        this.year = year;
        this.dayOfYear = dayOfYear;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    get weekOfMonth() {let res = Math.ceil(this.dayOfYear / parseFloat("7")) % 4; return res == 0 ? 4 : res}
    get month() {return Math.ceil(this.dayOfYear/parseFloat("28"))}
    get dayOfMonth() {return (this.weekOfMonth - 1) * 7 + (this.dayOfWeek === 0 ? 7 : this.dayOfWeek)}
    get dayOfWeek() {return this.dayOfYear % 7}

    get string() {
        return `${
            this.dayOfMonth<10?"0"+this.dayOfMonth:this.dayOfMonth
        }.${
            this.month<10?"0"+this.month:this.month
        }.${
            this.year
        } - ${
            this.hour<10?"0"+this.hour:this.hour
        }:${
            this.minute<10?"0"+this.minute:this.minute
        }:${
            this.second<10?"0"+this.second:this.second
        }`
    }

    get stringShort() {
        return `${
            this.dayOfMonth<10?"0"+this.dayOfMonth:this.dayOfMonth
        }.${
            this.month<10?"0"+this.month:this.month
        }.${
            this.year
        } - ${
            this.hour<10?"0"+this.hour:this.hour
        }:${
            this.minute<10?"0"+this.minute:this.minute
        }`
    }

    get fancyString() {
        return `${this.dayOfWeek===0?7:this.dayOfWeek} день ${this.weekOfMonth} недели ${this.month} месяца ${this.year} года`
    }
}

export enum ICUAge {
    POWER, DRAGON, MAGIC,
    PROSPERITY, REBIRTH, WAR,
    NEW_WORLD, ENLIGHTENMENT
}

export enum ICUDay {
    SUNDAY, MONDAY, TUESDAY,
    WEDNESDAY, THURSDAY, FRIDAY,
    SATURDAY
}

export enum ICUMonth {
    JAN = 1, FEB, MAR, APR,
    MAY, JUN, SOL, JUL,
    AUG, SEP, OCT, NOV,
    DEC
}