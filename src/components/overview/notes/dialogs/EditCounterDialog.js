import React, {useEffect, useState} from "react";
import {Button, Dialog, Divider, InputGroup, Intent, Label, NumericInput} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import SimpleMDE from "react-simplemde-editor";
import service from "../../../../logic/services/dialog/notes/EditNoteDialogService";
import { v4 as getUuid } from 'uuid';

const EditCounterDialog = () => {
    let state = useSelector(store => store.dialog[DG.OverviewCounterEdit]);
    let counter = state.context || {}

    let [name, setName] = useState(counter.name || "")
    let [min, setMin] = useState(counter.min || 0)
    let [max, setMax] = useState(counter.max || 0)
    let [value, setValue] = useState(counter.value || 0)
    let [step, setStep] = useState(counter.step || 1)
    let [uuid, setUuid] = useState(counter.uuid || "")
    let [loading, setLoading] = useState(false)

    useEffect(() => {
        setName(counter.name || "")
        setMin(counter.min || 0)
        setMax(counter.max || 0)
        setValue(counter.value || 0)
        setStep(counter.step || 1)
        setUuid(counter.uuid || "")
        setLoading(false)
    }, [state])

    let close = () => dialogueService.setState(DG.OverviewCounterEdit, {open: false, context: undefined})
    let save = () => {
        uuid === ""
            ? service.addCounter(name, max, min, value, step, getUuid())
            : service.updateCounter(name, max, min, value, step, uuid)
        close()
    }

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Добавление счетчика`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <Label>
                    <div className={"bp3-text-muted bp3-text-small"}>Название</div>
                    <InputGroup fill large value={name} onChange={e => setName(e.target.value)}/>
                </Label>
                <Label>
                    <div className={"bp3-text-muted bp3-text-small"}>Максимальное значение</div>
                    <NumericInput fill large value={max} onValueChange={e => setMax(e)}/>
                </Label>
                <Label>
                    <div className={"bp3-text-muted bp3-text-small"}>Минимальное значение</div>
                    <NumericInput fill large value={min} onValueChange={e => setMin(e)}/>
                </Label>
                <Label>
                    <div className={"bp3-text-muted bp3-text-small"}>Текущее значение</div>
                    <NumericInput fill large value={value} onValueChange={e => setValue(e)}/>
                </Label>
                <Label>
                    <div className={"bp3-text-muted bp3-text-small"}>Шаг</div>
                    <NumericInput fill large value={step} onValueChange={e => setStep(e)}/>
                </Label>
                <Divider style={{marginBottom: "15px"}}/>
                <Button loading={loading} intent={Intent.SUCCESS} fill onClick={() => save()}>Сохранить</Button>
            </div>
        </Dialog>
    )
};

export default EditCounterDialog