import React, {useEffect, useState} from "react";
import {Button, Dialog, Divider, InputGroup, Intent} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import s from "../../../../css/overview/traits/dialog/AddFeatureDialog.module.css"
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import profsService from "../../../../logic/services/ProfsService";
import service from "../../../../logic/services/dialog/profs/AddProfDialogService"
import traitService from "../../../../logic/services/TraitService";
import {textHtml} from "../../../../App";

const NoteViewDialog = () => {
    let state = useSelector(store => store.dialog[DG.OverviewNoteView]);
    let note = state.context || {text: ""}

    let close = () => dialogueService.setState(DG.OverviewNoteView, {open: false, context: undefined})

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"eye-open"}
                isCloseButtonShown
                title={`Заметка`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <div dangerouslySetInnerHTML={{__html: textHtml(note.text)}}/>
            </div>
        </Dialog>
    )
};

export default NoteViewDialog