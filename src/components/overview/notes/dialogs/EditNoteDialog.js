import React, {useEffect, useState} from "react";
import {Button, Dialog, Divider, InputGroup, Intent, Label} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import SimpleMDE from "react-simplemde-editor";
import service from "../../../../logic/services/dialog/notes/EditNoteDialogService";
import { v4 as getUuid } from 'uuid';

const EditNoteDialog = () => {
    let state = useSelector(store => store.dialog[DG.OverviewNoteEdit]);
    let note = state.context || {}

    let [name, setName] = useState(note.name || "")
    let [text, setText] = useState(note.text || "")
    let [uuid, setUuid] = useState(note.uuid || "")
    let [loading, setLoading] = useState(false)

    useEffect(() => {
        setText(note.text || "")
        setName(note.name || "")
        setUuid(note.uuid || "")
        setLoading(false)
    }, [state])

    let close = () => dialogueService.setState(DG.OverviewNoteEdit, {open: false, context: undefined})
    let save = () => {
        uuid === "" ? service.addNote(name, text, getUuid()) : service.updateNote(name, text, uuid)
        close()
    }

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Добавление заметки`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <Label>
                    <div className={"bp3-text-muted bp3-text-small"}>Название</div>
                    <InputGroup fill large value={name} onChange={e => setName(e.target.value)}/>
                </Label>
                <Label>
                    <div className={"bp3-text-muted bp3-text-small"}>Текст</div>
                    <SimpleMDE value={text} onChange={v => setText(v)}/>
                </Label>
                <Divider style={{marginBottom: "15px"}}/>
                <Button loading={loading} intent={Intent.SUCCESS} fill onClick={() => save()}>Сохранить</Button>
            </div>
        </Dialog>
    )
};

export default EditNoteDialog