import React, {useEffect, useState} from "react";
import style from '../../../css/overview/notes/NotesOverview.module.css'
import {Button, Card, Divider, Elevation, Intent, Label, Menu, MenuItem} from "@blueprintjs/core";
import {Container} from 'react-grid-system'
import dialogueService, {Dialog} from "../../../logic/services/DialogService";
import NoteViewDialog from "./dialogs/NoteViewDialog";
import EditNoteDialog from "./dialogs/EditNoteDialog";
import {useSelector} from "react-redux";
import notesService from "../../../logic/services/NotesService";
import {ContextMenuTarget} from "@blueprintjs/core/lib/esnext/components/context-menu/contextMenuTarget";
import Counter from "../../common/Counter";
import EditCounterDialog from "./dialogs/EditCounterDialog";

const NotesOverview = () => {
    let data = useSelector(store => store.overview.notes)

    useEffect(() => {notesService.load()}, [])

    return (
        <div className={style.root}>
            <NoteViewDialog/>
            <EditNoteDialog/>
            <EditCounterDialog/>
            <Container>
                <Card elevation={Elevation.TWO}>
                    <div style={{textAlign: "center", marginBottom: "5px"}}>Записи</div>
                    <div className={style.noteList}>
                        {data && data.notes && data.notes.map(note =>
                            <NoteCard key={note.uuid} note={note}/>
                        )}
                    </div>
                    <Divider style={{marginTop: "0"}}/>
                    <Button minimal fill
                            onClick={() => dialogueService.openState(Dialog.OverviewNoteEdit, {})}
                            intent={Intent.SUCCESS}>Добавить</Button>
                </Card>
                <br/>
                <Card elevation={Elevation.TWO}>
                    <div style={{textAlign: "center", marginBottom: "5px"}}>Счетчики</div>
                    <div className={style.counterList}>
                        {data && data.counters && data.counters.map(counter =>
                            <CounterCard key={counter.uuid} counter={counter}/>
                        )}
                    </div>
                    <Divider style={{marginTop: "0"}}/>
                    <Button minimal fill
                            onClick={() => dialogueService.openState(Dialog.OverviewCounterEdit, {})}
                            intent={Intent.SUCCESS}>Добавить</Button>
                </Card>
            </Container>
        </div>
    )
}

const NoteCard = ContextMenuTarget(class CounterCard extends React.Component {
    render() {
        let note = this.props.note

        let show = note => {dialogueService.openState(Dialog.OverviewNoteView, note)}
        let edit = note => {dialogueService.openState(Dialog.OverviewNoteEdit, note)}

        return (
            <div className={style.noteCardRoot} onClick={() => show(note)}>
                <div className={style.noteCardName+" bp3-text-overflow-ellipsis"}>{note.name}</div>
                <div className={style.noteCardBtn}>
                    <Button minimal icon={"edit"} onClick={e => {
                        e.stopPropagation()
                        e.preventDefault()
                        edit(note)
                    }}/>
                </div>
            </div>
        )
    }

    renderContextMenu() {
        return <Menu>
            <MenuItem icon={"trash"} onClick={this.handleDelete} text="Удалить" />
        </Menu>
    }

    handleDelete = () => {notesService.deleteNote(this.props.note)}
})

const CounterCard = ContextMenuTarget(class CounterCard extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {value: props.counter.value}
    }

    static getDerivedStateFromProps(props, state) {
        if (state.value === props.counter.value) return null
        return {...state, value: props.counter.value}
    }

    render() {
        let counter = this.props.counter

        let edit = note => {dialogueService.openState(Dialog.OverviewCounterEdit, note)}

        return (
            <div className={style.counterCardRoot}>
                <div className={style.counterCardBody}>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>{counter.name}</div>
                        <Counter minimalValue={counter.min} maxValue={counter.max}
                                 value={this.state.value} callback={this.saveValue} step={counter.step}/>
                    </Label>
                </div>
                <div className={style.counterCardBtn}>
                    <Button minimal icon={"edit"} onClick={e => {
                        e.stopPropagation()
                        e.preventDefault()
                        edit(counter)
                    }}/>
                </div>
            </div>
        )
    }

    saveValue = value => {notesService.setCounter(value, this.props.counter.uuid)}

    renderContextMenu() {
        return <Menu>
            <MenuItem icon={"trash"} onClick={this.handleDelete} text="Удалить" />
        </Menu>
    }

    handleDelete = () => {notesService.deleteCounter(this.props.counter)}
})

export default NotesOverview