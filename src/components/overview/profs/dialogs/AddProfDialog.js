import React, {useEffect, useState} from "react";
import {Button, Dialog, Divider, InputGroup, Intent} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import s from "../../../../css/overview/traits/dialog/AddFeatureDialog.module.css"
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import profsService from "../../../../logic/services/ProfsService";
import service from "../../../../logic/services/dialog/profs/AddProfDialogService"
import traitService from "../../../../logic/services/TraitService";

const AddProfDialog = () => {
    let state = useSelector(store => store.dialog[DG.ProfAdd]);

    let [list, setList] = useState([])
    let [search, setSearch] = useState("")
    let [prof, setProf] = useState(undefined)

    let [loading, setLoading] = useState(false)

    let close = () => dialogueService.setState(DG.ProfAdd, {open: false, context: undefined})

    useEffect(() => {
        profsService.all().then(res => setList(res))
        setSearch("")
    }, [state])

    let filter = query => { return list.filter(e => {
        return e.value.name.toUpperCase().includes(query.toUpperCase()) ||
            e.cat.name.toUpperCase().includes(query.toUpperCase())
    })}

    let select = prof => setProf(prof)

    let add = () => {
        if (!prof) return
        setLoading(true)
        service.add(prof.value.id, prof.cat.id)
            .then(charId => profsService.load(charId))
            .then(() => setLoading(false))
            .then(() => close())
    }

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Добавление владения`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <div className={s.listRoot}>
                    <div className={s.listRoot}>
                        <InputGroup type="search" value={search}
                                    onChange={(e) => setSearch(e.target.value)}
                                    leftIcon={"search"} placeholder={"Название"} />
                        <br/>
                        <div className={s.list}>
                            {filter(search).map(e => <div
                                className={prof
                                    ? prof.value.id === e.value.id
                                        ? s.listItemSelected
                                        : s.listItem
                                    : s.listItem}
                                onClick={() => select(e)} key={e.value.id+e.cat.id}>
                                {e.value.name} ({e.cat.name})
                            </div>)}
                        </div>
                    </div>
                </div>
                <br/>
                <Divider style={{marginBottom: "15px"}}/>
                <Button loading={loading} intent={Intent.SUCCESS} fill onClick={() => {add()}}>Добавить</Button>
            </div>
        </Dialog>
    )
};

export default AddProfDialog