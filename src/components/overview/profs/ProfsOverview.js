import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import LoadingView from "../../LoadingView";
import profsService from "../../../logic/services/ProfsService"
import style from "../../../css/overview/profs/ProfsOverview.module.css"
import {Button, Classes, Dialog as Dialog2, Icon, Intent, Tag} from "@blueprintjs/core";
import style2 from "../../../css/overview/items/ItemsOverview.module.css";
import {Button as FabButton, Container, lightColors} from "react-floating-action-button";
import dialogueService, {Dialog} from "../../../logic/services/DialogService";
import AddProfDialog from "./dialogs/AddProfDialog";

const ProfsOverview = () => {
    let char = useSelector(store => store.overview.char)
    let list = useSelector(store => store.overview.profs)

    let [open, setOpen] = useState(false)
    let [loading, setLoading] = useState(false)
    let [del, setDel] = useState()

    useEffect(() => {profsService.load(char.id)}, [char])

    let remove = item => {setDel(item); setOpen(true)}
    let deleteProf = () => {
        setLoading(true)
        profsService.delete(char.id, del.value.id, del.cat.id)
            .then(() => {
                setDel(undefined)
                setOpen(false)
                setLoading(false)
            })
    }

    return (!char || !list) ? <div className={style.divWrap}><LoadingView/></div> : (
        <div className={style.root}>
            <AddProfDialog/>
            <Dialog2 title="Удалить владение"  isOpen={open} style={{width: "80%"}}>
                <div className={Classes.DIALOG_BODY}>Точно удалить владение <b>{del && del.value.name}</b>?</div>
                <div className={Classes.DIALOG_FOOTER}>
                    <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                        <Button loading={loading} intent={Intent.SUCCESS} onClick={() => deleteProf()}>Да</Button>
                        <Button intent={Intent.DANGER}
                                loading={loading}
                                onClick={() => {setOpen(false); setDel(undefined)}}>Нет</Button>
                    </div>
                </div>
            </Dialog2>
            <ProfsList list={list} remove={remove}/>
            <Container className={style2.fabContainer}>
                <FabButton
                    tooltip="Добавить владение"
                    onClick={() => {dialogueService.open(Dialog.ProfAdd)}}
                    styles={{backgroundColor: lightColors.teal, color: lightColors.white}}>
                    <Icon icon={"plus"} iconSize={30}/>
                </FabButton>
            </Container>
        </div>
    )
}

const ProfsList = ({list, remove}) => {
    let catsById = {}
    let profsByCat = {}

    list.forEach(e => {
        if (!catsById[e.cat.id]) catsById[e.cat.id] = e.cat
        if (!profsByCat[e.cat.id]) profsByCat[e.cat.id] = []
        profsByCat[e.cat.id].push(e)
    })

    return <div className={style.list}>
        {Object.keys(catsById)
            .sort((a, b) => a.localeCompare(b))
            .map(key => <ProfCat remove={remove} key={key} cat={catsById[key]} items={profsByCat[key]}/>)}
    </div>
}

const ProfCat = ({cat, items, remove}) => {
    return <div className={style.catRoot}>
        <div className={style.catName}>{cat.name}</div>
        <div className={style.catBody}>
            {items.map(i => <ProfItem key={i.value.id} remove={remove} item={i}/>)}
        </div>
    </div>
}

const ProfItem = ({item, remove}) => {
    return <div className={style.itemRoot}>
        <Tag large onRemove={() => remove(item)} minimal>{item.value.name}</Tag>
    </div>
}

export default ProfsOverview