import React, {Fragment, useEffect, useState} from 'react';
import {Container} from "react-grid-system";
import {useSelector} from "react-redux";
import charService from "../../logic/services/CharService";
import LoadingView from "../LoadingView";
import {CharCard} from "../cards/CharCard";
import {useNavigate} from "react-router";
import {Button, Collapse} from "@blueprintjs/core";
import style from "../../css/overview/CharSelectView.module.css"

const CharSelectView = () => {
    const [groups, setGroups] = useState([]);
    const [loading, setLoading] = useState(true);
    const key = useSelector(store => store.account.key);

    useEffect(() => {
        setGroups([])
        setLoading(true)
        key && charService.getGroups(key)
            .then(res => setGroups(res))
            .then(_ => setLoading(false))
    }, [key])

    if (loading) return <div style={{height: "calc(100vh - 80px)"}}><LoadingView /></div>
    return (
        <Fragment>
            <Container>
                <div className={style.root}>
                    {groups.map(group => <CharCardCroup key={group} group={group} account={key}/>)}
                </div>
                <br/>
                <br/>
            </Container>
        </Fragment>
    )
};

const CharCardCroup = ({group, account}) => {
    let role = useSelector(state => state.account.role)
    let [open, setOpen] = useState(group === "Все" ? role !== "admin" : false)
    let [chars, setChars] = useState()

    let navigate = useNavigate()
    useEffect(() => {
        if (!open) { return }
        charService.getCardsForGroup(account, group)
            .then(res => setChars(res))
    }, [open])

    useEffect(() => {
        if (role !== "admin") return
        let lastGroup = localStorage.getItem("charSelectView:lastGroup");
        setOpen(lastGroup ? lastGroup === group : open)
    }, [])

    return (
        <div className={style.group}>
            <Button rightIcon={!open?"chevron-right":"chevron-down"}
                    fill large alignText={"left"}
                    onClick={() => {
                        setOpen(!open)
                        if (role !== "admin") return;
                        !open && localStorage.setItem("charSelectView:lastGroup", group)
                    }}
                    minimal>
                {group}
            </Button>
            <Collapse isOpen={open}>
                <div className={style.btnSpacer}/>
                <div className={style.collapse}>
                    {chars === undefined ? <LoadingView/> : chars.map(card => {
                        return <CharCard key={card.id}
                                         card={card}
                                         clickHandler={charSelectHandle(navigate)}
                                         delHandler={charDelHandle(setChars, chars)}/>
                    })}
                </div>
            </Collapse>
        </div>
    )
}

const charSelectHandle = navigate => card => e => {
    e.preventDefault();
    e.stopPropagation();
    charService.load(card.id).then(() => {
        let url = localStorage.getItem("attemptedLocation")
        if (url && url.includes("/overview/")) {
            url = url.replaceAll(/%charId%/g, card.id)
            navigate(url, {replace: false})
        } else {
            localStorage.setItem("attemptedLocation", "/overview/char/%charId%")
            navigate(`/overview/char/${card.id}`, {replace: false})
        }
    }).catch(() => {})
};

const charDelHandle = (setCharCards, charCards) => card => e => {
    e.preventDefault();
    e.stopPropagation();
    charService.del(card.id).catch(() => {}).then(() => {
        setCharCards(charCards.filter(c => c.id !== card.id))
    })
};

export default CharSelectView