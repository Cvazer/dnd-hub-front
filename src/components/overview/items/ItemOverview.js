import React, {Fragment, useEffect, useState} from 'react'
import {useParams} from "react-router";
import style from '../../../css/overview/item/ItemOverview.module.css'
import {Container} from "react-grid-system";
import {itemViewService as service} from '../../../logic/services/ItemViewService'
import SvgVault from "../../SvgVault";
import SvgIcon from "../../common/SvgIcon";
import LoadingView from "../../LoadingView";
import {Checkbox, Divider, InputGroup, Intent, Label, Popover, Position, Tag, Tooltip} from "@blueprintjs/core";
import s from "../../../css/overview/items/dialogs/ChaneInvItemDialog.module.css";
import {unitsToMoney} from "../../../Utils";
import {dictValues, textHtml} from "../../../App";
import cn from 'classnames'
import {Link} from "react-router-dom";

const ItemOverview = () => {
    let [item, setItem] = useState()
    let { id } = useParams();

    useEffect(() => {service.load(+id).then(res => setItem(res))}, [id])

    let weaponPros = dictValues("WeaponPropsDict")

    return item ? <Fragment>
            <div className={style.root}>
                <div className={style.pane}><Container>
                    <h3 className={style.name}>
                        {item.name}
                        {item.baseItem && " ("}
                        {item.baseItem && <Link to={`/overview/item/${item.baseItem.id}`}>{item.baseItem.name}</Link> }
                        {item.baseItem && ")"}
                    </h3>
                    <Divider/>
                    <ItemInfoBlockAll item={item}/>
                    <Divider/>
                    {item.descr && <Fragment>
                        <br/>
                        <div dangerouslySetInnerHTML={{__html: textHtml(item.descr)}}/>
                        <br/>
                    </Fragment>}
                    {item.armorInfo.armor && <Fragment>
                        <h3 className={style.section}>Броня</h3>
                        <Divider/>
                        <ItemInfoBlockArmor item={item}/>
                    </Fragment>}
                    {item.weaponInfo.weapon && <Fragment>
                        <h3 className={style.section}>Оружие</h3>
                        <Divider/>
                        <ItemInfoBlockWeapon item={item}/>
                        <Divider/>
                        <br/>
                        <div className={style.split}>
                            <div className={style.leftSplit}>
                                <Label>
                                    <div className={"bp3-text-muted bp3-text-small"}>Категория</div>
                                    <div className={cn("bp3-input-group bp3-small", style.fit)}>
                                        <div className="bp3-input">{item.weaponInfo.category.name}</div>
                                    </div>
                                </Label>
                                <Label>
                                    <div className={"bp3-text-muted bp3-text-small"}>Тип урона</div>
                                    <div className={cn("bp3-input-group bp3-small", style.fit)}>
                                        <div className="bp3-input">{item.weaponInfo.damageType.name}</div>
                                    </div>
                                </Label>
                                <Label>
                                    <div className={"bp3-text-muted bp3-text-small"}>Дистанция</div>
                                    <div className={cn("bp3-input-group bp3-small", style.fit)}>
                                        <div className="bp3-input">{item.weaponInfo.distance.name}</div>
                                    </div>
                                </Label>
                            </div>
                            <div className={style.rightSplit}>
                                <div className={"bp3-text-muted bp3-text-small"}>Свойства</div>
                                <br/>
                                <div className={style.tags}>
                                    {item.weaponInfo.props
                                        .concat(item.baseItem
                                            ? (item.baseItem.weaponInfo.props || [])
                                                .filter(e => !item.weaponInfo.props.map(e => e.id).includes(e.id))
                                            : [])
                                        .map(p => {
                                        let prop = weaponPros.find(e => e.id === p.id)
                                        return (
                                            <Tooltip key={p.id}
                                                     position={Position.AUTO}
                                                     content={<div dangerouslySetInnerHTML=
                                                                       {{__html: textHtml(prop.descr)}}/>}>
                                                <Tag minimal large intent={Intent.SUCCESS}>{prop.name}</Tag>
                                            </Tooltip>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </Fragment>}
                    {item.magicInfo.magical && <Fragment>
                        <h3 className={style.section}>Магия</h3>
                        <Divider/>
                        <br/>
                        <div className={style.split}>
                            <div className={style.leftSplit2}>
                                <Checkbox checked={item.magicInfo.attunement}>Настройка</Checkbox>
                                <Checkbox checked={item.magicInfo.curse}>Проклятый</Checkbox>
                            </div>
                            <div className={style.rightSplit2}>
                                <Checkbox checked={item.magicInfo.sentient}>Сознание</Checkbox>
                                <Checkbox checked={item.magicInfo.wondrous}>Диковинка</Checkbox>
                            </div>
                        </div>
                        <br/>
                        <div className={style.list}>
                            <div className={style.lbl}>
                                <div className={"bp3-text-muted bp3-text-small"}>Базовый</div>
                                <div className={cn("bp3-input-group bp3-small", style.fit2)}>
                                    <div className="bp3-input">{item.magicInfo.bonusBase}</div>
                                </div>
                            </div>
                            <div className={style.lbl}>
                                <div className={"bp3-text-muted bp3-text-small"}>Броня</div>
                                <div className={cn("bp3-input-group bp3-small", style.fit2)}>
                                    <div className="bp3-input">{item.magicInfo.bonusAc}</div>
                                </div>
                            </div>
                            <div className={style.lbl}>
                                <div className={"bp3-text-muted bp3-text-small"}>Атака</div>
                                <div className={cn("bp3-input-group bp3-small", style.fit2)}>
                                    <div className="bp3-input">{item.magicInfo.bonusAttack}</div>
                                </div>
                            </div>
                            <div className={style.lbl}>
                                <div className={"bp3-text-muted bp3-text-small"}>Магия</div>
                                <div className={cn("bp3-input-group bp3-small", style.fit2)}>
                                    <div style={{textAlign: "center"}} className="bp3-input">{item.magicInfo.bonusSpell}</div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                    </Fragment>}
                </Container></div>
            </div>
        </Fragment> : <LoadingView/>
}

const ItemInfoBlockAll = ({item}) => {
    return <div className={s.infoContainer}>
        {item.weight && <div className={s.info}>
            <SvgIcon src={SvgVault.weight} size={20}/>
            <span>{item.weight} lb.</span>
        </div>}
        {(item.value || 0) !== 0 && <div className={s.info}>
            <SvgIcon src={SvgVault.coin} size={20}/>
            <span>{unitsToMoney(item.value || 0).string()}</span>
        </div>}
    </div>
}

const ItemInfoBlockWeapon = ({item}) => {
    return <div className={s.infoContainer}>
        {item.weaponInfo.distance && item.weaponInfo.damage && <div className={s.info}>
            <SvgIcon src={SvgVault[item.weaponInfo.distance.id==="MELEE" ? "oneSword" : "simpleBow"]} size={20}/>
            <span>{item.weaponInfo.damage.replace(/1d/g, "D").toUpperCase()}</span>
        </div>}
        {(item.weaponInfo.rangeEffective || item.weaponInfo.rangeMax) && <div className={s.info}>
            <SvgIcon src={SvgVault.target} size={20}/>
            <span>{
                (item.weaponInfo.rangeEffective && item.weaponInfo.rangeMax)
                    ? `${item.weaponInfo.rangeEffective}/${item.weaponInfo.rangeMax}`
                    : (item.weaponInfo.rangeEffective && !item.weaponInfo.rangeMax)
                        ? `${item.weaponInfo.rangeEffective}`
                        : (!item.weaponInfo.rangeEffective && item.weaponInfo.rangeMax)
                            ? `${item.weaponInfo.rangeEffective}`
                            : ''
            }</span>
        </div>}
    </div>
}

const ItemInfoBlockArmor = ({item}) => {
    return <div className={s.infoContainer}>
        {item.armorInfo.ac && <div className={s.info}>
            <SvgIcon src={SvgVault.shield} size={20}/>
            <span>{item.armorInfo.ac}</span>
        </div>}
        {item.armorInfo.strength && <div className={s.info}>
            <SvgIcon src={SvgVault.strength} size={20}/>
            <span>{item.armorInfo.strength}</span>
        </div>}
        <div className={s.info}>
            <SvgIcon src={SvgVault.incognito} size={20}/>
            <span>{item.armorInfo.stealth?<SvgIcon src={SvgVault.cancel} size={12}/>:"-"}</span>
        </div>
    </div>
}

export default ItemOverview