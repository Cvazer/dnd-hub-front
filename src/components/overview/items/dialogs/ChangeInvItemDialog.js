import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog, Divider, Icon, InputGroup, TextArea} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import s from "../../../../css/overview/items/dialogs/ChaneInvItemDialog.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import Counter from "../../../common/Counter";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import SvgIcon from "../../../common/SvgIcon";
import SvgVault from "../../../SvgVault";
import {unitsToMoney} from "../../../../Utils";
import {changeInvItemDialogService as service} from "../../../../logic/services/dialog/items/ChangeInvItemDialogService";
import {Base64} from 'js-base64';

const ChangeInvItemDialog = () => {
    let state = useSelector(store => store.dialog[DG.InvItemChange]);

    let item = state.context || {}
    let [val, setVal] = useState("");
    let [com, setCom] = useState("");
    let [ali, setAli] = useState("");
    let [editing, setEditing] = useState(false);
    let [loading, setLoading] = useState(false);

    let close = () => dialogueService.setState(DG.InvItemChange, {
        open: false,
        context: undefined,
        invId: undefined,
        del: undefined
    });

    useEffect(() => setVal(item && item.count), [state]);
    useEffect(() => setCom(item && item.comment), [state]);
    useEffect(() => setAli(item && item.alias), [state]);
    useEffect(() => setEditing(false), [state]);

    let editingButton = <Button className={s.nameEditButton}
                                icon={"edit"} minimal small
                                onClick={e => {
                                    e.stopPropagation()
                                    e.preventDefault()
                                    if (!editing === true && (!ali || ali === "")) setAli(item.name)
                                    setEditing(!editing)
                                }}/>
    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                // icon={"edit"}
                isCloseButtonShown
                title={`Редактировать`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                {
                    editing
                        ? <div className={s.name}>
                            <InputGroup value={ali || ""} small
                                        onChange={e => {
                                            let newVal = e.target.value
                                            newVal === ""
                                                ? setAli(undefined)
                                                : setAli(newVal)
                                        }} rightElement={editingButton} />
                        </div>
                        : <div className={s.name}>
                            <span>{item && ali ? ali : item.name}</span>
                            {editingButton}
                        </div>
                }
                <Divider/>
                <ItemInfoBlock item={item}/>
                <br/>
                <Counter value={val} callback={setVal} />
                <br/>
                <TextArea growVertically={true}
                          placeholder={"Комментарий"}
                          small fill value={com} onChange={e => setCom(e.target.value)}/>
                <br/>
                <br/>
                {state.del && <ButtonGroup fill>
                    <Button loading={loading} intent={Intent.DANGER} minimal
                        onClick={() => service.remove(item.id, state.invId, setLoading)}>
                        Удалить
                    </Button>
                </ButtonGroup>}
                <br/>
                <ButtonGroup fill>
                    <Button loading={loading} intent={Intent.SUCCESS}
                            onClick={
                                item.id
                                    ? () => service.set(
                                        item.id, val,
                                        com && `"${Base64.encode(com)}"`,
                                        ali && `"${ali}"`, setLoading)
                                    : () => service.add(
                                        state.invId, item.itemId, val,
                                        com && `"${Base64.encode(com)}"`,
                                        ali && `"${ali}"`, setLoading)
                            }>
                        Сохранить
                    </Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

const ItemInfoBlock = ({item}) => {
    return <div className={s.infoContainer}>
        {item.weight && <div className={s.info}>
            <SvgIcon src={SvgVault.weight} size={20}/>
            <span>{item.weight} lb.</span>
        </div>}
        {(item.cost || 0) !== 0 && <div className={s.info}>
            <SvgIcon src={SvgVault.coin} size={20}/>
            <span>{unitsToMoney(item.cost || 0).string()}</span>
        </div>}
        {item.ac && <div className={s.info}>
            <SvgIcon src={SvgVault.shield} size={20}/>
            <span>{item.ac}</span>
        </div>}
        {item.distance && item.damage && <div className={s.info}>
            <SvgIcon src={SvgVault[item.distance==="MELEE" ? "oneSword" : "simpleBow"]} size={20}/>
            <span>{item.damage.replace(/1d/g, "D").toUpperCase()}</span>
        </div>}
        {item.range && <div className={s.info}>
            <SvgIcon src={SvgVault.target} size={20}/>
            <span>{item.range}</span>
        </div>}
        {item.armor && <div className={s.info}>
            <SvgIcon src={SvgVault.incognito} size={20}/>
            <span>{item.stealth?<SvgIcon src={SvgVault.cancel} size={12}/>:"-"}</span>
        </div>}
    </div>
}

export default ChangeInvItemDialog