import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog, Divider, InputGroup} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import s2 from '../../../../css/overview/items/ItemsOverview.module.css'
import s from "../../../../css/overview/items/dialogs/AddInvItemDialog.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {addInvItemDialogService as service} from "../../../../logic/services/dialog/items/AddInvItemDialogService";
import {useBottomScrollListener} from 'react-bottom-scroll-listener'
import SvgVault, {itemCats} from "../../../SvgVault";
import {ReactSVG} from "react-svg";

const AddInvItemDialog = () => {
    let state = useSelector(store => store.dialog[DG.InvItemAdd]);

    let [val, setVal] = useState("");
    let [itemId, setItemId] = useState(undefined);
    let [invId, setInvId] = useState(undefined);
    let [loading, setLoading] = useState(false);
    let [list, setList] = useState([]);
    let [page, setPage] = useState(1);

    let close = () => dialogueService.setState(DG.InvItemAdd, {open: false, context: undefined});

    useEffect(() => {
        setVal(""); setPage(0)
        state.context && setInvId(state.context.currInv)
        service.getPage("", 0, setLoading).then(res => {setList(res)})}, [state]);

    useEffect(() => {setPage(0); service.getPage(val, 0, setLoading).then(res => {setList(res)})}, [val])
    useEffect(() => {page !== 0 && service.getPage(val, page, setLoading).then(res => {setList(list.concat(res))})}, [page])

    let scrollRef = useBottomScrollListener(() => {setPage(page+1)}, 0, 200)

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Добавить предмет`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <InputGroup value={val} type={"search"}
                            placeholder={"Поиск"}
                            onChange={e => {setVal(e.target.value); scrollRef.current.scrollTo(0, 0)}}
                            leftIcon={"search"}/>
                <br/>
                <div ref={scrollRef} className={s.list}>
                    {list.map(item =>
                        <div key={item.itemId} className={cn(s.item, item.itemId === itemId && s.itemSelected)}
                             onClick={() => setItemId(item.itemId)}>
                            <ReactSVG src={itemCats[item.type]} beforeInjection={(svg) => {svg.setAttribute('style',
                                'width: 18px; ' +
                                'height: 18px; ' +
                                'fill: #515151')}}/>
                            <div>{item.name}</div>
                        </div>)
                    }
                </div>
                <br/>
                <Divider/>
                <div className={style.invList}>
                    { state.context && state.context.list.map(sec =>
                        <div className={cn(s2.icon, style.inv, invId === sec.id && style.invSelected)}
                             onClick={() => setInvId(sec.id)}
                             key={sec.id}>
                            <ReactSVG src={SvgVault[sec.icon]} beforeInjection={(svg) => {svg.setAttribute('style',
                                'width: 35px; ' +
                                'height: 35px; ' +
                                'fill: #515151')}}/>
                            <span>{sec.name}</span>
                        </div>
                    )}
                </div>
                <Divider/>
                <br/>
                <ButtonGroup fill>
                    <Button loading={loading} intent={Intent.SUCCESS}
                            onClick={() => service.add(itemId, invId, setLoading)}>
                        Добавить
                    </Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

export default AddInvItemDialog