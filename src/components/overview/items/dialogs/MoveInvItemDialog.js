import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog, Divider} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import s2 from '../../../../css/overview/items/ItemsOverview.module.css'
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {moveInvItemDialogService as service} from "../../../../logic/services/dialog/items/MoveInvItemDialogService";
import SvgVault from "../../../SvgVault";
import {ReactSVG} from "react-svg";

const MoveInvItemDialog = () => {
    let state = useSelector(store => store.dialog[DG.InvItemMove]);

    let [invId, setInvId] = useState(undefined);
    let [loading, setLoading] = useState(false);

    let close = () => dialogueService.setState(DG.InvItemMove, {open: false, context: undefined});

    useEffect(() => {state.context && setInvId(state.context.currInv)}, [state]);

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Переместить предмет`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <div className={style.invList2}>
                    {state.context && state.context.list.length !== 0
                        ? state.context.list.map(sec =>
                                <div className={cn(s2.icon, style.inv, invId === sec.id && style.invSelected)}
                                     onClick={() => setInvId(sec.id)}
                                     key={sec.id}>
                                    <ReactSVG src={SvgVault[sec.icon]} beforeInjection={(svg) => {
                                        svg.setAttribute('style',
                                            'width: 35px; ' +
                                            'height: 35px; ' +
                                            'fill: #515151')
                                    }}/>
                                    <span>{sec.name}</span>
                                </div>)
                        : <div className={"bp3-text-disabled bp3-text-large"} style={{
                            textAlign: "center",
                            width: "100%",
                          }}>Нет других инвентарей</div>
                    }
                </div>
                <br/>
                <Divider/>
                <br/>
                <ButtonGroup fill>
                    <Button loading={loading} intent={Intent.SUCCESS}
                            onClick={() => service.move(state.context.item.id, +invId, setLoading)}>
                        Переместить
                    </Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

export default MoveInvItemDialog