import React, {Fragment, useEffect, useState} from 'react'
import style from '../../../css/overview/items/ItemsOverview.module.css'
import '../../../css/overview/items/ItemsOverview.css'
import SvgVault, {itemCats} from "../../SvgVault";
import {ReactSVG} from 'react-svg';
import {Icon, Menu, MenuItem, Tab, Tabs} from "@blueprintjs/core";
import invService from "../../../logic/services/InvService";
import {useSelector} from "react-redux";
import LoadingView from "../../LoadingView";
import dialogueService, {Dialog} from "../../../logic/services/DialogService";
import ChangeInvItemDialog from "./dialogs/ChangeInvItemDialog";
import { Container, Button, lightColors } from 'react-floating-action-button'
import AddInvItemDialog from "./dialogs/AddInvItemDialog";
import MoveInvItemDialog from "./dialogs/MoveInvItemDialog";
import {useNavigate} from "react-router";
import {ContextMenu2} from "@blueprintjs/popover2";


const ItemsOverview = () => {
    let [sec, setSec] = useState(-1)
    let char = useSelector(store => store.overview.char)
    let [loading, setLoading] = useState(true)
    let list = useSelector(store => Object.values(store.overview.items || {}))

    useEffect(() => {invService.load(char.id).then(() => {setLoading(false)})}, [char])
    useEffect(() => {window.scrollTo(0, 0)}, [])

    let allInv = {
        id: -1,
        icon: "list",
        name: "Все",
        items: list.flatMap(inv => inv.items || [])
    }

    return loading
        ? <div style={{height: "100vh"}}><LoadingView /></div>
        : <div className={style.root}>
            <ChangeInvItemDialog />
            <AddInvItemDialog />
            <MoveInvItemDialog />
            <div className={style.catPanelsContainer+" items-overview-container"}>
                <Tabs selectedTabId={sec} onChange={e => setSec(e)} vertical className={style.verticalTabs}>
                    {[allInv].concat(list).map(s => getSecTab(s))}
                </Tabs>
            </div>
            <Container className={style.fabContainer}>
                <Button
                    tooltip="Добавить предмет"
                    onClick={() => dialogueService.setState(Dialog.InvItemAdd, {
                        open: true,
                        context: {
                            list: list.map(e => {return {icon: e.icon, id: e.id, name: e.name}}),
                            currInv: sec
                        }
                    })}
                    styles={{backgroundColor: lightColors.teal, color: lightColors.white}}>
                    <Icon icon={"plus"} iconSize={30}/>
                </Button>
            </Container>
        </div>
}

const ItemList = ({items, tab}) => {
    let list = useSelector(store => Object.values(store.overview.items || {}))

    let navigate = useNavigate()
    return <div className={style.itemList}>
        {items.map(item => {
            return <ItemElement navigate={navigate} list={list} key={item.id} item={item} tab={tab}/>
        })}
    </div>
}

const TypesTab = ({sec}) => {
    let getTabTitle = cat => {
        return <div className={style.icon}>
            <ReactSVG src={itemCats[cat]} beforeInjection={(svg) => {svg.setAttribute('style',
                'width: 28px; ' +
                'height: 28px; ' +
                'fill: #515151')}}/>
        </div>
    }

    let [type, setType] = useState("ALL")
    let cats = [...(new Set(sec.items.map(i => i.type)))]
    let catTabs = cats.map(cat => <Tab key={cat+sec.id} id={cat} title={
            getTabTitle(cat)} panel={<ItemList tab={cat} items={sec.items.filter(e => e.type === cat)}/>
        }/>)
    let allCats = <Tab key={"ALL"} id={"ALL"} title={
        getTabTitle("ALL")} panel={<ItemList tab={"ALL"} items={sec.items}/>
    }/>

    return <div className={"items-overview-types-container"}>
        <Tabs selectedTabId={type} onChange={e => setType(e)} className={style.horizontalTabs}>
            {[allCats].concat(catTabs)}
        </Tabs>
    </div>;
}

const getSecTab = sec => {
    let getTabTitle = sec => <div className={style.icon}>
        <ReactSVG src={SvgVault[sec.icon]} beforeInjection={(svg) => {svg.setAttribute('style',
            'width: 35px; ' +
            'height: 35px; ' +
            'fill: #515151')}}/>
        <span>{sec.name}</span>
    </div>
    return <Tab key={sec.id} id={sec.id} title={getTabTitle(sec)} panel={
        <TypesTab sec={sec}/>
    }/>
}

const ItemElement = ({item, tab, list, navigate}) => {
    return (
        <ContextMenu2 content={
            <Menu>
                <MenuItem icon={"move"} onClick={() => {
                    dialogueService.setState(Dialog.InvItemMove, {
                        open: true,
                        context: {
                            list: list
                                .map(e => {return {icon: e.icon, id: e.id, name: e.name}})
                                .filter(e => e.id !== item.invId),
                            item: item,
                            currInvId: item.invId
                        }
                    })
                }} text="Переместить" />
            </Menu>
        }>
            <div className={style.item}
                 onClick={e => dialogueService.setState(Dialog.InvItemChange, {
                     open: true,
                     context: item,
                     invId: item.invId,
                     del: true})}>
                <div className={style.itemCount}>x{item.count}</div>
                {tab === "ALL" && <div className={style.itemIcon}>
                    <ReactSVG src={itemCats[item.type]} beforeInjection={(svg) => {svg.setAttribute('style',
                        'width: 18px; ' +
                        'height: 18px; ' +
                        'fill: #515151')}}/>
                </div>}
                <div className={style.itemName}>{item.name}</div>
                <div className={style.itemAction} onClick={(e) => {
                    e.stopPropagation()
                    e.preventDefault()
                    navigate(`/overview/item/${item.itemId}`)
                }}><Icon icon="eye-open" iconSize={18} /></div>
            </div>
        </ContextMenu2>
    )
}

export default ItemsOverview