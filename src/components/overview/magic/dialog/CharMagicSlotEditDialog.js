import React, {useState} from "react";
import {Button, Dialog, InputGroup, Intent} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Container} from "react-grid-system";
import toasterService from "../../../../logic/services/ToasterService";
import {charMagicService} from "../../../../logic/services/CharMagicService";

const CharMagicSlotEditDialog = ({info, charId, onClose}) => {
    let [level, setLevel] = useState((info && info.level) || 0)
    let [total, setTotal] = useState((info && info.total) || 0)
    let [current, setCurrent] = useState((info && info.current) || 0)

    let [open, setOpen] = useState(true)
    let [loading, setLoading] = useState(false)

    let close = e => { setOpen(false); onClose(e) }

    let save = e => {
        let regex = /^\d+$/;
        let valid = true;
        if (!regex.test(level)) {
            toasterService.error("\"Уровень\" должен быть положительным числом без знака +")
            valid = false
        }
        if (!regex.test(total)) {
            toasterService.error("\"Максимум\" должен быть положительным числом без знака +")
            valid = false
        }
        if (!regex.test(current)) {
            toasterService.error("\"Сейчас\" должен быть положительным числом без знака +")
            valid = false
        }
        if (valid && +current > +total) {
            toasterService.error("Значение \"Сейчас\" не должно быть больше чем \"Максимум\"")
            valid = false
        }
        if (!valid) return
        setLoading(true)
        charMagicService.setCharMagic(charId, +total, +current, +level)
            .catch(() => setLoading(false))
            .then(close)
    }

    return (
        <Dialog isOpen={open}
                isCloseButtonShown
                title={"Информация о ячейках"}
                onClose={close}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <Container style={{width: "100%", position: "relative"}}>
                <br/>
                <span className={"bp3-text-small bp3-text-muted"}>Уровень</span>
                <InputGroup fill value={level} onChange={e => setLevel(e.target.value+"")}/>
                <br/>
                <span className={"bp3-text-small bp3-text-muted"}>Максимум</span>
                <InputGroup fill value={total} onChange={e => setTotal(e.target.value+"")}/>
                <br/>
                <span className={"bp3-text-small bp3-text-muted"}>Сейчас</span>
                <InputGroup fill value={current} onChange={e => setCurrent(e.target.value+"")}/>
                <br/>
                <Button fill intent={Intent.SUCCESS}
                        text={"Сохранить"}
                        onClick={save}/>
            </Container>
        </Dialog>
    )
}

export default CharMagicSlotEditDialog