import React, {useState} from "react";
import {Dialog} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import style2 from "../../../../css/overview/magic/dialog/CharMagicAddSpellDialog.module.css";
import {Container} from "react-grid-system";
import SpellSelect from "../../../common/SpellSelect";
import {charMagicService} from "../../../../logic/services/CharMagicService";

const CharMagicAddSpellDialog = ({charId, onClose}) => {
    let [open, setOpen] = useState(true)

    let close = e => { setOpen(false); onClose(e) }
    let select = spell => { charMagicService.addSpell(charId, spell).then(() => close()) }

    return (
        <Dialog isOpen={open}
                isCloseButtonShown
                title={"Добавить заклинание"}
                onClose={close}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <Container style={{width: "100%", position: "relative"}}>
                <br/>
                <div className={style2.select}>
                    <SpellSelect onSpellSelect={select}/>
                </div>
            </Container>
        </Dialog>
    )
}

export default CharMagicAddSpellDialog