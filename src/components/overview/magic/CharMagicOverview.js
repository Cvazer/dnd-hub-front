import React, {Fragment, useEffect, useState} from "react";
import LoadingView from "../../LoadingView";
import {charMagicService} from "../../../logic/services/CharMagicService";
import {useSelector} from "react-redux";
import {Container} from "react-grid-system";
import CharMagicSlotEditDialog from "./dialog/CharMagicSlotEditDialog";
import {Button, Card, ControlGroup, Divider, Elevation, Icon, Intent, Menu, MenuItem, Tag} from "@blueprintjs/core";
import style from "../../../css/overview/magic/CharMagicOverview.module.css"
import {ContextMenu2} from "@blueprintjs/popover2";
import CharMagicAddSpellDialog from "./dialog/CharMagicAddSpellDialog";
import SpellListItem from "../../cards/SpellListItem";

const CharMagicOverview = () => {
    let [magic, setMagic] = useState()
    let char = useSelector(store => store.overview.char)
    let [edit, setEdit] = useState()
    let [add, setAdd] = useState()


    let openEditSlotInfo = info => {
        setEdit(<CharMagicSlotEditDialog info={info} charId={char.id} onClose={() => {
            setEdit(undefined)
            updateMagic()
        }}/>)
    }

    let openAddSpell = () => {
        setAdd(<CharMagicAddSpellDialog charId={char.id} onClose={() => {
            setAdd(undefined)
            updateMagic()
        }}/>)
    }

    let resetAll = () => {
        charMagicService.resetAll(char.id)
            .then(() => updateMagic())
    }

    let updateMagic = () => charMagicService.getCharMagic(char.id).then(res => {setMagic(res)})

    useEffect(() => { updateMagic() }, [])

    return !magic ? <LoadingView/> : (
        <Fragment>
            {edit && edit}
            {add && add}
            <Container>
                <Button fill minimal intent={Intent.SUCCESS}
                        onClick={openAddSpell}
                        text={"Добавить заклинание"}/>
                <br/>
                <div className={style.cardsContainer}>
                    {Object.values(magic.slots).map(it => {
                        return <SpellSlotCard info={it} key={it.level}
                                              spells={magic.spells}
                                              update={updateMagic}
                                              charId={char.id}
                                              edit={openEditSlotInfo}/>
                    })}
                </div>
                <br/>
                <ControlGroup>
                    <Button fill minimal intent={Intent.NONE}
                            text={"Добавить ячейки"}
                            onClick={() => openEditSlotInfo({})}/>
                    <Button fill minimal intent={Intent.PRIMARY}
                            text={"Восстановить все"}
                            onClick={() => resetAll()}/>
                </ControlGroup>
                <br/>
            </Container>
        </Fragment>
    )
}

const SpellSlotCard = ({spells, info, update, edit, charId}) => {


    let getIntent = () => {
        if (info.current === 0) return Intent.DANGER
        if (info.current > info.total / 2) return Intent.SUCCESS
        if (info.current <= info.total / 2) return Intent.WARNING
    }

    let deleteSlots = () => {
        charMagicService.deleteMagicSlots(charId, info.level)
            .then(() => update())
    }

    let switchPrepared = spell => () => {
        charMagicService.setPrepared(charId, spell.id, !spell.prepared)
            .then(() => update())
    }

    let deleteSpell = spell => () => {
        charMagicService.removeSpell(charId, spell.id)
            .then(() => update())
    }

    return (
        <Fragment>
            <Card elevation={Elevation.ONE}>
                <ContextMenu2 content={
                    <Menu>
                        <MenuItem text={"Удалить ячейки"}
                                  onClick={deleteSlots}
                                  icon={"trash"}/>
                    </Menu>
                }>
                    <div className={style.slotCardTagsContainer}>
                        <Tag large minimal className={style.cardLevel}>{info.level} Уровень</Tag>
                        <Tag large minimal interactive fill intent={getIntent()} onClick={() => edit(info)}>
                            Осталось ячеек {info.current} из {info.total}
                        </Tag>
                        <Tag minimal interactive intent={Intent.PRIMARY}
                             onClick={() => {charMagicService.useSlot(charId, info).then(() => update())}}
                             className={style.cardAction}>
                            <Icon icon={"confirm"}/>
                        </Tag>
                        <Tag minimal interactive
                             onClick={() => {charMagicService.restoreSlot(charId, info).then(() => update())}}
                             className={style.cardAction}>
                            <Icon icon={"refresh"}/>
                        </Tag>
                    </div>
                </ContextMenu2>
                <div className={style.cardSpacer}/>
                <Divider/>
                {
                    spells && spells
                        .filter(s => s.level === info.level)
                        .sort((a, b) => a.name.localeCompare(b.name))
                        .sort((a, b) => (a.prepared?0:1) - (b.prepared?0:1) )
                        .map(s => {
                            return <ContextMenu2 key={s.id}
                                                 content={
                                                     <Menu>
                                                         <MenuItem text={"Удалить"}
                                                                   icon={"trash"}
                                                                   onClick={deleteSpell(s)}/>
                                                     </Menu>
                                                 }>
                                <SpellListItem
                                    skip={["level"]}
                                    onPrepared={switchPrepared(s)}
                                    spell={s}/>
                            </ContextMenu2>
                        })
                }
            </Card>
        </Fragment>
    )
}

export default CharMagicOverview