import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import Counter from "../../../common/Counter";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {changeHdDialogService as service} from "../../../../logic/services/dialog/char/ChangeHdDialogService";

const ChangeHitDieDialog = () => {
    let char = useSelector(store => store.overview.char);
    let state = useSelector(store => store.dialog[DG.OverviewHd]);

    let { context, open } = state;
    let [val, setVal] = useState("");
    let [loading, setLoading] = useState(false);

    let close = () => {dialogueService.setState(DG.OverviewHd, {open: false, context: undefined})};

    useEffect(() => setVal(context && context.current), [state]);

    return (
        <Dialog isOpen={open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={"Изменить параметр"}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <Counter callback={service.checkMax((context || {}).max || 1, setVal)} value={val}/>
                <br/>
                <ButtonGroup fill>
                    <Button intent={Intent.SUCCESS}
                            loading={loading}
                            onClick={() => service.save(val, context.die, setLoading)}
                            >Сохранить</Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

export default ChangeHitDieDialog