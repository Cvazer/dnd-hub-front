import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import Counter from "../../../common/Counter";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {changeSpeedDialogService as service} from "../../../../logic/services/dialog/char/ChangeSpeedDialogService";

const ChangeSpeedDialog = () => {
    let char = useSelector(store => store.overview.char);
    let state = useSelector(store => store.dialog[DG.OverviewSpeed]);

    let [val, setVal] = useState(char.speed);
    let [loading, setLoading] = useState(false);

    let close = () => {dialogueService.close(DG.OverviewSpeed)};

    useEffect(() => setVal(char.speed), [state]);

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={"Изменить скорость"}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <Counter callback={setVal} value={val} step={5}/>
                <br/>
                <ButtonGroup fill>
                    <Button intent={Intent.SUCCESS}
                            loading={loading}
                            onClick={() => service.save(val, setLoading)}>Сохранить</Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

export default ChangeSpeedDialog