import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog, InputGroup} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {changeExpDialogService as service} from "../../../../logic/services/dialog/char/ChangeExpDialogService";

const ChangeExpDialog = () => {
    let state = useSelector(store => store.dialog[DG.OverviewExp]);

    let [val, setVal] = useState("");
    let [loading, setLoading] = useState(false);

    let close = () => {dialogueService.close(DG.OverviewExp)};

    useEffect(() => setVal(""), [state]);

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={"Получить опыт"}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <InputGroup value={val} placeholder={"Количество..."} onChange={e => setVal(e.target.value)}/>
                <br/>
                <ButtonGroup fill>
                    <Button intent={Intent.SUCCESS}
                            loading={loading}
                            onClick={() => service.save(val, setLoading)}>Получить</Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

export default ChangeExpDialog