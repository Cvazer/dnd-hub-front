import React, {useEffect, useState} from "react";
import {Button, Dialog, Divider, InputGroup} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {changeAttackDialogService as service} from "../../../../logic/services/dialog/char/ChangeAttackDialogService";
import context from "react-redux/lib/components/Context";

const ChangeAttackDialog = () => {
    let state = useSelector(store => store.dialog[DG.OverviewAttack]);

    let { attack, list } = state.context || {};
    let [attackBonus, setAttackBonus] = useState("");
    let [damageBase, setDamageBase] = useState("");
    let [damageBonus, setDamageBonus] = useState("");
    let [name, setName] = useState("");

    let [loading, setLoading] = useState(false);

    let close = () => {dialogueService.setState(DG.OverviewAttack, {open: false, context: undefined})};

    useEffect(() => {
        setAttackBonus(attack && attack.attackBonus !== null ? attack.attackBonus : "");
        setDamageBase(attack && attack.damageBase || "");
        setDamageBonus(attack && attack.damageBonus !== null ? attack.attackBonus : "");
        setName(attack && attack.name || "");
    }, [state]);

    return (
        <div>
            <Dialog isOpen={state.open}
                    usePortal
                    onClose={() => close()}
                    icon={"edit"}
                    isCloseButtonShown
                    title={"Изменить действие"}
                    className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
                <div className={style.hpDialogContainer}>
                    <div>
                        <InputGroup value={name} onChange={e => setName(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Название</span>
                        <br/>
                        <br/>
                        <InputGroup value={attackBonus} onChange={e => setAttackBonus(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Бонус попадания</span>
                        <br/>
                        <br/>
                        <InputGroup value={damageBase} onChange={e => setDamageBase(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Базовый урон</span>
                        <br/>
                        <br/>
                        <InputGroup value={damageBonus} onChange={e => setDamageBonus(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Бонусный урон</span>
                        <br/>
                        <br/>
                        <Divider/>
                        <br/>
                        {attack && attack.id && <Button minimal fill intent={Intent.DANGER} icon={"trash"}
                                                        loading={loading}
                                                        onClick={() => service.save(
                                                            attack, list.filter(e => e.id !== attack.id),
                                                            attackBonus, damageBase,
                                                            damageBonus, name,
                                                            setLoading
                                                        )}
                                                        >Удалить</Button>}
                    </div>
                    <br/>
                    <Button fill intent={Intent.SUCCESS} icon={"tick"}
                            loading={loading}
                            onClick={() => service.save(
                                attack, list,
                                attackBonus, damageBase,
                                damageBonus, name,
                                setLoading
                            )}>Сохранить</Button>
                </div>
            </Dialog>
        </div>
    )
};

export default ChangeAttackDialog