import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import Counter from "../../../common/Counter";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {changeStatDialogService as service} from "../../../../logic/services/dialog/char/ChangeStatDialogService";

const ChangeStatDialog = () => {
    let char = useSelector(store => store.overview.char);
    let state = useSelector(store => store.dialog[DG.OverviewStat]);

    let stat = state.context
    let [val, setVal] = useState("");
    let [loading, setLoading] = useState(false);

    let close = () => dialogueService.setState(DG.OverviewStat, {open: false, context: undefined});

    useEffect(() => setVal(stat && stat.value), [state]);

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Изменить параметр`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <Counter value={val} callback={setVal}/>
                <br/>
                <ButtonGroup fill>
                    <Button loading={loading} intent={Intent.SUCCESS}
                            onClick={() => service.setStat(val, stat.id, setLoading)}>
                        Сохранить
                    </Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

export default ChangeStatDialog