import React, {Fragment, useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog, Divider, InputGroup} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import {Money} from "../../../../Utils";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {changeMoneyDialogService as service} from "../../../../logic/services/dialog/char/ChangeMoneyDialogService";

const ChangeMoneyDialog = () => {
    let char = useSelector(store => store.overview.char);
    let state = useSelector(store => store.dialog[DG.OverviewMoney]);

    let money = new Money(char.pp, char.gp, char.sp, char.cp)

    let [platinum, setPlatinum] = useState(money.platinum);
    let [gold, setGold] = useState(money.gold);
    let [silver, setSilver] = useState(money.silver);
    let [copper, setCopper] = useState(money.copper);

    let [val, setVal] = useState("");
    let [loading, setLoading] = useState(false);

    let close = () => {dialogueService.close(DG.OverviewMoney)};

    useEffect(() => {
        setPlatinum(money.platinum);
        setGold(money.gold);
        setSilver(money.silver);
        setCopper(money.copper);
        setVal("");
    }, [state]);

    return (
        <div>
            <Dialog isOpen={state.open}
                    usePortal
                    onClose={() => close()}
                    icon={"edit"}
                    isCloseButtonShown
                    title={"Изменить деньги"}
                    className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
                <div className={style.hpDialogContainer}>
                    <InputGroup value={val} placeholder={"Количество..."} onChange={e => {setVal(e.target.value);}}/>
                    <br/>
                    <ButtonGroup fill minimal style={{border: "1px solid lightgrey", borderRadius: "5px"}}>
                        {loading ? (
                            <Button loading={true}/>
                        ) : (
                            <Fragment>
                                <Button intent={Intent.SUCCESS}
                                        onClick={() => service.get(val, setLoading)}>Получить</Button>
                                <Button intent={Intent.DANGER}
                                        onClick={() => service.spend(val, setLoading)}>Потратить</Button>
                            </Fragment>
                        )}
                    </ButtonGroup>
                    <br/>
                    <Divider/>
                    <div>
                        <h3>Текущие значения</h3>
                        <InputGroup value={platinum} onChange={e => setPlatinum(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Платина</span>
                        <br/>
                        <br/>
                        <InputGroup value={gold} onChange={e => setGold(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Золото</span>
                        <br/>
                        <br/>
                        <InputGroup value={silver} onChange={e => setSilver(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Серебро</span>
                        <br/>
                        <br/>
                        <InputGroup value={copper} onChange={e => setCopper(e.target.value)}/>
                        <span className={"bp3-text-small bp3-text-muted"}>Медь</span>
                    </div>
                    <br/>
                    <Button fill intent={Intent.SUCCESS} icon={"tick"}
                            loading={loading}
                            onClick={() => service.save(platinum, gold, silver, copper, setLoading)}>Сохранить</Button>
                </div>
            </Dialog>
        </div>
    )
};

export default ChangeMoneyDialog