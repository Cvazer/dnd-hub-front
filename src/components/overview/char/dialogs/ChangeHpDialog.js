import React, {Fragment, useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog, Divider, InputGroup} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {changeHpDialogService as service} from "../../../../logic/services/dialog/char/ChangeHpDialogService";

const ChangeHpDialog = () => {
    let char = useSelector(store => store.overview.char);
    let state = useSelector(store => store.dialog[DG.OverviewHp]);

    let [current, setCurrent] = useState(char.hpCurrent);
    let [max, setMax] = useState(char.hpMax);
    let [tmp, setTmp] = useState(char.hpTmpCurrent);
    let [val, setVal] = useState("");
    let [loading, setLoading] = useState(false);

    let close = () => {dialogueService.close(DG.OverviewHp)};

    useEffect(() => {
        setCurrent(char.hpCurrent);
        setMax(char.hpMax);
        setTmp(char.hpTmpCurrent);
        setVal("");
    }, [state]);

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={"Изменить хиты"}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <InputGroup value={val} placeholder={"Количество..."}
                            onChange={e => {setVal(e.target.value)}}/>
                <br/>
                <ButtonGroup fill minimal style={{border: "1px solid lightgrey", borderRadius: "5px"}}>
                    {loading ? (
                        <Button loading={true}>Урон</Button>
                    ) : (
                        <Fragment>
                            <Button intent={Intent.DANGER}
                                    onClick={_ => {service.damage(val, setLoading)}}>Урон</Button>
                            <Button intent={Intent.SUCCESS}
                                    onClick={_ => {service.heal(val, setLoading)}}>Лечение</Button>
                        </Fragment>
                    )}
                </ButtonGroup>
                <br/>
                <Divider/>
                <div>
                    <h3>Хиты</h3>
                    <InputGroup value={current} onChange={e => setCurrent(e.target.value)}/>
                    <span className={"bp3-text-small bp3-text-muted"}>Текущее значение</span>
                    <br/>
                    <br/>
                    <InputGroup value={max} onChange={e => setMax(e.target.value)}/>
                    <span className={"bp3-text-small bp3-text-muted"}>Максимальное значение</span>
                </div>
                <br/>
                <Divider/>
                <div>
                    <h3>Временные хиты</h3>
                    <InputGroup value={tmp} onChange={e => setTmp(e.target.value)}/>
                    <span className={"bp3-text-small bp3-text-muted"}>Текущее значение</span>
                </div>
                <br/>
                <Divider/>
                <br/>
                <Button fill intent={Intent.SUCCESS} icon={"tick"} loading={loading}
                        onClick={() => service.save(current, max, tmp, setLoading)}>Сохранить</Button>
            </div>
        </Dialog>
    )
};

export default ChangeHpDialog