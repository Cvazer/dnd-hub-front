import React, {Fragment, useEffect, useState} from "react";
import {CircularProgressbar} from "react-circular-progressbar";
import 'react-circular-progressbar/dist/styles.css';
import {Elevation} from "@blueprintjs/core/lib/cjs/common/elevation";
import {Button, Card, Checkbox, Divider, HTMLTable, Icon, Tag} from "@blueprintjs/core";
import {Container} from 'react-grid-system'
import style from '../../../css/CharOverview.module.css'
import cn from 'classnames'
import ChangeHpDialog from "./dialogs/ChangeHpDialog";
import ChangeExpDialog from "./dialogs/ChangeExpDialog";
import ChangeStatDialog from "./dialogs/ChangeStatDialog";
import ChangeHitDieDialog from "./dialogs/ChangeHitDieDialog";
import {Money} from "../../../Utils";
import {useSelector} from "react-redux";
import ChangeAcDialog from "./dialogs/ChangeAcDialog";
import ChangeSpeedDialog from "./dialogs/ChangeSpeedDialog";
import ChangeMoneyDialog from "./dialogs/ChangeMoneyDialog";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import ChangeAttackDialog from "./dialogs/ChangeAttackDialog";
import charService from "../../../logic/services/CharService";
import LoadingView from "../../LoadingView";
import dialogueService, {Dialog} from "../../../logic/services/DialogService";
import ChangeInitDialog from "./dialogs/ChangeInitDialog";
import listItemStyle from "../../../css/cards/AttackActionListItem.module.css";
import {changeStatDialogService} from "../../../logic/services/dialog/char/ChangeStatDialogService";

const CharOverview = ({match}) => {
    let char = useSelector(store => store.overview.char);
    useEffect(() => {if (!char) charService.load(match.params.id).then(_ => {})}, [char])

    useEffect(() => {window.scrollTo(0, 0)}, [])

    if (!char) return <LoadingView/>
    return <div style={{textAlign: "center"}}>
        <Dialogs/>
        <Container>
            <InfoSection/>
            <MoneySection/>
            <AttacksSection/>
            <StatsSection/>
        </Container>
    </div>
};

const MoneySection = () => {
    let char = useSelector(store => store.overview.char);
    let money = new Money(char.pp, char.gp, char.sp, char.cp)
    return (
        <Fragment>
            <Card elevation={Elevation.TWO} style={{cursor: "pointer"}}>
                <div className={style.walletContainer} onClick={() => dialogueService.open(Dialog.OverviewMoney)}>
                    <div className={style.coin}>
                        <span className={cn("bp3-text-small bp3-text-muted", style.coinLabel)}>Платина</span>
                        <Icon icon={"ring"} className={style.coinPlatinum} iconSize={25}/>
                        <span className={style.coinValue}>{money.platinum}</span>
                    </div>
                    <div className={style.coin}>
                        <span className={cn("bp3-text-small bp3-text-muted", style.coinLabel)}>Золото</span>
                        <Icon icon={"ring"} className={style.coinGold} iconSize={25}/>
                        <span className={style.coinValue}>{money.gold}</span>
                    </div>
                    <div className={style.coin}>
                        <span className={cn("bp3-text-small bp3-text-muted", style.coinLabel)}>Серебро</span>
                        <Icon icon={"ring"} className={style.coinSilver} iconSize={25}/>
                        <span className={style.coinValue}>{money.silver}</span>
                    </div>
                    <div className={style.coin}>
                        <span className={cn("bp3-text-small bp3-text-muted", style.coinLabel)}>Медь</span>
                        <Icon icon={"ring"} className={style.coinCopper} iconSize={25}/>
                        <span className={style.coinValue}>{money.copper}</span>
                    </div>
                </div>
            </Card>
            <br/>
            <br/>
        </Fragment>
    )
};

const StatsSection = () => {
    let char = useSelector(store => store.overview.char);
    let [disabled, setDisabled] = useState(false);
    let order = {STR: 1, DEX: 2, CON: 3, INT: 4, WIS: 5, CHA: 6}
    return (
        <Fragment>
            <Card elevation={Elevation.TWO}>
                <div>
                    {char.stats
                        .map(e => {return {...e, order: order[e.id]}})
                        .sort((s1, s2) => s1.order - s2.order)
                        .map(stat => <StatBlock stat={stat}
                                                disabled={disabled}
                                                setDisabled={setDisabled}
                                                key={stat.id}/>)}
                </div>
            </Card>
            <br/>
            <br/>
        </Fragment>
  )
};

const InfoSection = () => {
    let char = useSelector(store => store.overview.char);
    return (
        <div className={style.infoSection}>
            <Tag minimal large className={style.charNameTag}>
                <span className={style.charName}>{char.name}</span>
            </Tag>
            <Card elevation={Elevation.TWO}>
                <div className={style.infoSectionContainer}>
                    <HpBlock/>
                    <CommonStatBlock/>
                </div>
                <br/>
                <Divider/>
                <br/>
                <div className={style.infoSectionContainer}>
                    <ExpBlock/>
                    <CommonInfoBlock/>
                </div>
                <br/>
                <Divider/>
                <br/>
                <div className={style.infoSectionContainer}>
                    <div style={{display: "flex", justifyContent: "center", width: "100%", alignItems: "center"}}>
                        <span className={"bp3-text-large"} style={{paddingRight: "10px"}}>Бонус владения:</span>
                        <Tag large minimal fill style={{width: "20%"}}>{char.prof>=0?"+":"-"}{char.prof}</Tag>
                    </div>
                </div>
            </Card>
            <br/>
            <br/>
        </div>
    )
};

const AttacksSection = () => {
    let char = useSelector(store => store.overview.char);
    return (
        <Fragment>
            <Card elevation={Elevation.TWO}>
                <Fragment>
                    {char.attacks && char.attacks.sort((a1, a2) => a1.id - a2.id).map(attack => {
                        return <div className={listItemStyle.root} key={attack.id}>
                            <div className={listItemStyle.header}>
                                <div className={"bp3-text-large bp3-text-muted"}>{attack.name}</div>
                                <Icon icon={"annotation"} iconSize={18}
                                      onClick={() => dialogueService.setState(Dialog.OverviewAttack, {
                                          open: true,
                                          context: {
                                              attack: attack,
                                              list: char.attacks
                                          }
                                      })}
                                      className={cn(listItemStyle.editIcon, "bp3-text-muted")}/>
                            </div>
                            <div className={listItemStyle.body}>
                                <div className={listItemStyle.attack}>
                                    <div className={listItemStyle.iconContainer}>
                                        <div className={listItemStyle.icon}>
                                            <i className={cn("ra ra-sword bp3-text-muted")} style={{fontSize: "25px"}}/>
                                            <div className={cn(listItemStyle.value, "bp3-text-large")}>
                                                {attack.attackBonus>=0?"+":"-"}{attack.attackBonus}
                                            </div>
                                        </div>
                                        <span className={cn(listItemStyle.iconText, "bp3-text-small bp3-text-disabled")}>Бонус попадания</span>
                                    </div>
                                </div>
                                <div className={listItemStyle.baseDamage}>
                                    <div className={listItemStyle.iconContainer}>
                                        <div className={listItemStyle.icon}>
                                            <i className={cn("ra ra-bleeding-hearts bp3-text-muted")} style={{fontSize: "25px"}}/>
                                            <div className={cn(listItemStyle.value, "bp3-text-large")}>{attack.damageBase}</div>
                                        </div>
                                        <span className={cn(listItemStyle.iconText, "bp3-text-small bp3-text-disabled")}>Базовый урон</span>
                                    </div>
                                </div>
                                <div className={listItemStyle.bonusDamage}>
                                    <div className={listItemStyle.iconContainer}>
                                        <div className={listItemStyle.icon}>
                                            <i className={cn("ra ra-explosion bp3-text-muted")} style={{fontSize: "25px"}}/>
                                            <div className={cn(listItemStyle.value, "bp3-text-large")}>
                                                {attack.damageBonus>=0?"+":"-"}{attack.damageBonus}
                                            </div>
                                        </div>
                                        <span className={cn(listItemStyle.iconText, "bp3-text-small bp3-text-disabled")}>Бонусный урон</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    })}
                    <Button fill minimal intent={Intent.SUCCESS}
                            onClick={() => dialogueService.setState(Dialog.OverviewAttack, {
                                open: true,
                                context: {
                                    attack: {},
                                    list: char.attacks
                                }
                            })}>
                        Добавить действие
                    </Button>
                </Fragment>
            </Card>
            <br/>
            <br/>
        </Fragment>
    );
};

const StatBlock = ({stat, disabled, setDisabled}) => {
    return (
        <div className={style.statCard}>
            <div className={style.statContainer}>
                <span className={style.statName+" bp3-text-muted"}>{stat.name}</span>
                <Tag minimal className={style.statMod}>{stat.value}</Tag>
            </div>
            <div className={style.skillContainer}>
                <h2 className={cn(style.skillValue, "bp3-text-muted")}
                    onClick={() => dialogueService.setState(Dialog.OverviewStat, {open: true, context: stat})}>
                    {stat.bonus >= 0 ? "+" : ""}{stat.bonus}
                </h2>
                <div className={style.skillList}>
                    <div className={style.skillBox}>
                        <Checkbox checked={stat.save} className={style.skillCheckBox}
                                  disabled={disabled}
                                  onClick={e => changeStatDialogService.setSave(e.target.checked, stat.id, setDisabled)}
                        />
                        <div className={style.skillMod}>{stat.saveBonus >= 0 ? "+" : ""}{stat.saveBonus}</div>
                        <div className={style.skillName}>Спас. бросок</div>
                    </div>
                    {stat.skills.sort((s1, s2) => s1.id - s2.id).map(skill =>
                        <div className={style.skillBox} key={skill.id}>
                            <Checkbox checked={skill.prof} className={style.skillCheckBox}
                                      disabled={disabled}
                                      onClick={e => changeStatDialogService.setProf(
                                          e.target.checked, skill.id, setDisabled
                                      )}/>
                            <div className={style.skillMod}>{skill.bonus >= 0 ? "+" : ""}{skill.bonus}</div>
                            <div className={style.skillName}>{skill.nameEng}</div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
};

const HpBlock = () => {
    let char = useSelector(store => store.overview.char);
    if (!char) return <div/>
    return (
        <div className={style.hpSection} onClick={() => dialogueService.open(Dialog.OverviewHp)}>
            <h3 className="bp3-heading bp3-text-muted" style={{fontWeight: "normal"}}>Хиты:</h3>
            <CircularProgressbar
                value={char.hpCurrent}
                text={`${char.hpCurrent + "/" + char.hpMax}`}
                strokeWidth={10}
                maxValue={char.hpMax}
                styles={{
                    root: {width: "100%"},
                    text: {fontSize: '16px', fill: "rgb(245, 80, 80)"},
                    path: {stroke: 'rgb(245, 80, 80)'},
                    trail: {stroke: 'rgba(0, 0, 0, 0.1)'}
                }}
            />
            <br/>
            <br/>
            <div style={{margin: "auto"}}>
                <Tag minimal large style={{width: "50%"}}>{char.hpTmpCurrent}</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Временные хиты</div>
            </div>
        </div>
    )
};

const ExpBlock = () => {
    let char = useSelector(store => store.overview.char);
    const currentExp = char.exp;
    const nextExp = char.expNext;
    const prevExp = char.expPrev;
    const need = nextExp-prevExp;
    const got = need - (nextExp - currentExp);
    const left = nextExp - currentExp;

    return (
        <div className={style.hpSection} onClick={() => dialogueService.open(Dialog.OverviewExp)}>
            <h3 className="bp3-heading bp3-text-muted" style={{fontWeight: "normal"}}>Опыт:</h3>
            <div style={{transform: "translateY(-10px)"}}
                 className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>
                Cлед. уровень: {(nextExp).toLocaleString().replace(/,/g," ",)}
            </div>
            <CircularProgressbar
                value={got}
                text={`${left}`}
                strokeWidth={10}
                maxValue={need}
                styles={{
                    root: {width: "100%"},
                    text: {fontSize: '16px', fill: "rgb(49, 193, 98)"},
                    path: {stroke: 'rgb(49, 193 , 98)'},
                    trail: {stroke: 'rgba(0, 0, 0, 0.1)'}
                }}
            />
            <br/>
            <br/>
            <div style={{margin: "auto"}}>
                <Tag minimal large fill>{(char.exp).toLocaleString().replace(/,/g," ",)}</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Текущий опыт</div>
            </div>
        </div>
    )
};

const CommonStatBlock = () => {
    let char = useSelector(store => store.overview.char);
    if (!char) return <div/>
    return (
        <div className={style.commonStatSection}>
            <div className={style.commStatSectionContainer}>
                <Tag minimal large fill style={{cursor: "pointer"}}
                     onClick={() => dialogueService.open(Dialog.OverviewAc)}>{char.ac}</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Класс брони</div>
            </div>
            <div className={style.commStatSectionContainer}>
                <Tag minimal large fill style={{cursor: "pointer"}}
                     onClick={() => dialogueService.open(Dialog.OverviewInit)}>+{char.initiative}</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Инициатива</div>
            </div>
            <div className={style.commStatSectionContainer}>
                <Tag minimal large fill style={{cursor: "pointer"}}
                     onClick={() => dialogueService.open(Dialog.OverviewSpeed)}>{char.speed} фт.</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Скорость</div>
            </div>
            <div>
                <HTMLTable condensed striped style={{display: "flex", flexFlow: "column"}}>
                    <thead>
                    <tr style={{display: "flex", width: "100%", alignItems: "center"}}>
                        <th style={{display: "inline-block", width: "50%", overflowWrap: "break-word"}}>
                            <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                Кость
                            </div>
                        </th>
                        <th style={{display: "inline-block", width: "50%", overflowWrap: "break-word"}}>
                            <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                Кол-во
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {char.hd.sort((o1, o2) => o2["die"].localeCompare(o1["die"])).map(obj =>
                        <tr key={obj["die"]} className={style.dieRow} style={{display: "flex"}}
                            onClick={() => dialogueService.openState(Dialog.OverviewHd, obj)}>
                            <td style={{textAlign: "center", display: "inline-block", width: "50%"}}>
                                <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                    {obj["die"]}
                                </div>
                            </td>
                            <td style={{textAlign: "center", display: "inline-block", width: "50%"}}>
                                <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                    {obj["current"]}/{obj["max"]}
                                </div>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </HTMLTable>
            </div>
        </div>
    )
};

const CommonInfoBlock = () => {
    let char = useSelector(store => store.overview.char);
    return (
        <div className={style.commonStatSection}>
            <div className={style.commStatSectionContainer}>
                <Tag minimal large fill>{char.raceName}</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Раса</div>
            </div>
            <div className={style.commStatSectionContainer}>
                <Tag minimal large fill>{char.backgroundName}</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Предыстория</div>
            </div>
            <div className={style.commStatSectionContainer}>
                <Tag minimal large fill>{char.playerName}</Tag>
                <div className={cn(style.commStatSectionLabel, "bp3-text-small", "bp3-text-muted")}>Игрок</div>
            </div>
            <div style={{width: "100%"}}>
                <HTMLTable condensed striped style={{display: "flex", flexFlow: "column"}}>
                    <thead>
                    <tr style={{display: "flex", width: "100%", alignItems: "center"}}>
                        <th style={{display: "inline-block", width: "50%", overflowWrap: "break-word"}}>
                            <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                Класс
                            </div>
                        </th>
                        <th style={{display: "inline-block", width: "50%", overflowWrap: "break-word"}}>
                            <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                Уровень
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {char.classes.sort((o1, o2) => o1.name.localeCompare(o2.name)).map(charClass =>
                        <tr key={charClass.id} style={{display: "flex"}}>
                            <td style={{textAlign: "center", display: "inline-block", width: "50%"}}>
                                <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                    {charClass.name}
                                </div>
                            </td>
                            <td style={{textAlign: "center", display: "inline-block", width: "50%"}}>
                                <div className={cn(style.classTableSpan, "bp3-text-overflow-ellipsis")}>
                                    {charClass.lvl}
                                </div>
                            </td>
                        </tr>
                    )}
                    </tbody>
                </HTMLTable>
            </div>
        </div>
    )
};

const Dialogs = () => <Fragment>
        <ChangeHpDialog/>
        <ChangeExpDialog/>
        <ChangeStatDialog/>
        <ChangeHitDieDialog/>
        <ChangeAcDialog/>
        <ChangeInitDialog/>
        <ChangeSpeedDialog/>
        <ChangeMoneyDialog/>
        <ChangeAttackDialog/>
    </Fragment>;

export default CharOverview