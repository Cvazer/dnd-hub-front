import React, {Fragment, useEffect, useState} from "react";
import {useSelector} from "react-redux";
import CharSelectView from "./CharSelectView";
import {Navigate, Route, Routes, useLocation} from "react-router";
import ItemsOverview from "./items/ItemsOverview";
import CharOverview from "./char/CharOverview";
import charService from "../../logic/services/CharService";
import TraitsOverview from "./traits/TraitsOverview";
import LoadingView from "../LoadingView";
import ProfsOverview from "./profs/ProfsOverview";
import NotesOverview from "./notes/NotesOverview";
import CharMagicOverview from "./magic/CharMagicOverview";

const Overview = () => {
    let char = useSelector(store => store.overview.char);
    let [loading, setLoading] = useState(true)
    let location = useLocation()

    useEffect(() => {
        if (char) setLoading(false)
        if (location.pathname === "/overview" || location.pathname === "/overview/") {setLoading(false)}
        else {
            if (!location.pathname.match(/\/overview\/[^/]+\/([0-9]+)/)) return
            let id = location.pathname.match(/\/overview\/[^/]+\/([0-9]+)/)[1]
            if (!char) charService.load(id)
        }
    })
    // return <div>asd</div>

    return loading ? <LoadingView/> : location.pathname === '/overview/' ? <CharSelectView/> : char ? <Fragment>
            <Routes>
                <Route path="items/:id" exact element={<ItemsOverview/>}/>
                <Route path="char/:id" exact element={<CharOverview/>}/>
                <Route path="traits/:id" exact element={<TraitsOverview/>}/>
                <Route path="profs/:id" exact element={<ProfsOverview/>}/>
                <Route path="notes/:id" exact element={<NotesOverview/>}/>
                <Route path="magic/:id" exact element={<CharMagicOverview/>}/>
                <Route path="*" element={<div/>}/>
            </Routes>
        </Fragment> : <Navigate replace to={"/overview/"}/>
}

export default Overview