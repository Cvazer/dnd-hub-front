import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import service from "../../../logic/services/TraitService";
import LoadingView from "../../LoadingView";
import {Container as GridContainer} from 'react-grid-system'
import style from "../../../css/overview/traits/TraitsOverview.module.css";
import style2 from '../../../css/overview/items/ItemsOverview.module.css'
import {Button, Card, Divider, Elevation, Icon, InputGroup, Intent, Menu, MenuItem, Tag} from "@blueprintjs/core";
import {ContextMenuTarget} from "@blueprintjs/core/lib/esnext/components/context-menu/contextMenuTarget";
import {textHtml} from "../../../App";
import {useNavigate} from "react-router";
import dialogueService, {Dialog} from "../../../logic/services/DialogService";
import {Button as FabButton, Container, lightColors} from 'react-floating-action-button'
import TraitFilterDialog from "./dialogs/TraitFilterDialog";
import {dispatch} from "../../../store/ConfigureStore";
import {AccountReducerAction, initialState} from "../../../store/reducers/AccountReducer";
import AddFeatureDialog from "./dialogs/AddFeatureDialog";

const TraitOverview = () => {
    let char = useSelector(store => store.overview.char)
    let [loading, setLoading] = useState(true)
    let [search, setSearch] = useState("")

    let list = useSelector(store => store.overview.traits || [])
    let filter = useSelector(store => store.account.traitsFilter || {})

    useEffect(() => {return () => {dispatch({
        type: AccountReducerAction.TraitsFilterSet,
        payload: initialState.traitsFilter
    })}}, [])
    useEffect(() => {window.scrollTo(0, 0)}, [])

    useEffect(() => {service.load(char.id).then(() => {setLoading(false)})}, [char])
    useEffect(() => {service.load(char.id)}, [filter])

    return loading ? <div className={style.fix}><LoadingView/></div> : <div className={style.root}>
        <TraitFilterDialog/>
        <AddFeatureDialog/>
        <GridContainer>
            <div className={style.topFiller}/>
            <div className={style.search}>
                <InputGroup leftIcon={"search"} value={search} type={"search"}
                            onChange={e => {window.scrollTo(0, 0); setSearch(e.target.value)}}
                            placeholder={"Поиск..."}/>
            </div>
            <div className={style.cardContainer}>
                {
                    applyFilter(list, filter)
                        .filter(e => !search ? true : search === "" ? true
                            : e.trait.name.toUpperCase().includes(search.toUpperCase()))
                        .map(e => <TraitCard char={char} holder={e} key={e.trait.id} trait={e.trait} origin={e.origin}/>)
                }
                <Container className={style2.fabContainerFixed}>
                    <FabButton
                        tooltip="Добавить черту"
                        onClick={() => {dialogueService.open(Dialog.TraitsAddFeature)}}
                        styles={{backgroundColor: lightColors.teal, color: lightColors.white}}>
                        <Icon icon={"plus"} iconSize={20}/>
                    </FabButton>
                    <FabButton
                        tooltip="Фильтр"
                        onClick={() => {dialogueService.setState(Dialog.TraitsFilter, {
                            open: true,
                            context: filter
                        })}}
                        styles={{backgroundColor: lightColors.teal, color: lightColors.white}}>
                        <Icon icon={"filter"} iconSize={30}/>
                    </FabButton>
                </Container>
            </div>
            <br/>
            <br/>
            <br/>
        </GridContainer>
    </div>
}

const applyFilter = (list, filter) => {
    return list
        .filter(e => filter.origin.id === "ALL" ? true : filter.origin.id === e.origin.id)
        .filter(e => filter.showHidden ? true : !e.hidden)
}

const CardHeader = ContextMenuTarget(class CardHeaderClass extends React.Component {
    render() {
        let trait = this.props.trait
        let origin = this.props.origin
        return (
            <div className={style.cardHeader}>
                <div className={style.cardName}>{trait.name}</div>
                <div className={style.cardTag}><Tag minimal large>{origin.name}</Tag></div>
            </div>
        )
    }

    renderContextMenu() {
        return (
            <Menu>
                { !(this.props.holder.hidden) &&
                    <MenuItem icon={"eye-off"}
                              onClick={this.handleHide(this.props.char, this.props.trait)}
                              text="Скрыть" />
                }
                { this.props.holder.hidden &&
                    <MenuItem icon={"eye-on"}
                              onClick={this.handleShow(this.props.char, this.props.trait)}
                              text="Показывать" />
                }
            </Menu>
        );
    }

    handleHide = (char, trait) => () => {service.hide(char.id, trait.id).then(() => service.load(char.id))}

    handleShow = (char, trait) => () => {service.show(char.id, trait.id).then(() => service.load(char.id))}
});

const TraitCard = ({char, holder, trait, origin}) => {
    let navigate = useNavigate()
    return <Card interactive elevation={Elevation.TWO}>
        <div className={style.cardRoot}>
            <CardHeader char={char} holder={holder} trait={trait} origin={origin}/>
            <Divider/>
            <div className={style.cardBody}>
                <div dangerouslySetInnerHTML={{__html: textHtml(trait.descr)}}/>
            </div>
            <Divider/>
            <div className={style.footer}>
                <div className={style.footerBtn}>
                    <Button minimal fill
                            onClick={() => navigate(`/overview/trait/${trait.id}`, {replace: false})}
                            intent={Intent.WARNING}>
                        Подробнее
                    </Button>
                </div>
            </div>
        </div>
    </Card>
}

export default TraitOverview