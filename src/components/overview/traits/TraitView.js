import React, {Fragment, useEffect, useState} from 'react'
import {useMatch, useParams} from "react-router";
import LoadingView from "../../LoadingView";
import service from '../../../logic/services/TraitService'
import style from '../../../css/overview/traits/TraitView.module.css'
import {Divider} from "@blueprintjs/core";
import {Container} from 'react-grid-system'
import {textHtml} from "../../../App";

const TraitView = () => {
    let [trait, setTrait] = useState()
    let [filter, setFilter] = useState()
    let { id } = useParams()

    useEffect(() => {service.get(+id).then(res => setTrait(res))}, [id])

    return trait ? <Fragment>
        <div className={style.root}>
            <Container>
                <div className={style.header}>{trait.name}</div>
                <br/>
                <Divider/>
                <br/>
                <div dangerouslySetInnerHTML={{__html: textHtml(trait.descr)}}/>
            </Container>
        </div>
        </Fragment> : <LoadingView/>
}

export default TraitView