import React, {useEffect, useState} from "react";
import {Button, Dialog, Divider, Intent, Label, Switch} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {Select} from "@blueprintjs/select";
import style2 from "../../../../css/createchar/CreateCharView.module.css"
import {traitsFilterDialogService as service} from '../../../../logic/services/dialog/traits/TraitsFilterDialogService'

const origins = [
    {id: "ALL", name: "Все"},
    {id: "CLS", name: "Класс"},
    {id: "SUBCLS", name: "Архетип"},
    {id: "SUBRACE", name: "Подвид"},
    {id: "FEAT", name: "Черта"},
    {id: "RACE", name: "Раса"},
    {id: "BG", name: "Предыстория"}
]

const TraitFilterDialog = () => {
    let state = useSelector(store => store.dialog[DG.TraitsFilter]);
    let filter = useSelector(store => store.account.traitsFilter || {})

    let [loading, setLoading] = useState(false)
    let [origin, setOrigin] = useState(filter && filter.origin);
    let [showHidden, setShowHidden] = useState(filter && filter.showHidden);

    let close = () => dialogueService.setState(DG.TraitsFilter, {open: false, context: undefined})

    useEffect(() => {
        setLoading(false)
        setOrigin(filter && filter.origin)
        setShowHidden(filter && filter.showHidden)
    }, [state])

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Фильтр`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <div className={style.flow}>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Источник</div>
                        <Select items={origins}
                                popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                                activeItem={origin}
                                filterable={false}
                                itemRenderer={(item, props) =>
                                    <div key={item.id} className={style2.giClsSelItem} onClick={props.handleClick}>
                                        {item.name}
                                    </div>
                                } onItemSelect={(item) => setOrigin(item)}>
                            <Button style={{width: "120px"}} loading={loading}>{origin.name}</Button>
                        </Select>
                    </Label>
                    <Switch checked={showHidden || false}
                            label={"Показывать скрытые"}
                            onChange={() => {setShowHidden(!showHidden)}}/>
                </div>
                <Divider style={{marginBottom: "15px"}}/>
                <Button loading={loading} intent={Intent.SUCCESS} fill onClick={() => {
                    setLoading(true)
                    service.set(origin, showHidden)
                        .then(() => close())
                        .catch(() => setLoading(false))
                }}>Готово</Button>
            </div>
        </Dialog>
    )
};

export default TraitFilterDialog