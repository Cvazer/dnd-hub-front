import React, {useEffect, useState} from "react";
import {Button, Dialog, Divider, InputGroup, Intent} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import s from "../../../../css/overview/traits/dialog/AddFeatureDialog.module.css"
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../../logic/services/DialogService";
import {addFeatureDialogService as service} from '../../../../logic/services/dialog/traits/AddFeatureDialogService'
import traitService from "../../../../logic/services/TraitService";

const AddFeatureDialog = () => {
    let state = useSelector(store => store.dialog[DG.TraitsAddFeature]);

    let [list, setList] = useState([])
    let [search, setSearch] = useState("")
    let [feat, setFeat] = useState(undefined)

    let [loading, setLoading] = useState(false)

    let close = () => dialogueService.setState(DG.TraitsAddFeature, {open: false, context: undefined})

    useEffect(() => {
        service.list().then(res => setList(res))
        setSearch("")
    }, [state])

    let filter = query => { return list.filter(e => {
        return e.name.toUpperCase().includes(query.toUpperCase()) ||
            e.source.id.includes(query.toUpperCase())
    })}

    let select = feat => setFeat(feat)

    let add = () => {
        if (!feat) return
        setLoading(true)
        service.add(feat.id)
            .then(charId => traitService.load(charId))
            .then(() => setLoading(false))
            .then(() => close())
    }

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={`Добавление черты`}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <div className={s.listRoot}>
                    <InputGroup type="search" value={search}
                                onChange={(e) => setSearch(e.target.value)}
                                leftIcon={"search"} placeholder={"Название"} />
                    <br/>
                    <div className={s.list}>
                        {filter(search).map(e => <div
                            className={feat ? feat.id === e.id ? s.listItemSelected : s.listItem : s.listItem}
                            onClick={() => select(e)} key={e.id}>
                            {e.name} ({e.source.id})
                        </div>)}
                    </div>
                </div>
                <br/>
                <Divider style={{marginBottom: "15px"}}/>
                <Button loading={loading} intent={Intent.SUCCESS} fill onClick={() => {add()}}>Добавить</Button>
            </div>
        </Dialog>
    )
};

export default AddFeatureDialog