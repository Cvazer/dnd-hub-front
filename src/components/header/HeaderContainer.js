import React, {Fragment} from "react";
import cn from 'classnames'
import style from '../../css/header/HeaderContainer.module.css'
import {Icon} from "@blueprintjs/core";
import {useLocation} from "react-router";

const getText = loc => {
    if (/^.*overview\/items.*$/.test(loc)) return "Предметы"
    if (/^.*overview\/item\/\d+$/.test(loc)) return "Предметы"
    if (/^.*admin\/items.*$/.test(loc)) return "Предметы (Админ)"
    if (/^.*admin\/campaigns.*$/.test(loc)) return "Кампании (Админ)"
    if (/^.*admin\/accounts.*$/.test(loc)) return "Аккаунты"
    if (/^.*admin\/scrapping.*$/.test(loc)) return "Скраппинг"
    if (/^.*overview\/char.*$/.test(loc)) return "Персонаж"
    if (/^.*overview\/traits\/\d+$/.test(loc)) return "Способности"
    if (/^.*overview\/trait\/\d+$/.test(loc)) return "Способности"
    if (/^.*overview\/profs\/\d+$/.test(loc)) return "Владения"
    if (/^.*overview\/magic\/\d+$/.test(loc)) return "Магия"
    if (/^.*overview\/notes\/\d+$/.test(loc)) return "Записи"
    if (/^\/create-char\/$/.test(loc)) return "Новый персонаж"
    if (/^\/overview\/$/.test(loc)) return "Выбор персонажа"
    if (/^\/settings$/.test(loc)) return "Настройки"
    if (/^\/admin\/?$/.test(loc)) return "Панель администратора"
    if (/^\/campaign\/?$/.test(loc)) return "Панель кампании"
    if (/^.*campaign\/\d+$/.test(loc)) return "Панель кампании"
    return "Где-то"
}

const HeaderContainer = ({setIsOpen}) => {
    let location = useLocation()
    return (
        <Fragment>
            <div className={style.filler}/>
            <div className={cn(style.root)}>
                <div className={cn(style.drawerToggleContainer)} onClick={() => setIsOpen(true)}>
                    <Icon icon={"menu"} iconSize={30}/>
                    <div className={cn(style.header)}>{getText(location.pathname)}</div>
                </div>
            </div>
        </Fragment>
    )
};

export default HeaderContainer