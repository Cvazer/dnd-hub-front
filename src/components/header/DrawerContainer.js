import React, {Fragment} from "react";
import {Drawer, Icon} from "@blueprintjs/core";
import {Position} from "@blueprintjs/core/lib/cjs/common/position";
import style from '../../css/Drawer.module.css'
import cn from 'classnames'
import {useSelector} from "react-redux";
import charService from "../../logic/services/CharService";
import {useNavigate} from "react-router";
import {replace} from "lodash";

const DrawerContainer = ({isOpen, setIsOpen}) => {
    let char = useSelector(store => store.overview.char)
    let role = useSelector(store => store.account.role)
    let campaignId = useSelector(store => store.campaign.id)

    return (
        <Drawer size={"300px"}
                isOpen={isOpen}
                canOutsideClickClose={true}
                onClose={() => setIsOpen(false)}
                position={Position.LEFT}>
            <div className={style.root}>
                <div className={style.headerContainer}>
                    <div className={style.headerIcon} onClick={() => setIsOpen(false)}>
                        <Icon icon={"menu-closed"} iconSize={30}/>
                    </div>
                    <div className={cn(style.header)}>D&D Hub</div>
                </div>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"id-number"}
                            text={"Обзор"}
                            attempt={"/overview/char/%charId%"}
                            url={char?"/overview/char/"+char.id:"/overview/"}/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"briefcase"}
                            text={"Предметы"}
                            attempt={"/overview/items/%charId%"}
                            url={char?"/overview/items/"+char.id:"/overview/"}/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"diagram-tree"}
                            text={"Способности"}
                            attempt={"/overview/traits/%charId%"}
                            url={char?"/overview/traits/"+char.id:"/overview/"}/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"search-around"}
                            text={"Владения"}
                            attempt={"/overview/profs/%charId%"}
                            url={char?"/overview/profs/"+char.id:"/overview/"}/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"flame"}
                            text={"Магия"}
                            attempt={"/overview/magic/%charId%"}
                            url={char?"/overview/magic/"+char.id:"/overview/"}/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"book"}
                            text={"Записи"}
                            attempt={"/overview/notes/%charId%"}
                            url={char?"/overview/notes/"+char.id:"/overview/"}/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"clipboard"}
                            text={"Кампания"}
                            attempt={"/campaign/%campaignId%"}
                            url={campaignId?`/campaign/${campaignId}`:"/campaign/"}/>
                <Divider/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"properties"}
                            text={"Выбор персонажа"}
                            url={"/overview/"}
                            clickHandler={() => {charService.clear()}}/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"new-person"}
                            text={"Создать персонажа"}
                            url={"/create-char/"}
                            clickHandler={() => {charService.clear()}}/>
                <Divider/>
                <DrawerItem setIsOpen={setIsOpen}
                            icon={"cog"}
                            text={"Настройки"}
                            url={"/settings"}
                            clickHandler={() => {charService.clear()}}/>
                { role === "admin" && <Fragment>
                    <Divider/>
                    <DrawerItem setIsOpen={setIsOpen}
                                icon={"control"}
                                text={"Админская зона"}
                                url={"/admin/"}
                                clickHandler={() => {charService.clear()}}/>
                </Fragment>}
            </div>
        </Drawer>
    );
};

const DrawerItem = ({attempt, setIsOpen, url, icon, text, clickHandler}) => {
    let navigate = useNavigate()

    return <div className={style.element} onClick={() => {
        clickHandler && clickHandler()
        attempt && localStorage.setItem("attemptedLocation", attempt)
        go(url)(navigate)(setIsOpen)()
    }}>
        <Icon className={cn(style.elementIcon, "bp3-text-muted")} icon={icon} iconSize={25}/>
        <span className={cn(style.elementText, "bp3-text-muted bp3-text-overflow-ellipsis")}>
            {text}
        </span>
    </div>
}

const Divider = () => <div className={style.divider}/>;

const go = url => navigate => setIsOpen => () => {
    setIsOpen(false);
    navigate(url, {replace: false});
};

export default DrawerContainer