import React, {Fragment, useState} from 'react';
import {Col, Container, Row} from 'react-grid-system'
import {Button, ControlGroup, Icon, InputGroup} from "@blueprintjs/core";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import style from "../css/InitialView.module.css"
import accountService from "../logic/services/AccountService";

const InitialView = () => {
    const [value, setValue] = useState("");
    const [loading, setLoading] = useState(false);
    return (
        <Fragment>
            <div style={{height: "calc(100vh)"}}>
                <Container className={style.container}>
                    <Icon icon={"key"} className={"bp3-text-muted"} iconSize={48}/>
                    <br/>
                    <br/>
                    <Row><Col xs={12}><div className="bp3-text-muted bp3-text-large">Нужен ключ от мастера!</div></Col></Row>
                    <br/>
                    <Row><Col xs={12}>
                        <ControlGroup fill={true} vertical={false}>
                            <InputGroup placeholder="Ключ..."
                                        value={value}
                                        onChange={e => setValue(e.target.value)}/>
                            <Button icon="tick" intent={Intent.SUCCESS} onClick={send(value, setLoading)} loading={loading}/>
                        </ControlGroup>
                    </Col></Row>
                </Container>
            </div>
        </Fragment>
    )
};

const send = (value, loadingCallback) => () => {
    loadingCallback(true);
    accountService
        .setAccount(value)
        .then(_ => {}, _ => {loadingCallback(false)})
};

export default InitialView