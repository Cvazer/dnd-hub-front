import React, {Fragment, useEffect, useState} from "react";
import {Container} from 'react-grid-system'
import {Button, ControlGroup, InputGroup, Intent, Label, NumericInput} from "@blueprintjs/core";
import charService from "../../logic/services/CharService";
import accountService from "../../logic/services/AccountService";
import toasterService from "../../logic/services/ToasterService";
import {useSelector} from "react-redux";
import campaignService from "../../logic/services/CampaignService";
import {useNavigate} from "react-router";

const SettingsView = () => {
    let spacer = useSelector(store => store.account.spacer)

    let [key, setKey] = useState(localStorage.getItem("dnd-hub:key"))
    let [loadingKey, setLoadingKey] = useState(false)
    let [loadingSpc, setLoadingSpc] = useState(false)
    let [spc, setSpc] = useState(0)

    let navigate = useNavigate()

    useEffect(() => {
        if ((window.location+"").includes("cache=")) {
            navigate("/settings")
        }
    }, [])

    useEffect(() => setSpc(spacer), [spacer])

    let confirmKeyChange = () => {
        setLoadingKey(true)
        accountService.setAccount(key)
            .then(() => charService.clear())
            .then(() => campaignService.clear())
            .then(() => setLoadingKey(false))
            .then(() => toasterService.ok("Ключ изменен"))
            .catch(() => setLoadingKey(false))
    }

    return (
        <Fragment>
            <Container>
                <Label style={{marginBottom: "5px"}}>
                    <div className={"bp3-text-muted bp3-text-small"}>Ключ</div>
                </Label>
                <ControlGroup fill vertical={false}>
                    <InputGroup fill large value={key}
                                onKeyPress={e => e.key === 'Enter' && confirmKeyChange()}
                                onChange={e => setKey(e.target.value)}/>
                    <Button intent={Intent.SUCCESS} loading={loadingKey} icon={"tick"} onClick={confirmKeyChange}/>
                </ControlGroup>
                <br/>
                <Label style={{marginBottom: "5px"}}>
                    <div className={"bp3-text-muted bp3-text-small"}>Ширина контента (0 = max)</div>
                </Label>
                <ControlGroup fill vertical={false}>
                    <NumericInput fill large value={spc} onValueChange={e => setSpc(e)}/>
                    <Button intent={Intent.SUCCESS} loading={loadingSpc} icon={"tick"} onClick={() => {
                        setLoadingSpc(true)
                        accountService.setSpacer(+spc)
                            .then(() => setLoadingSpc(false))
                            .then(() => toasterService.ok("Ширина изменена"))
                            .catch(() => setLoadingSpc(false))
                    }}/>
                </ControlGroup>
                <div style={{marginTop: "30px"}}>
                    <Button fill intent={Intent.PRIMARY} onClick={() => {
                        window.location = location.href + '?cache=' + new Date().getTime();
                    }}>Сбросить кэш</Button>
                </div>
            </Container>
        </Fragment>
    )
}

export default SettingsView