import React, {useState} from "react";
import style from "../../css/cards/CharCard.module.css";
import {Button, Card, HTMLTable, Icon, Intent, Menu, MenuItem, Popover} from "@blueprintjs/core";
import {Elevation} from "@blueprintjs/core/lib/cjs/common/elevation";
import cn from "classnames";
import {Position} from "@blueprintjs/core/lib/cjs/common/position";

export const CharCard = ({card, clickHandler, delHandler}) => {
    let [loading, setLoading] = useState(false);

    return (
        <div className={style.root} key={card.id}>
            <Card elevation={Elevation.TWO} interactive={true} onClick={clickHandler(card)}>
                <div className={style.header}>
                    <div className={cn(style.headerIconContainer, "bp3-text-muted")}>
                        <Icon icon={"person"} iconSize={32}/>
                        <div className={style.iconUnderline}/>
                    </div>
                    <div className={cn(style.headerTextContainer)}>
                <span className={cn("bp3-text-overflow-ellipsis bp3-text-large", style.headerTextTop)}>
                    {card.name}
                    {/*Too Long of A nameToo Long of A nameToo Long of A nameToo Long of A nameToo Long of A nameToo Long of A name*/}
                </span>
                        <span className={cn("bp3-text-overflow-ellipsis bp3-text-small bp3-text-muted", style.headerTextBot)}>
                    {card.playerName}
                            {/*Too Long of A nameToo Long of A nameToo Long of A nameToo Long of A nameToo Long of A nameToo Long of A name*/}
                </span>
                    </div>
                    <div className={style.headerLvlContainer} onClick={e => e.stopPropagation()}>
                        <Popover content={
                            <Menu>
                                <MenuItem text="Удалить" onClick={delHandler(card)}/>
                            </Menu>
                        } position={Position.LEFT}>
                            <Icon icon={"more"} iconSize={18} className={"bp3-text-muted"}/>
                        </Popover>
                    </div>
                </div>
                <br/>
                <div className={style.body}>
                    <HTMLTable condensed={true} striped={true} className={style.table}>
                        <thead>
                        <tr>
                            <th>Класс</th>
                            <th>Уровень</th>
                        </tr>
                        </thead>
                        <tbody>
                        {card.classes.map(charClass => <tr key={charClass.id}>
                            <td>{charClass.name}</td>
                            <td>{charClass.lvl}</td>
                        </tr>)}
                        </tbody>
                    </HTMLTable>
                </div>
                <div className={style.footer}>
                    <Button text={"Выбрать"}
                            onClick={() => {
                                setLoading(true)
                                clickHandler(card);
                            }}
                            loading={loading}
                            large={true}
                            intent={Intent.SUCCESS}
                            minimal={true}/>
                </div>
            </Card>
        </div>
    )
};