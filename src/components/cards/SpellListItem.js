import {Button, Checkbox, Tag} from "@blueprintjs/core";
import style from "../../css/cards/SpellListItem.module.css";
import React, {useEffect, useState} from "react";
import cn from "classnames";

const SpellListItem = ({className, spell, onClick, onPrepared, tagTextExtractor, skip = []}) => {
    let getTag = text => <Tag key={text} minimal>{text}</Tag>

    let normalizeCastingTime = spell => {
        if (spell.castingTime === 'Bonus action') return "Bonus"
        return spell.castingTime
    }

    let [tags, setTags] = useState([])
    useEffect(() => {
        if (tagTextExtractor) {
            setTags(tagTextExtractor(spell).map(text => getTag(text)))
        } else {
            let list = []
            !skip.includes("level") && spell.level && list.push(getTag(spell.level))
            !skip.includes("castingTime") && spell.castingTime && list.push(getTag(normalizeCastingTime(spell)))
            !skip.includes("verbal") && spell.verbal && list.push(getTag("V"))
            !skip.includes("somatic") && spell.somatic && list.push(getTag("S"))
            !skip.includes("material") && spell.material && list.push(getTag("M"))
            !skip.includes("ritual") && spell.ritual && list.push(getTag("R"))
            setTags(list)
        }
    }, [spell])

    return (
        <div className={cn(className, style.itemRoot)} onClick={onClick||(() => {})}>
            {onPrepared && <div className={style.itemPreparedContainer}>
                <Checkbox checked={spell.prepared}
                          className={style.fixPrepared}
                          onClick={onPrepared}
                          alignIndicator={"center"}/>
            </div>}
            <div className={style.itemNameContainer}>
                {spell.name}
            </div>
            <div className={style.itemTagsContainer}>
                {tags}
            </div>
            <div className={style.itemControlsContainer}>
                <Button icon={"eye-open"} minimal/>
            </div>
        </div>
    )
}

export default SpellListItem