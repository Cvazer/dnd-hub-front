import React, {useState} from "react";
import ace from 'brace'
import {Button, ControlGroup, Dialog, Divider, Intent} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../../css/CharOverview.module.css";
import { JsonEditor as Editor } from 'jsoneditor-react';
import dialogStyle from '../../../../css/admin/scrapping/dialog/ResolveResourceConflictDialog.module.css'
import {resolveResourceConflictDialogService as service} from '../../../../logic/services/admin/scrapping/dialogs/ResolveResourceConflictDialogService'
import 'jsoneditor-react/es/editor.min.css';

const ResolveResourceConflictDialog = ({problem, resolverName, onClose, update}) => {
    let [open, setOpen] = useState(true)
    let [generalNew, setGeneralNew] = useState({...problem.meta.new, data: undefined})
    let [generalPersisted, setGeneralPersisted] = useState({...problem.meta.persisted, data: undefined})
    let [generalMerged, setGeneralMerged] = useState({...problem.meta.new, data: undefined})

    let [dataNew, setDataNew] = useState(JSON.parse(problem.meta.new.data))
    let [dataPersisted, setDataPersisted] = useState(JSON.parse(problem.meta.persisted.data))
    let [dataMerged, setDataMerged] = useState({})

    let commit = action => {
        service.resolve(action, problem, resolverName, dataNew, generalNew,
            dataPersisted, generalPersisted, dataMerged, generalMerged)
            .then(() => {
                setOpen(false)
                onClose()
                update()
            })
    }

    return (
        <Dialog usePortal
                isOpen={open}
                isCloseButtonShown
                title={"Конфликт ресурсов"}
                onClose={e => {setOpen(false); onClose(e)}}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog, dialogStyle.portal)}>
            <div className={style.hpDialogContainer}>
                <div className={dialogStyle.sidedContainer}>
                    <div className={dialogStyle.newRes}><span>Данные нового ресурса:</span></div>
                    <div className={dialogStyle.persistedRes}><span>Данные уже существующего ресурса:</span></div>
                </div>
                <div className={dialogStyle.sidedContainer}>
                    <div className={dialogStyle.newRes}>
                        <Editor
                            mode={"view"} ace={ace}
                            allowedModes={["view", "code", "tree"]}
                            value={generalNew}
                            onChange={e => setGeneralNew(e)}
                        />
                        <br/>
                    </div>
                    <div className={dialogStyle.persistedRes}>
                        <Editor
                            mode={"view"} ace={ace}
                            allowedModes={["view", "code", "tree"]}
                            value={generalPersisted}
                            onChange={e => setGeneralPersisted(e)}
                        />
                    </div>
                </div>
                <br/>
                <div className={dialogStyle.sidedContainer}>
                    <div className={dialogStyle.newRes}>
                        <Editor
                            mode={"view"} ace={ace}
                            allowedModes={["view", "code", "tree"]}
                            value={dataNew}
                            onChange={e => setDataNew(e)}
                        />
                        <br/>
                    </div>
                    <div className={dialogStyle.persistedRes}>
                        <Editor
                            mode={"view"} ace={ace}
                            allowedModes={["view", "code", "tree"]}
                            value={dataPersisted}
                            onChange={e => setDataPersisted(e)}
                        />
                    </div>
                </div>
                <br/>
                <span>Данные консенсуса:</span>
                <div className={dialogStyle.mergedContainer}>
                    <Editor
                        mode={"code"}
                        allowedModes={["view", "code", "tree"]}
                        value={generalMerged}
                        onChange={e => setGeneralMerged(e)}
                    />
                </div>
                <div className={dialogStyle.mergedContainer}>
                    <Editor
                        mode={"code"}
                        allowedModes={["view", "code", "tree"]}
                        value={dataMerged}
                        onChange={e => setDataMerged(e)}
                    />
                </div>
                <br/>
                <Divider/>
                <br/>
                <ControlGroup>
                    <Button fill intent={Intent.DANGER} text={"Выбросить новый"}
                            onClick={() => commit("DISCARD_NEW")}/>
                    <Button fill intent={Intent.WARNING} text={"Использовать новый"}
                            onClick={() => commit("USE_NEW")}/>
                    <Button fill intent={Intent.PRIMARY} text={"Использовать консенсус"}
                            onClick={() => commit("USE_CONSENSUS")}/>
                    <Button fill intent={Intent.SUCCESS} text={"Сохранить оба"}
                            onClick={() => commit("KEEP_BOTH")}/>
                </ControlGroup>
                <br/>
            </div>
        </Dialog>
    )
}

export default ResolveResourceConflictDialog