import React, {Fragment, useEffect, useState} from "react";
import {Container} from "react-grid-system";
import {adminScrappingViewService as service} from "../../../logic/services/admin/AdminScrappingViewService";
import style from "../../../css/admin/scrapping/AdminScrappingView.module.css"
import {MultiSelect, Select} from "@blueprintjs/select";
import {Button, ControlGroup, Icon, Tag, Tree} from "@blueprintjs/core";
import LoadingView from "../../LoadingView";
import cn from "classnames";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import ResolveResourceConflictDialog from "./dialogs/ResolveResourceConflictDialog";

const AdminScrappingView = () => {
    let [scrapper, setScrapper] = useState()

    return (
        <Container>
            <ScrapperSelector scrapper={scrapper} setScrapper={setScrapper}/>
            <br/>
            {scrapper && <ScrapperInfo scrapper={scrapper}/>}
        </Container>
    )
}

const openProblemDialog = (problem, onClose, name, update) => {
    if (problem.type === "RESOURCE_CONFLICT") {
        return <ResolveResourceConflictDialog key={problem.id+"|DIALOG"}
                                              problem={problem}
                                              resolverName={name}
                                              update={update}
                                              onClose={e => {
                                                  e.stopPropagation()
                                                  e.preventDefault()
                                                  onClose()
                                              }}/>
    }
}

const iconsByLevel = {
    INFO: "info-sign",
    WARNING: "warning-sign",
    ERROR: "error"
}
const intentByLevel = {
    INFO: Intent.PRIMARY,
    WARNING: Intent.WARNING,
    ERROR: Intent.DANGER
}

const ScrapperSelector = ({scrapper, setScrapper}) => {
    let [scrappersList, setScrappersList] = useState([])
    useEffect(() => {
        service.listScrappers()
            .then(res => {setScrappersList(res); return res})
            .then(res => res && res.length !== 0 && setScrapper(res[0]))
    }, [])

    return (
        <Select items={scrappersList} fill
                onItemSelect={s => setScrapper(s)}
                itemRenderer={(s, props) => {
                    return (
                        <div key={s+""}
                             onClick={props.handleClick}>
                            {s}
                        </div>
                    )
                }}>
            <Button fill text={scrapper || "None"}/>
        </Select>
    )
}

const ScrapperInfo = ({scrapper}) => {
    let [status, setStatus] = useState()

    let updateStatus = () => {
        service.status(scrapper)
            .then(res => setStatus(res))
    }

    let startScrapping = () => {
        service.startScrapping(scrapper)
            .then(() => updateStatus())
    }

    let reset = () => {
        service.reset(scrapper)
            .then(() => updateStatus())
    }


    useEffect(() => { updateStatus() }, [])

    return !status ? <LoadingView/> : (
        <Fragment>
            <div className={style.infoRoot}>
                <div className={cn("bp3-elevation-0", style.infoTree)}>
                    <ScrapperInfoStatusTree scrapperStatus={status} name={scrapper}/>
                </div>
                <ScrapperInfoProblemList scrapperStatus={status} name={scrapper} updateStatus={updateStatus}/>
            </div>
            <br/>
            <ControlGroup fill>
                <Button fill minimal
                        intent={Intent.PRIMARY}
                        text={"Обновить статус"}
                        onClick={() => updateStatus()}/>
                <Button fill minimal
                        intent={Intent.DANGER}
                        text={"Начать скраппинг"}
                        onClick={() => startScrapping()}/>
                <Button fill minimal
                        intent={Intent.WARNING}
                        text={"Сбросить"}
                        onClick={() => reset()}/>
            </ControlGroup>
        </Fragment>
    )
}

const ScrapperInfoStatusTree = ({scrapperStatus, name}) => {
    let getNodes = scrStatus => {
        return {
            id: 0,
            icon: "flow-branch",
            hasCaret: false,
            label: `${name}: ${scrStatus.status}`,
            isExpanded: true,
            childNodes: Object.keys(scrStatus.providersStatus).map(key => {
                return {
                    id: key,
                    icon: "flow-end",
                    label: `${key}: ${scrStatus.providersStatus[key]}`,
                }
            })
        }
    }

    return (
        <Tree contents={[getNodes(scrapperStatus)]}/>
    )
}

const ScrapperInfoProblemList = ({scrapperStatus, name, updateStatus}) => {
    let levels = ["WARNING", "ERROR", "INFO"]
    let [selectedLevels, setSelectedLevels] = useState(["ERROR"])
    let [query, setQuery] = useState("")

    return (
        <div className={style.infoProblems}>
            <div className={style.infoProblemsControls}>
                <ControlGroup fill>
                    <MultiSelect fill items={(levels || []).filter(l => !selectedLevels.includes(l))}
                                 selectedItems={selectedLevels}
                                 placeholder={"Уровни..."}
                                 tagInputProps={{
                                     onRemove: t => {setSelectedLevels(selectedLevels.filter(l => l !== t))},
                                     tagProps: t => {return {minimal: true, intent: intentByLevel[t]}}
                                 }}
                                 onItemSelect={l => setSelectedLevels(selectedLevels.concat(l))}
                                 tagRenderer={l => l}
                                 onQueryChange={q => setQuery(q)}
                                 resetOnSelect={true}
                                 itemRenderer={(l, p) => {
                                     return <div key={l} style={{marginTop: "5px"}}>
                                         <Tag fill minimal intent={intentByLevel[l]} onClick={p.handleClick}>{l}</Tag>
                                     </div>
                                 }}/>
                </ControlGroup>
            </div>
            <div className={style.infoProblemsListContainer}>
                <div className={style.infoProblemsList}>
                    {scrapperStatus.problems
                        .filter(p => p.descr.includes(query))
                        .filter(p => selectedLevels.length === 0 ? true : selectedLevels.includes(p.level))
                        .map(p => <ProblemElement key={p.id} problem={p} name={name} update={updateStatus}/>)
                    }
                </div>
            </div>
        </div>
    )
}

const ProblemElement = ({problem, name, update}) => {
    let [dialogs, setDialogs] = useState([])

    return (
        <div className={style.problem}
             onClick={() => {
                 let dialog = openProblemDialog(problem, () => setDialogs([]), name, update)
                 setDialogs([dialog])
             }}>
            {dialogs}
            <div className={style.problemIcon}>
                <Icon size={20}
                        intent={intentByLevel[problem.level]}
                        icon={iconsByLevel[problem.level]}/>
            </div>
            <span>{problem.descr}</span>
        </div>
    )
}

export default AdminScrappingView