import React, {useEffect, useState} from "react";
import {Container, Row, Col} from "react-grid-system";
import style from '../../css/admin/AdminAccountsView.module.css';
import '../../css/admin/AdminAccountsViewFixes.css';
import CommonList from "../common/list/CommonList";
import LoadingView from "../LoadingView";
import adminAccountsViewService from "../../logic/services/admin/AdminAccountsViewService";
import {Button, Dialog, Divider, HTMLSelect, Icon, InputGroup, Intent, Label, Tab, Tabs} from "@blueprintjs/core";
import cn from 'classnames'
import charService from "../../logic/services/CharService";
import { FocusStyleManager } from "@blueprintjs/core";
import AdminCardCharsTabList from "./common/AdminCardCharsTabList"; FocusStyleManager.onlyShowFocusOnTabs();

const AdminAccountsView = () => {
    let [list, setList] = useState()
    let [current, setCurrent] = useState()
    let [editOpen, setEditOpen] = useState(true)

    let getAccounts = () => {adminAccountsViewService.getAll().then(res => setList(res))}

    useEffect(() => {getAccounts()}, [])

    return (
        <Container>
            <Row>
                <Col>
                    {!list
                        ? <LoadingView/>
                        : <div>
                            <CommonList items={list}
                                        onItemClick={onAccountListItemClick(setCurrent, setEditOpen)}
                                        keyExtractor={(a) => a.key}
                                        renderer={accountRenderer(
                                            onAccountListItemClick(setCurrent, setEditOpen),
                                            getAccounts
                                        )}
                            />
                            <br/>
                            <Button minimal fill intent={Intent.SUCCESS}
                                    onClick={e => onAccountListItemClick(setCurrent, setEditOpen)({created: true})}>
                                Добавить
                            </Button>
                            <EditDialog open={editOpen} account={current}
                                        setOpen={setEditOpen} getAccounts={getAccounts}/>
                        </div>}
                </Col>
            </Row>
        </Container>
    )
}

const accountRenderer = (editAction, getAccounts) => account => {
    return <div className={style.listItemRoot}>
        <div className={style.listItemLeftPanel}>
            <span>Ключ: <b>{account.key}</b></span>
            <span>Имя: <b>{account.name}</b></span>
            <span>Роль: <b>{account.role}</b></span>
        </div>
        <div className={style.listItemRightPanel}>
            <Button minimal onClick={(e) => {
                e.stopPropagation()
                editAction(account)
            }}>
                <Icon icon={"edit"}/>
            </Button>
            <Button minimal intent={Intent.DANGER} onClick={(e) => {
                e.stopPropagation()
                adminAccountsViewService.deleteAccount(account.key).then(() => getAccounts())
            }}>
                <Icon icon={"trash"}/>
            </Button>
        </div>
    </div>
}

const onAccountListItemClick = (setCurrent, setEditOpen) => account => {
    setCurrent({name: "", role: "user", chars: [], ...account})
    setEditOpen(true)
}

const EditDialog = ({account, open, setOpen, getAccounts}) => {
    let [loading, setLoading] = useState(false)

    return account ? (
        <Dialog isOpen={open}
                isCloseButtonShown
                title={"Карточка аккаунта"}
                onClose={() => setOpen(false)}
                // className={cn(Classes.OVERLAY_SCROLL_CONTAINER)}
                style={{backgroundColor: "white", width: "60vw", minWidth: "400px"}}
                usePortal>
            <div style={{height: "70vh", width: "100%"}} className={"admin-accounts-edit-dialog-root"}>
                <Container className={style.editDialogContainer}>
                    <Tabs id="AccountEditTabs" defaultSelectedTabId={"info"}
                          className={cn(style.editDialogTabs, "admin-accounts-edit-dialog-tabs")}
                          renderActiveTabPanelOnly vertical>
                        <Tab id={"info"} title={"Информация"}
                             panel={<EditDialogInfoTab account={account}/>}/>
                        <Tab id={"chars"} title={"Персонажи"}
                             panel={<EditDialogCharsTab account={account}/>}/>
                    </Tabs>
                </Container>
            </div>
            <Container style={{width: "100%", paddingTop: "10px"}}>
                <Divider/>
                <br/>
                <Button fill intent={Intent.SUCCESS}
                        loading={loading}
                        onClick={() => {
                            setLoading(true)
                            adminAccountsViewService.save({
                                key: account.key,
                                name: account.name,
                                role: account.role,
                                chars: account.chars
                            }).then(() => {setLoading(false); setOpen(false); getAccounts()})
                                .catch(() => setLoading(false))
                        }}>
                    Сохранить
                </Button>
            </Container>
        </Dialog>
    ) : (
        <div/>
    )
}

const EditDialogCharsTab = ({account}) => {
    return (
        <Container>
            <Row>
                <Col>
                    <AdminCardCharsTabList
                        target={account}
                        charsExtractor={() => adminAccountsViewService.getChars()}
                        charListExtractor={() => !account.created
                            ? charService.getAllCardsForAccount(account.key)
                                .then(res => res.map(e => e.id))
                            : Promise.resolve([])}
                        charListSetter={(_, res) => {account.chars = res}}
                    />
                </Col>
            </Row>
        </Container>
    )
}

const EditDialogInfoTab = ({account}) => {
    let [key, setKey] = useState(account ? account.key || "" : "")
    let [name, setName] = useState(account ? account.name || "" : "")
    let [role, setRole] = useState(account ? account.role || "user" : "user")

    useEffect(() => {
        setKey(account ? account.key || "" : "");
        setName(account ? account.name || "" : "");
        setRole(account ? account.role || "user" : "user");
    }, [account])
    return (
        <Container style={{width: "100%"}}>
            <Row>
                <Col>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Ключ</div>
                        <InputGroup value={key || ""} onChange={e => {
                            setKey(e.target.value); account.key = e.target.value
                        } } disabled={!!account.key && !account.created}/>
                    </Label>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Имя</div>
                        <InputGroup value={name || ""} onChange={e => {
                            setName(e.target.value)
                            account.name = e.target.value
                        }}/>
                    </Label>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Роль</div>
                        <HTMLSelect value={role || "user"}
                                    options={["admin", "user"]}
                                    onChange={e => {
                                        setRole(e.target.value)
                                        account.role = e.target.value
                                    }}/>
                    </Label>
                </Col>
            </Row>
        </Container>
    )
}

export default AdminAccountsView