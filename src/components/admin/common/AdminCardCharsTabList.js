import React, {useEffect, useState} from "react";
import CommonList from "../../common/list/CommonList";
import style from "../../../css/admin/common/AdminCardCharsTabList.module.css";
import {Button, Checkbox, Icon, Intent} from "@blueprintjs/core";
import adminAccountsViewService from "../../../logic/services/admin/AdminAccountsViewService";

const AdminCardCharsTabList = ({charsExtractor, charListExtractor, charListSetter, target}) => {
    let [chars, setChars] = useState([])
    let [charList, setCharList] = useState([])

    useEffect(() => {
        target && charsExtractor(target).then(res => setChars(res))
        target && charListExtractor(target).then(res => setCharList(res))
    }, [target])

    useEffect(() => {charListSetter(target, charList)}, [charList])

    return (
        <CommonList items={chars}
                    onItemClick={c => charList.includes(c.id)
                        ? setCharList(charList.filter(e => e !== c.id))
                        : setCharList(charList.concat([c.id]))}
                    keyExtractor={c => c.id}
                    renderer={c => <CharComponent
                        char={c}
                        selected={charList.includes(c.id)}/>}
        />
    )
}

const CharComponent = ({selected, char}) => {
    return (
        <div className={style.charListItemRoot}>
            <div className={style.charListItemLeftPanel}>
                <span>ID: <b>{char.id}</b></span>
                <span>Имя: <b>{char.name}</b></span>
                <span>Игрок: <b>{char.playerName}</b></span>
            </div>
            <div className={style.charListItemRightPanel}>
                <Checkbox style={{transform: "translate(5px, 5px)"}}
                          checked={selected}
                />
            </div>
        </div>
    )
}

export default AdminCardCharsTabList