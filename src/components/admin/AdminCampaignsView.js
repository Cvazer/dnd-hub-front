import React, {useEffect, useState} from "react";
import style from "../../css/admin/AdminCampaignsView.module.css";
import {Col, Container, Row} from 'react-grid-system'
import adminCampaignsViewService from "../../logic/services/admin/AdminCampaignsViewService";
import LoadingView from "../LoadingView";
import CommonList from "../common/list/CommonList";
import {Button, Dialog, Divider, HTMLSelect, Icon, InputGroup, Intent, Label, Tab, Tabs} from "@blueprintjs/core";
import {date} from "../../model/ICUDateTime";
import cn from "classnames";
import ICUDatePicker from "../common/ICUDatePicker";
import AdminCardCharsTabList from "./common/AdminCardCharsTabList";
import {Select} from "@blueprintjs/select";
import adminAccountsViewService from "../../logic/services/admin/AdminAccountsViewService";
import charService from "../../logic/services/CharService";

const AdminCampaignsView = () => {
    let [campaigns, setCampaigns] = useState([])
    let [current, setCurrent] = useState()
    let [loading, setLoading] = useState(true)
    let [editOpen, setEditOpen] = useState(false)

    let getCampaigns = () => adminCampaignsViewService.getAll().then(res => setCampaigns(res))

    useEffect(() => {getCampaigns().then(() => setLoading(false))}, [])

    return loading ? <LoadingView/> : (
        <Container className={style.root}>
            <Row>
                <Col>
                    <CommonList items={campaigns}
                                onItemClick={onCampaignListItemClick(setCurrent, setEditOpen)}
                                keyExtractor={(c) => c.id}
                                renderer={campaignRenderer(
                                    onCampaignListItemClick(setCurrent, setEditOpen),
                                    getCampaigns
                                )}
                    />
                    <br/>
                    <Button minimal fill intent={Intent.SUCCESS}
                            onClick={e => onCampaignListItemClick(setCurrent, setEditOpen)({created: true})}>
                        Добавить
                    </Button>
                    <EditDialog open={editOpen} campaign={current}
                                setOpen={setEditOpen} getCampaigns={getCampaigns}/>
                </Col>
            </Row>
        </Container>
    )
}

const onCampaignListItemClick = (setCurrent, setEditOpen) => campaign => {
    setCurrent({
        name: "",
        currentDateTime: {...date({})},
        chars: [],
        accounts: [],
        ...campaign
    })
    setEditOpen(true)
}

const campaignRenderer = (editAction, getCampaigns) => campaign => {
    return (
        <div className={style.listItemRoot}>
            <div className={style.listItemLeftPanel}>
                <span>Название: <b>{campaign.name}</b></span>
                <span>Дата: <b>{date(campaign.currentDateTime).fancyString}</b></span>
            </div>
            <div className={style.listItemRightPanel}>
                <Button minimal onClick={(e) => {
                    e.stopPropagation()
                    editAction(campaign)
                }}>
                    <Icon icon={"edit"}/>
                </Button>
                <Button minimal intent={Intent.DANGER} onClick={(e) => {
                    e.stopPropagation()
                    console.log(JSON.stringify(campaign))
                }}>
                    <Icon icon={"trash"}/>
                </Button>
            </div>
        </div>
    )
}

const EditDialog = ({campaign, open, setOpen, getCampaigns}) => {
    let [loading, setLoading] = useState(false)

    return campaign ? (
        <Dialog isOpen={open}
                isCloseButtonShown
                title={"Карточка кампании"}
                onClose={() => setOpen(false)}
            // className={cn(Classes.OVERLAY_SCROLL_CONTAINER)}
                style={{backgroundColor: "white", width: "70vw", minWidth: "400px"}}
                usePortal>
            <div style={{height: "70vh", width: "100%"}} className={"admin-accounts-edit-dialog-root"}>
                <Container className={style.editDialogContainer}>
                    <Tabs id="AccountEditTabs" defaultSelectedTabId={"info"}
                          className={cn(style.editDialogTabs, "admin-accounts-edit-dialog-tabs")}
                          renderActiveTabPanelOnly vertical>
                        <Tab id={"info"} title={"Информация"}
                             panel={<EditDialogInfoTab campaign={campaign}/>}/>
                        <Tab id={"chars"} title={"Персонажи"}
                             panel={<EditDialogCharsTab campaign={campaign}/>}/>
                        <Tab id={"accounts"} title={"Участники"}
                             panel={<EditDialogAccountsTab campaign={campaign}/>}/>
                    </Tabs>
                </Container>
            </div>
            <Container style={{width: "100%", paddingTop: "10px"}}>
                <Divider/>
                <br/>
                <Button fill intent={Intent.SUCCESS}
                        loading={loading}
                        onClick={() => {
                            setLoading(true)
                            adminCampaignsViewService.save({
                                id: campaign.id,
                                name: campaign.name,
                                accounts: campaign.accounts,
                                chars: campaign.chars,
                                currentDateTime: campaign.currentDateTime
                            })
                                .catch(() => setLoading(false))
                                .then(() => {setLoading(false); setOpen(false); getCampaigns()})
                        }}>
                    Сохранить
                </Button>
            </Container>
        </Dialog>
    ) : (
        <div/>
    )
}

const EditDialogInfoTab = ({campaign}) => {
    let [name, setName] = useState(campaign ? campaign.name || "" : "")
    let [dt, setDt] = useState(campaign ? campaign.currentDateTime || {} : {})

    useEffect(() => {
        setName(campaign ? campaign.name || "" : "");
        setDt(campaign ? campaign.currentDateTime || {} : {});
    }, [campaign])

    return (
        <Container style={{width: "100%"}}>
            {!campaign.created && <Row>
                <Col>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Идентификатор</div>
                        <InputGroup value={campaign.id} disabled={true}/>
                    </Label>
                </Col>
            </Row>}
            <Row>
                <Col>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Название</div>
                        <InputGroup value={name || ""} onChange={e => {
                            setName(e.target.value)
                            campaign.name = e.target.value
                        }}/>
                    </Label>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Дата</div>
                        <ICUDatePicker maxWidth={"none"} viewMode={false}
                                       onValidValue={newDt => {
                                           let tmp = {...newDt}
                                           setDt(tmp)
                                           campaign.currentDateTime = tmp
                                       }}
                                       value={date(dt)}/>
                    </Label>
                </Col>
            </Row>
        </Container>
    )
}

const EditDialogCharsTab = ({campaign}) => {
    return (
        <Container>
            <Row>
                <Col>
                    <AdminCardCharsTabList
                        target={campaign}
                        charsExtractor={() => adminCampaignsViewService.getChars()}
                        charListExtractor={() => Promise.resolve(campaign.chars)}
                        charListSetter={(_, chars) => campaign.chars = chars}
                    />
                </Col>
            </Row>
        </Container>
    )
}

const EditDialogAccountsTabActiveCharInfo = ({account, campaign, setAccountsList, accountsList}) => {
    let [role, setRole] = useState(account.role+"")
    let [char, setChar] = useState(undefined)
    let [chars, setChars] = useState([])
    let [loading, setLoading] = useState(false)

    useEffect(() => {
        setRole(account ? account.role || "player" : "player")

        setLoading(true)

        charService.getAllCardsForAccount(account.key)
            .then(res => res.filter(c => campaign.chars.includes(c.id)))
            .then(res => setChars(res))

        if (!account || !account.activeCharId) { setLoading(false); return }

        account && account.activeCharId && charService
            .getCard(+account.activeCharId)
            .then(res => setChar({id: res.id, name: res.name}))
            .then(() => setLoading(false))
            .catch(() => setLoading(false))
    }, [account])

    useEffect(() => {account.role = role}, [role])
    useEffect(() => {account.activeCharId = char && char.id}, [char])

    return (
        <div className={style.listItemRoot}>
            <div className={style.listItemLeftPanelAccount}>
                <span className={style.managedSpan}>Ключ: <b>{account.key}</b></span>
                <span className={style.managedSpan}>
                        <HTMLSelect options={["master", "player"]}
                                    onChange={e => setRole(e.currentTarget.value)}
                                    value={role}/>
                    </span>
                <span className={style.managedSpan}>
                        <Select
                            onItemSelect={c => setChar({id: c.id, name: c.name})}
                            items={chars}
                            popoverProps={{fill: true, popoverClassName: style.editDialogAccountsTabAddAccountPopup}}
                            filterable={false}
                            itemRenderer={(item, props) =>
                                <div key={item.id}
                                     className={style.editDialogAccountsTabAddAccountListItem}
                                     onClick={props.handleClick}>
                                    {item.id}: {item.name}
                                </div>
                            }>
                            <Button loading={loading}>{char ? char.name : "(нет)"}</Button>
                        </Select>
                    </span>
            </div>
            <div className={style.listItemRightPanel}>
                <Button minimal intent={Intent.DANGER} onClick={(e) => {
                    e.stopPropagation()
                    setAccountsList(accountsList.filter(a => a.key !== account.key))
                }}>
                    <Icon icon={"trash"}/>
                </Button>
            </div>
        </div>
    )
}

const EditDialogAccountsTab = ({campaign}) => {
    let [accounts, setAccounts] = useState([])
    let [accountsList, setAccountsList] = useState(campaign ? campaign.accounts || [] : [])

    useEffect(() => {
        adminAccountsViewService.getAll()
            .then(res => setAccounts(res))
        campaign && setAccountsList(campaign.accounts)
    }, [campaign])

    useEffect(() => {campaign.accounts = accountsList}, [accountsList])

    let filteredAccounts = () => {
        let keys = accountsList.map(a => a.key);
        return accounts.filter(a => !keys.includes(a.key))
    }

    return (
        <Container>
            <Row>
                <Col>
                    <br/>
                    <CommonList keyExtractor={a => a.key}
                                renderer={account =>
                                    <EditDialogAccountsTabActiveCharInfo
                                        account={account}
                                        campaign={campaign}
                                        setAccountsList={setAccountsList}
                                        accountsList={accountsList}
                                        />
                                }
                                items={accountsList}/>
                    <br/>
                    <Select
                        onItemSelect={a => {
                            setAccountsList(accountsList.concat([{
                                key: a.key,
                                role: "player",
                                activeCharId: undefined
                            }]))
                        }}
                        items={filteredAccounts()}
                        popoverProps={{fill: true, popoverClassName: style.editDialogAccountsTabAddAccountPopup}}
                        filterable={false}
                        itemRenderer={(item, props) =>
                            <div key={item.key}
                                 className={style.editDialogAccountsTabAddAccountListItem}
                                 onClick={props.handleClick}>
                                {item.key} -- ({item.name})
                            </div>
                        }>
                        <Button fill minimal intent={Intent.SUCCESS}
                                onClick={() => {
                                    // setLoading(true)
                                    // console.log(campaign)
                                    // setLoading(false)
                                }}>
                            Добавить
                        </Button>
                    </Select>
                </Col>
            </Row>
        </Container>
    )
}

export default AdminCampaignsView