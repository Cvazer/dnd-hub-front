import React, {useEffect, useState} from "react";
import * as PropTypes from "prop-types";
import style from "../../css/admin/AdminItemEdit.module.css"
import {Button, ButtonGroup, Checkbox, Collapse, HTMLSelect, Icon, InputGroup, Label, Pre} from "@blueprintjs/core";
import {dict, dictValues} from "../../App";
import SimpleMDE from "react-simplemde-editor";
import "easymde/dist/easymde.min.css";
import {adminItemsEditService as service} from "../../logic/services/admin/AdminItemsEditService";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";

const AdminItemEdit = ({item, setItem, setSearchVal, searchVal}) => {
    let [data, setData] = useState({...item})
    let [loading, setLoading] = useState(false)

    useEffect(() => {setData({...item})}, [item])
    return (
        <div className={style.root}>
            <GeneralInfo data={data} setData={(data) => setData({...data})}
                         setItem={setItem} setSearchVal={setSearchVal} searchVal={searchVal}/>
            <WeaponInfo data={data} setData={(data) => setData({...data})}/>
            <ArmorInfo data={data} setData={(data) => setData({...data})}/>
            <MagicInfo data={data} setData={(data) => setData({...data})}/>
            <br/>
            <ButtonGroup fill>
                <Button intent={Intent.SUCCESS}
                        loading={loading}
                        onClick={() => {service.save(data, setLoading).then(res => {setData(res); setSearchVal(data.name)})}}>
                    Сохранить
                </Button>
            </ButtonGroup>
        </div>
    )
}

const GeneralInfo = ({data, setData, setItem, setSearchVal, searchVal}) => {
    let [baseId, setBaseId] = useState((data.baseItem || {}).id || "")
    useEffect(() => {setBaseId((data.baseItem || {}).id || "")}, [data])

    return <div className={style.generalRoot}>
        <div className={style.generalTop}>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Id</div>
                <div className={style.id}>{data.id}</div>
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Название</div>
                <InputGroup value={data.name}
                            placeholder={"Название"}
                            onChange={e => service.setData(data, setData, "name", e.target.value)}
                />
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Тип</div>
                <HTMLSelect value={data.type.id || ""}
                            options={dictValues("ItemTypeDict").map(it => {
                                return {label: it.name.capitalize(), value: it.id}
                            })}
                            onChange={e => service.setData(data, setData, "type", dict("ItemTypeDict")[e.currentTarget.value])}
                />
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Цена</div>
                <InputGroup value={data.value || ""}
                            placeholder={"Цена"}
                            onChange={e => service.setData(data, setData, "value", +e.target.value)}

                />
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Вес</div>
                <InputGroup value={data.weight || ""}
                            placeholder={"Вес"}
                            onChange={e => service.setData(data, setData, "weight", +e.target.value)}

                />
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Base ID</div>
                <InputGroup value={baseId}
                            placeholder={"Base ID"}
                            rightElement={<Button minimal icon={"small-plus"} onClick={ () =>
                                service.setBase(baseId, data, setData)
                            }/>}
                            onChange={e => setBaseId(e.target.value)}
                />
            </Label>
            {data.id && <Button icon={"trash"} intent={Intent.DANGER} onClick={() => {
                service.delete(data.id, setItem).then(() => setSearchVal(""))
            }}/>}
        </div>
        <div className={style.generalBot}>
            <Checkbox checked={data.base} label={"Base"}
                      onChange={() => service.setBool(data, setData, "base")}/>
            <Checkbox checked={data.generic} label={"Generic"}
                      onChange={() => service.setBool(data, setData, "generic")}/>
            <Checkbox checked={data.concrete} label={"Concrete"}
                      onChange={() => service.setBool(data, setData, "concrete")}/>
            <Checkbox checked={data.catalogue} label={"Catalogue"}
                      onChange={() => service.setBool(data, setData, "catalogue")}/>
        </div>
        <SimpleMDE value={data.descr || ""} onChange={v => service.setData(data, setData, "descr", v)}/>
    </div>
}

const WeaponInfo = ({data, setData}) => {
    let [open, setOpen] = useState(true)

    return <div className={style.weaponRoot}>
        <div className={style.collapserBtn}>
            <Button minimal icon={open?"chevron-down":"chevron-right"} onClick={() => setOpen(!open)}>Оружие</Button>
        </div>
        <div className={style.collapser}>
            <Collapse isOpen={open} keepChildrenMounted={true}>
                <div className={style.weapon}>
                    <Checkbox checked={data.weaponInfo.weapon} label={"Оружие"}
                              onChange={() => service.setInfoData(data, setData, "weapon",
                                  !data.weaponInfo.weapon, "weaponInfo")}
                    />
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Урон</div>
                        <InputGroup value={data.weaponInfo.damage || ""}
                                    placeholder={"Урон"}
                                    onChange={e => service.setInfoData(data, setData, "damage",
                                        e.target.value, "weaponInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Тип урона</div>
                        <HTMLSelect value={(data.weaponInfo.damageType || {}).id || ""}
                                    options={service.getDictSet("DamageTypeDict")}
                                    onChange={e => service.setInfoDictVal(data, setData, "damageType",
                                            e.currentTarget.value, "weaponInfo", "DamageTypeDict")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Категория</div>
                        <HTMLSelect value={(data.weaponInfo.category || {}).id || ""}
                                    options={service.getDictSet("WeaponCategoryDict")}
                                    onChange={e => service.setInfoDictVal(data, setData, "category",
                                            e.currentTarget.value, "weaponInfo", "WeaponCategoryDict")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Дистанция</div>
                        <HTMLSelect value={(data.weaponInfo.distance || {}).id || ""}
                                    options={service.getDictSet("WeaponDistanceDict")}
                                    onChange={e => service.setInfoDictVal(data, setData, "distance",
                                            e.currentTarget.value, "weaponInfo", "WeaponDistanceDict")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Перезарядка</div>
                        <InputGroup value={data.weaponInfo.reload || ""}
                                    placeholder={"Перезарядка"}
                                    onChange={e => service.setInfoData(data, setData, "reload",
                                        e.target.value, "weaponInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Эф. дистанция</div>
                        <InputGroup value={data.weaponInfo.rangeEffective || ""}
                                    placeholder={"Эф. дистанция"}
                                    onChange={e => service.setInfoData(data, setData, "rangeEffective",
                                        e.target.value, "weaponInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Макс. дистанция</div>
                        <InputGroup value={data.weaponInfo.rangeMax || ""}
                                    placeholder={"Макс. дистанция"}
                                    onChange={e => service.setInfoData(data, setData, "rangeMax",
                                        e.target.value, "weaponInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Ammo ID</div>
                        <InputGroup value={data.weaponInfo.ammoId || ""}
                                    placeholder={"Ammo ID"}
                                    onChange={e => service.setInfoData(data, setData, "ammoId",
                                        e.target.value, "weaponInfo")}
                        />
                    </Label>
                </div>
            </Collapse>
        </div>
    </div>
}

const MagicInfo = ({data, setData}) => {
    let [open, setOpen] = useState(true)

    return <div className={style.magicRoot}>
        <div className={style.collapserBtn}>
            <Button minimal icon={open?"chevron-down":"chevron-right"} onClick={() => setOpen(!open)}>Магия</Button>
        </div>
        <div className={style.collapser}>
            <Collapse isOpen={open} keepChildrenMounted={true}>
                <div className={style.magic}>
                    <Checkbox checked={data.magicInfo.magical} label={"Магический"}
                              onChange={() => service.setInfoData(data, setData, "magical",
                                  !data.magicInfo.magical, "magicInfo")}
                    />
                    <Checkbox checked={data.magicInfo.attunement} label={"Настройка"}
                              onChange={() => service.setInfoData(data, setData, "attunement",
                                  !data.magicInfo.attunement, "magicInfo")}
                    />
                    <Checkbox checked={data.magicInfo.wondrous} label={"Диковинка"}
                              onChange={() => service.setInfoData(data, setData, "wondrous",
                                  !data.magicInfo.wondrous, "magicInfo")}
                    />
                    <Checkbox checked={data.magicInfo.curse} label={"Проклятый"}
                              onChange={() => service.setInfoData(data, setData, "curse",
                                  !data.magicInfo.curse, "magicInfo")}
                    />
                    <Checkbox checked={data.magicInfo.sentient} label={"Есть сознание"}
                              onChange={() => service.setInfoData(data, setData, "sentient",
                                  !data.magicInfo.sentient, "magicInfo")}
                    />
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Тип фокусировки</div>
                        <HTMLSelect value={(data.magicInfo.scf || {}).id || ""}
                                    options={service.getDictSet("ScfTypeDict")}
                                    onChange={e => service.setInfoDictVal(data, setData, "scf",
                                        e.currentTarget.value, "magicInfo", "ScfTypeDict")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Редкость</div>
                        <HTMLSelect value={(data.magicInfo.rarity || {}).id || ""}
                                    options={service.getDictSet("RarityDict")}
                                    onChange={e => service.setInfoDictVal(data, setData, "rarity",
                                        e.currentTarget.value, "magicInfo", "RarityDict")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Сопротивление</div>
                        <HTMLSelect value={(data.magicInfo.resistType || {}).id || ""}
                                    options={service.getDictSet("DamageTypeDict")}
                                    onChange={e => service.setInfoDictVal(data, setData, "resistType",
                                        e.currentTarget.value, "magicInfo", "DamageTypeDict")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Перезарядка</div>
                        <HTMLSelect value={(data.magicInfo.recharge || {}).id || ""}
                                    options={service.getDictSet("MagicRechargeDict")}
                                    onChange={e => service.setInfoDictVal(data, setData, "recharge",
                                        e.currentTarget.value, "magicInfo", "MagicRechargeDict")}
                        />
                    </Label>
                </div>
                <br/>
                <div className={style.magic}>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Базовый бонус</div>
                        <InputGroup value={data.magicInfo.bonusBase || ""}
                                    placeholder={"Базовый бонус"}
                                    onChange={e => service.setInfoData(data, setData, "bonusBase",
                                        e.target.value, "magicInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Бонус урона</div>
                        <InputGroup value={data.magicInfo.bonusDamage || ""}
                                    placeholder={"Бонус урона"}
                                    onChange={e => service.setInfoData(data, setData, "bonusDamage",
                                        e.target.value, "magicInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Бонус AC</div>
                        <InputGroup value={data.magicInfo.bonusAc || ""}
                                    placeholder={"Бонус AC"}
                                    onChange={e => service.setInfoData(data, setData, "bonusAc",
                                        e.target.value, "magicInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Бонус атаки</div>
                        <InputGroup value={data.magicInfo.bonusAttack || ""}
                                    placeholder={"Бонус атаки"}
                                    onChange={e => service.setInfoData(data, setData, "bonusAttack",
                                        e.target.value, "magicInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Бонус спас. броска</div>
                        <InputGroup value={data.magicInfo.bonusSave || ""}
                                    placeholder={"Бонус спас. броска"}
                                    onChange={e => service.setInfoData(data, setData, "bonusSave",
                                        e.target.value, "magicInfo")}
                        />
                    </Label>
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Бонус маг. атаки</div>
                        <InputGroup value={data.magicInfo.bonusSpell || ""}
                                    placeholder={"Бонус маг. атаки"}
                                    onChange={e => service.setInfoData(data, setData, "bonusSpell",
                                        e.target.value, "magicInfo")}
                        />
                    </Label>
                </div>
                <br/>
                <div className={style.magic}>

                </div>
                <br/>
                <div className={style.magic}>
                    <InputGroup value={data.magicInfo.attuneCondition || ""} fill
                                placeholder={"Условия настройки"}
                                onChange={e => service.setInfoData(data, setData, "attuneCondition",
                                    e.target.value, "magicInfo")}
                    />
                    <br/>
                </div>
            </Collapse>
        </div>
    </div>
}

const ArmorInfo = ({data, setData}) => {
    let [open, setOpen] = useState(true)

    return <div className={style.armorRoot}>
        <div className={style.collapserBtn}>
            <Button minimal icon={open?"chevron-down":"chevron-right"} onClick={() => setOpen(!open)}>Броня</Button>
        </div>
        <div className={style.collapser}>
            <Collapse isOpen={open} keepChildrenMounted={true}>
                <div className={style.weapon}>
                    <Checkbox checked={data.armorInfo.armor} label={"Броня"}
                              onChange={() => service.setInfoData(data, setData, "armor",
                                  !data.armorInfo.armor, "armorInfo")}
                    />
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>AC</div>
                        <InputGroup value={data.armorInfo.ac || ""}
                                    placeholder={"AC"}
                                    onChange={e => service.setInfoData(data, setData, "ac",
                                        e.target.value, "armorInfo")}
                        />
                    </Label>
                    <Checkbox checked={data.armorInfo.stealth} label={"Помеха на скрытность"}
                              onChange={() => service.setInfoData(data, setData, "stealth",
                                  !data.armorInfo.stealth, "armorInfo")}
                    />
                    <Label>
                        <div className={"bp3-text-muted bp3-text-small"}>Сила</div>
                        <InputGroup value={data.armorInfo.strength || ""}
                                    placeholder={"Сила"}
                                    onChange={e => service.setInfoData(data, setData, "strength",
                                        e.target.value, "armorInfo")}
                        />
                    </Label>
                </div>
            </Collapse>
        </div>
    </div>
}

AdminItemEdit.protoType = {
    item: PropTypes.object.isRequired,
    setItem: PropTypes.func.isRequired
};


export default AdminItemEdit