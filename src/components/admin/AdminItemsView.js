import React, {useEffect, useState} from "react";
import style from "../../css/admin/AdminItemsView.module.css"
import {Button, ButtonGroup, InputGroup} from "@blueprintjs/core";
import {useBottomScrollListener} from "react-bottom-scroll-listener";
import {adminItemsViewService as adminService} from "../../logic/services/admin/AdminItemsViewService";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {addInvItemDialogService as service} from "../../logic/services/dialog/items/AddInvItemDialogService";
import AdminItemEdit from "./AdminItemEdit";

const AdminItemsView = () => {
    let [searchVal, setSearchVal] = useState("")
    let [page, setPage] = useState(0);
    let [loading, setLoading] = useState(false);
    let [list, setList] = useState([]);
    let [item, setItem] = useState(undefined);

    useEffect(() => {service.getPage("", 0, setLoading).then(res => {setList(res)})}, []);
    useEffect(() => {setPage(0); service.getPage(searchVal, 0, setLoading).then(res => {setList(res)})}, [searchVal])
    useEffect(() => {page !== 0 && service.getPage(searchVal, page, setLoading).then(res => {setList(list.concat(res))})}, [page])

    let scrollRef = useBottomScrollListener(() => {setPage(page+1)}, 0, 200)

    return (
        <div className={style.root}>
            <div className={style.leftPanel}>
                <InputGroup value={searchVal} type={"search"}
                            placeholder={"Поиск"}
                            onChange={e => {setSearchVal(e.target.value); scrollRef.current.scrollTo(0, 0)}}
                            leftIcon={"search"}/>
                <div className={style.list} ref={scrollRef}>
                    {list.map(item => <div key={item.id} className={style.listItem}
                                           onClick={() => {
                                               adminService.getItemFromView(item)
                                                   .then(res => setItem(res))
                                           }}>
                        <span>{item.name}</span>
                    </div>)}
                </div>
                <ButtonGroup fill>
                    <Button loading={loading} intent={Intent.PRIMARY}
                            onClick={() => adminService.getNew().then(res => setItem(res))}
                    >
                        Добавить
                    </Button>
                </ButtonGroup>
            </div>
            <div className={style.rightPanel}>
                {item
                    ? <AdminItemEdit item={item} setItem={setItem} setSearchVal={setSearchVal} searchVal={searchVal}/>
                    : <div className={style.placeHolder}>Нет пердмета</div>}
            </div>
        </div>
    )
}

export default AdminItemsView