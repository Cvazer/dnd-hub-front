import React, {Fragment, useEffect, useState} from "react";
import {Col, Container, Row} from 'react-grid-system'
import {Button, Card, Divider, Elevation, Icon, Intent, Tag} from "@blueprintjs/core";
import style from '../../css/admin/AdminView.module.css';
import {useNavigate} from "react-router";
import style2 from '../../css/CharOverview.module.css'
import userService from "../../logic/services/admin/UserService";
import cn from "classnames";
import LoadingView from "../LoadingView";

const AdminView = () => {
    let [online, setOnline] = useState([])
    let navigate = useNavigate()

    useEffect(() => {userService.online().then(res => setOnline(res))}, [])

    return (
        <Fragment>
            <Container>
                <Row>
                    <Col>
                        <ActionCard icon={"user"}
                                    title={"Аккаунты"}
                                    onClick={() => navigate("../admin/accounts", {replace: false})}
                                    body={
                                        <Fragment>
                                            <p>
                                                Управление ключами, аккаунтами и связанной с ними информацией.
                                            </p>
                                            <p>
                                                Здесь можно создавать новые ключи, привязывать к ним персонажей и менять
                                                информацию об аккаунте привязанном к ключу
                                            </p>
                                        </Fragment>
                                    }/>
                    </Col>
                    <Col>
                        <ActionCard icon={"briefcase"}
                                    title={"Предметы"}
                                    onClick={() => navigate("../admin/items", {replace: false})}
                                    body={
                                        <Fragment>
                                            <p>
                                                Управление всеми предметами персонажей,
                                                их свойствами и параметрами.
                                            </p>
                                            <p>
                                                Здесь можно создавать новые предметы,
                                                изменять старые и менять их параметры.
                                            </p>
                                        </Fragment>
                                    }/>
                    </Col>
                </Row>
                <br/>
                <br/>
                <br/>
                <Row>
                    <Col>
                        <ActionCard icon={"compass"}
                                    title={"Кампании"}
                                    onClick={() => navigate("../admin/campaigns", {replace: false})}
                                    body={
                                        <Fragment>
                                            <p>
                                                Управление кампаниями в которых играют игроки
                                                и происходят события.
                                            </p>
                                            <p>
                                                Здесь можно создавать кампании, изменять их
                                                активных участников (игроков), и добавлять в
                                                них новых персонажей
                                            </p>
                                        </Fragment>
                                    }/>
                    </Col>
                    <Col>
                        <ActionCard icon={"heat-grid"}
                                    title={"Скраппинг"}
                                    onClick={() => navigate("../admin/scrapping", {replace: false})}
                                    body={
                                        <Fragment>
                                            <p>
                                                Панель управления скраппингом ресурсов
                                            </p>
                                            <p>
                                                Здесь можно управлять процессом скраппинга ресурсов для заполнения
                                                базы данных новыми предметами, классами, расами и заклинаниями.
                                            </p>
                                        </Fragment>
                                    }/>
                    </Col>
                </Row>
                <br/>
                <br/>
                <br/>
                <Row>
                    <Col>
                        <Card elevation={Elevation.TWO}>
                            <h3 style={{textAlign: "center"}}>Пользователи online</h3>
                            <Divider/>
                            <br/>
                            {online.length === 0 ? <Fragment><LoadingView/><br/></Fragment>: <Online users={online}/>}
                            <Divider/>
                            <Button icon={"refresh"} fill
                                    intent={Intent.PRIMARY}
                                    onClick={() => userService.online().then(res => setOnline(res))}/>
                        </Card>
                    </Col>
                    <Col>

                    </Col>
                </Row>
            </Container>
        </Fragment>
    )
}

const Online = ({users}) => {
    return (
        <Fragment>
            {users.filter(e => e !== null).map(user => <div className={style.onlineCardContainer} key={user.key}>
                <div className={style2.commStatSectionContainer+" "+style.onlineCardValContainer}>
                    <Tag minimal large fill>{user.key}</Tag>
                    <div className={cn(style2.commStatSectionLabel, style.onlineCardUnderline,
                        "bp3-text-small", "bp3-text-muted")}>Ключ</div>
                </div>
                <div className={style2.commStatSectionContainer+" "+style.onlineCardValContainer}>
                    <Tag minimal large fill>{user.name}</Tag>
                    <div className={cn(style2.commStatSectionLabel, style.onlineCardUnderline,
                        "bp3-text-small", "bp3-text-muted")}>Имя</div>
                </div>
                <div className={style2.commStatSectionContainer+" "+style.onlineCardValContainer}>
                    <Tag minimal large fill>{user.role}</Tag>
                    <div className={cn(style2.commStatSectionLabel, style.onlineCardUnderline,
                        "bp3-text-small", "bp3-text-muted")}>Роль</div>
                </div>
            </div>)}
        </Fragment>
    )
}

const ActionCard = ({title, body, icon, onClick}) => {
    return (
        <Card className={style.actionCard}
              onClick={onClick}
              elevation={Elevation.ONE}
              interactive>
            <h3 className={style.cardHeader}>
                {title}
                <Icon icon={icon}
                      color={"#3f3f3f"}
                      iconSize={25}
                      className={style.cardIcon}/>
            </h3>
            <Divider/>
            <br/>
            <div className={style.cardBody}>
                {body}
            </div>
        </Card>
    )
}

export default AdminView