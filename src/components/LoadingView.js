import React from 'react';
import {Spinner} from "@blueprintjs/core";
import style from "../css/LoadingView.module.css"

const LoadingView = () => <div className={style.root}>
    <div className={style.container}>
        <Spinner/>
    </div>
</div>;

export default LoadingView