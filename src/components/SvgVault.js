import WagonSvg from '../css/svg/wagon.svg';
import SackSvg from '../css/svg/sack.svg';
import SaddleBagSvg from '../css/svg/saddleBag.svg';
import PortalSvg from '../css/svg/portal.svg';
import BagSvg from '../css/svg/bag.svg';
import Compass from '../css/svg/compas.svg';
import Arrow from '../css/svg/arrow.svg';
import Amulet from '../css/svg/amulet.svg';
import MagicHat from '../css/svg/magicHat.svg';
import Bomb from '../css/svg/bomb.svg';
import Meat from '../css/svg/meat.svg';
import Info from '../css/svg/information.svg';
import Chromosome from '../css/svg/chromosome.svg';
import MediumArmor from '../css/svg/mediumArmor.svg';
import LightArmor from '../css/svg/lightArmor.svg';
import MagicWand from '../css/svg/magic-wand.svg';
import Magician from '../css/svg/magician.svg';
import Triquetra from '../css/svg/triquetra.svg';
import MagicBall from '../css/svg/magic-ball.svg';
import Strength from '../css/svg/strength.svg';
import HeavyArmor from '../css/svg/heavyArmor.svg';
import Swords from '../css/svg/swords.svg';
import Sun from '../css/svg/sun.svg';
import Horse from '../css/svg/horse.svg';
import Question from '../css/svg/qustion.svg';
import Skull from '../css/svg/skull.svg';
import Potion from '../css/svg/potion.svg';
import Ring from '../css/svg/ring.svg';
import Rod from '../css/svg/rod.svg';
import Scroll from '../css/svg/scroll.svg';
import Shield from '../css/svg/shield.svg';
import Beads from '../css/svg/beads.svg';
import Tattoo from '../css/svg/tatoo.svg';
import Hammer from '../css/svg/hammer.svg';
import Diamond from '../css/svg/diamond.svg';
import Airship from '../css/svg/airship.svg';
import Wand from '../css/svg/wand.svg';
import Ship from '../css/svg/ship.svg';
import Tongs from '../css/svg/tongs.svg';
import Chess from '../css/svg/chess.svg';
import Music from '../css/svg/music.svg';
import Bow from '../css/svg/bow.svg';
import Gold from '../css/svg/gold.svg';
import Weight from '../css/svg/weight.svg';
import Coin from '../css/svg/coin.svg';
import OneSword from '../css/svg/one-sword.svg';
import Target from '../css/svg/target.svg';
import SimpleBow from '../css/svg/simplBow.svg';
import Incognito from '../css/svg/incognito.svg';
import Check from '../css/svg/check.svg';
import Cancel from '../css/svg/cancel.svg';
import All from '../css/svg/all.svg';
import List from '../css/svg/list.svg';
import Moon from '../css/svg/moon.svg';
import SunRise from '../css/svg/sunrise.svg';

export default {
    wagon: WagonSvg,
    sack: SackSvg,
    saddleBag: SaddleBagSvg,
    portal: PortalSvg,
    bag: BagSvg,
    question: Question,
    weight: Weight,
    coin: Coin,
    shield: Shield,
    oneSword: OneSword,
    target: Target,
    simpleBow: SimpleBow,
    incognito: Incognito,
    check: Check,
    cancel: Cancel,
    strength: Strength,
    sun: Sun,
    all: All,
    list: List,
    moon: Moon,
    sunrise: SunRise,
    info: Info
}

export const itemCats = {
    ADVENTURING_GEAR: Compass,
    AMMUNITION: Arrow,
    AMULET: Amulet,
    ELDRITCH_MACHINE: MagicHat,
    EXPLOSIVE: Bomb,
    FOOD_AND_DRINK: Meat,
    GENERIC_VARIANT: Chromosome,
    HEAVY_ARMOR: HeavyArmor,
    LIGHT_ARMOR: LightArmor,
    MEDIUM_ARMOR: MediumArmor,
    MELEE_WEAPON: Swords,
    MOUNT: Horse,
    OTHER: Triquetra,
    // OTHER: Question,
    POISON: Skull,
    POTION: Potion,
    RING: Ring,
    ROD: Rod,
    SCROLL: Scroll,
    SHIELD: Shield,
    SPELLCASTING_FOCUS: Beads,
    TACK_AND_HARNESS: SaddleBagSvg,
    TATTOO: Tattoo,
    TOOLS: Hammer,
    TREASURE: Diamond,
    VEHICLE_AIR: Airship,
    VEHICLE_LAND: WagonSvg,
    WAND: Wand,
    VEHICLE_WATER: Ship,
    ARTISANS_TOOLS: Tongs,
    GAMING_SET: Chess,
    INSTRUMENT: Music,
    RANGED_WEAPON: Bow,
    TRADE_GOOD: Gold,
    ALL: All
}