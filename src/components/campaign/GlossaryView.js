import React, {useEffect, useState} from 'react'
import style from '../../css/campaign/GlossaryView.module.css';
import {Col, Container, Row} from "react-grid-system";
import {Button, Divider, InputGroup, Intent} from "@blueprintjs/core";
import glossaryService from "../../logic/services/campaign/GlossaryService";

const GlossaryView = () => {
    let [search, setSearch] = useState("")
    let [list, setList] = useState([])
    let [loading, setLoading] = useState(false)
    let [pageNum, setPageNum] = useState(0)

    let loadList = num => {
        setLoading(true)
        return glossaryService.load(search, num, 14)
            .then(res => {setLoading(false); return res})
    }

    useEffect(() => {loadList(pageNum).then(res => setList(res))}, [])
    useEffect(() => {loadList(pageNum).then(res => {setList(res); setPageNum(0)})}, [search])
    useEffect(() => {loadList(pageNum).then(res => setList(list.concat(res)))}, [pageNum])

    return (
        <Container>
            <Row>
                <InputGroup fill type="search" value={search}
                            onChange={e => setSearch(e.target.value)}
                            leftIcon={"search"} placeholder={"Поиск записи..."}/>
            </Row>
            <br/>
            <Row>
                <Col>
                    <div className={style.contentRoot}>
                        {list.map(e => <GlossaryListItem key={e.id} item={e}/>)}
                        <div className={style.moreBtn}>
                            <Button minimal fill loading={loading}
                                    onClick={() => setPageNum(pageNum+1)}
                                    intent={Intent.SUCCESS}>
                                Загрузить еще
                            </Button>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}

const GlossaryListItem = ({item}) => {
    return (
        <div className={style.listItemRoot}>{item.name}</div>
    )
}

export default GlossaryView