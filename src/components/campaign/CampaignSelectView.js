import React, {Fragment, useEffect, useState} from "react";
import {useNavigate} from "react-router";
import {useSelector} from "react-redux";
import LoadingView from "../LoadingView";
import {Container} from "react-grid-system"
import campaignService from "../../logic/services/CampaignService";
import {Card, Divider, Elevation, Icon} from "@blueprintjs/core";
import style from "../../css/campaign/CampaignSelectView.module.css";

const CampaignSelectView = () => {
    let key = useSelector(store => store.account.key)
    let campaignId = useSelector(store => store.campaign.campaign && store.campaign.campaign.id)
    let navigate = useNavigate()

    let [list, setList] = useState([])
    let [loading, setLoading] = useState(true)

    useEffect(() => {
        if (campaignId) {
            navigate("../campaign/"+campaignId, {replace: false})
        } else {
            setLoading(true)
            campaignService.list(key)
                .then(res => setList(res))
                .then(() => setLoading(false))
                .catch(() => setLoading(false))
        }
        return () => {}
    }, [])

    let getRole = (role) => role === "master"?"Мастер":"Игрок"

    return loading ? <LoadingView/> : (
        <Fragment>
            <Container>
                {list.map(c => <Fragment key={c.id}>
                    <Card elevation={Elevation.TWO} interactive
                          onClick={() => {navigate("../campaign/"+c.id, {replace: false})}}>
                        <div className={style.cardHeader}>{c.name}</div>
                        <Divider/>
                        {c.players.map(player => <div className={style.cardContainer} key={player.key}>
                            <div className={style.cardBody}>
                                <div className={style.cardBodyIcon}>
                                    <Icon icon={"person"}
                                          iconSize={24}
                                          color={"#6a6a6a"}/>
                                </div>
                                {player.charName
                                    ? <div className={style.cardBodyText}>
                                        <b>{player.name}</b>: {player.charName} ({player.charRace}) {player.charLvl} LVL
                                    </div>
                                    : <div className={style.cardBodyText}>
                                        <b>{player.name}</b>: {getRole(player.role)}
                                    </div>
                                }
                            </div>
                            <div className={style.cardDivider}/>
                        </div>)}
                    </Card>
                    <br/>
                    <br/>
                </Fragment>)}
            </Container>
        </Fragment>
    )
}

export default CampaignSelectView