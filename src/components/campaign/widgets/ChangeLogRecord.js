import React, {useEffect, useState} from "react";
import {Button, ButtonGroup, Dialog, Divider, Intent, TextArea} from "@blueprintjs/core";
import cn from "classnames";
import * as Classes from "@blueprintjs/core/lib/cjs/common/classes";
import style from "../../../css/CharOverview.module.css";
import {useSelector} from "react-redux";
import dialogueService, {Dialog as DG} from "../../../logic/services/DialogService";
import changeLogRecordService from "../../../logic/services/campaign/ChangeLogRecordService";

const ChangeLogRecord = () => {
    let state = useSelector(store => store.dialog[DG.CampaignChangeRecord]);
    let data = useSelector(store => store.dialog[DG.CampaignChangeRecord].context);

    let [loading, setLoading] = useState(false);
    let [text, setText] = useState(data && data.textData);

    let close = () => dialogueService.setState(DG.CampaignChangeRecord, {open: false, context: undefined});

    useEffect(() => data && setText(data.textData), [data]);

    return (
        <Dialog isOpen={state.open}
                usePortal
                onClose={() => close()}
                icon={"edit"}
                isCloseButtonShown
                title={"Изменить запись"}
                className={cn(Classes.OVERLAY_SCROLL_CONTAINER, style.hpDialog)}>
            <div className={style.hpDialogContainer}>
                <TextArea value={text} fill
                          style={{height: "200px"}}
                          onChange={e => setText(e.target.value)}/>
                <div style={{marginTop: "15px"}}/>
                <Divider/>
                <div style={{marginTop: "15px"}}/>
                <ButtonGroup fill>
                    {/*<Button fill intent={Intent.DANGER} loading={loading}>*/}
                    {/*    Удалить*/}
                    {/*</Button>*/}
                    <Button fill intent={Intent.SUCCESS} loading={loading}
                            onClick={() => {
                                setLoading(true)
                                changeLogRecordService.setText(text, data.id)
                                    .then(() => setLoading(false))
                                    .then(() => close())
                                    .catch(() => setLoading(false))
                            }}>
                        Сохранить
                    </Button>
                </ButtonGroup>
            </div>
        </Dialog>
    )
};

export default ChangeLogRecord