import React, {Fragment, useEffect, useRef} from "react";
import style from "../../../css/campaign/widgets/TimeOfDayWidget.module.css"
import SvgVault from "../../SvgVault";
import SvgIcon from "../../common/SvgIcon";

const TimeOfDayWidget = ({date}) => {
    let scaleRef = useRef(null)
    let sliderRef = useRef(null)

    useEffect(() => {
        if (!date) return
        let element = Array.from(scaleRef.current.childNodes)
            .find(n => n.textContent===date.hour+"")
        let x = element.offsetLeft
        let width = element.clientWidth
        sliderRef.current.style.left = x+"px"
        sliderRef.current.style.visibility = "visible"
        sliderRef.current.style.width = width+1+"px"
    }, [date])

    return (
        <Fragment>
            <div className={style.root}>
                <div className={style.gradient}>
                    <SvgIcon src={SvgVault.sunrise} size={40} className={style.sunrise} fill={"#FFFFFF"}/>
                    <SvgIcon src={SvgVault.sun} size={35}/>
                    <SvgIcon src={SvgVault.moon} size={28} className={style.moon} fill={"#FFFFFF"}/>
                </div>
                <div className={style.scale} ref={scaleRef}>
                    <div className={style.scaleSlider} ref={sliderRef}/>
                    {Array(24).fill(0)
                        .map((x, y) => x + y)
                        .map((hour, i) =>
                            <span className={style.scaleHour}
                                  style={{
                                      // position: "absolute",
                                      // left: ref.current.offsetWidth+"px"
                                  }}
                                  key={"hour"+hour}>
                                {hour}
                            </span>
                        )}
                </div>
            </div>
        </Fragment>
    )
}

export default TimeOfDayWidget