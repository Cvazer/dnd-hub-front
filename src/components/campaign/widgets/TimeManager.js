import React, {Fragment, useState} from "react";
import campaignService from "../../../logic/services/CampaignService";
import {Button, Card, ControlGroup, Elevation, InputGroup, Intent} from "@blueprintjs/core";
import TimeOfDayWidget from "./TimeOfDayWidget";
import {date} from "../../../model/ICUDateTime";
import style from "../../../css/campaign/widgets/TimeManager.module.css";
import timeManagerService from "../../../logic/services/campaign/CampaignTimeManagerService";
import ICUDatePicker from "../../common/ICUDatePicker";

const TimeManager = ({campaign, role}) => {
    let setCurrentDate = date => campaignService.setCurrentDate(date, campaign.id)

    let [src, setSrc] = useState("")

    return (
        <Card elevation={Elevation.TWO}>
            <TimeOfDayWidget date={date(campaign.currentDateTime)}/>
            <br/>
            { role==="master" &&
            <Fragment>
                <div className={style.timeWidgetControlPanel}>
                    <ControlGroup fill>
                        <InputGroup fill placeholder={"Выражение времени..."}
                                    value={src} onChange={e => setSrc(e.target.value)}/>
                        <Button icon="plus" intent={Intent.SUCCESS}
                                onClick={e => {
                                    timeManagerService
                                        .changeTime(date(campaign.currentDateTime), src, 1)
                                        .then(() => setSrc(""))
                                }}/>
                        <Button icon="minus" intent={Intent.DANGER}
                                onClick={e => {
                                    timeManagerService
                                        .changeTime(date(campaign.currentDateTime), src, -1)
                                        .then(() => setSrc(""))
                                }}/>
                    </ControlGroup>
                </div>
                <br/>
            </Fragment>
            }
            <ICUDatePicker maxWidth={"none"} viewMode={role==="player"}
                           onValidValue={role==="master"?setCurrentDate:()=>{}}
                           value={date(campaign.currentDateTime)}/>
            <br/>
            <div className={style.timeWidgetTextContainer}>
                <span>{date(campaign.currentDateTime).string} | {date(campaign.currentDateTime).fancyString}</span>
            </div>
        </Card>
    )
}

export default TimeManager