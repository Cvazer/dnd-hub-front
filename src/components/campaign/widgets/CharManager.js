import React, {useEffect, useState} from "react";
import charManagerService from "../../../logic/services/campaign/CampaignCharManagerService";
import {Button, Card, Elevation, HTMLTable} from "@blueprintjs/core";
import {Select} from "@blueprintjs/select";
import style from "../../../css/campaign/widgets/CharManager.module.css";

const CharManager = ({campaign}) => {
    let [chars, setChars] = useState([])
    let [activeChar, setActiveChar] = useState()

    let setChar = char => {
        if (activeChar && char.id === activeChar.id) return
        charManagerService.setActiveChar(char).then(() => setActiveChar(char))
    }

    useEffect(() => {
        if (!campaign) return
        if (!campaign.id) return;
        charManagerService.getCharsList().then(list => {
            if (list.length === 0) return
            setChars(list)
            charManagerService.getActiveChar().then(id => {
                if (id === "OK") return
                setActiveChar(list.find(c => c.id === +id))
            })
        })
    }, [campaign])

    return (
        <Card elevation={Elevation.TWO}>
            <Select items={chars}
                    popoverProps={{fill: true}}
                    filterable={false}
                    onItemSelect={item => setChar(item)}
                    itemRenderer={(item, props) =>
                        <div key={item.id}
                             onClick={props.handleClick}
                             className={style.charManagerListItem}>
                            {item.name} -- {item.lvl+" LVL"}
                        </div>
                    }>
                <Button fill>
                    {activeChar
                        ? `${activeChar.name}  ---  ${activeChar.lvl} LVL`
                        : "Нет активного персонажа"}
                </Button>
            </Select>
            <br/>
            {activeChar && <HTMLTable bordered striped
                                      cellSpacing={"center"}
                                      className={style.charManagerTable}>
                <thead><tr>
                    <th style={{textAlign: "center"}}>Класс</th>
                    <th style={{textAlign: "center"}}>Подкласс</th>
                    <th style={{textAlign: "center"}}>Уровень</th>
                </tr></thead>
                <tbody>
                {activeChar.classes && activeChar.classes.map(c => <tr key={c.id+""}>
                    <td style={{textAlign: "center"}}>{c.name}</td>
                    <td style={{textAlign: "center"}}>{c.subname || "--\\--"}</td>
                    <td style={{textAlign: "center"}}>{c.lvl}</td>
                </tr>)}
                </tbody>
            </HTMLTable>}
        </Card>
    )
}

export default CharManager