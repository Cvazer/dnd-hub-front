import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {animateScroll} from "react-scroll";
import eventLogService from "../../../logic/services/campaign/CampaignEventLogService";
import {dispatch} from "../../../store/ConfigureStore";
import campaignReducerActionsFactory from "../../../store/reducers/CampaignReducer";
import charManagerService from "../../../logic/services/campaign/CampaignCharManagerService";
import ws from "../../../logic/websocket/WSService";
import style from "../../../css/campaign/widgets/EventLog.module.css";
import style2 from "../../../css/campaign/widgets/CharManager.module.css";
import {Button, Card, ControlGroup, Elevation, InputGroup, Intent} from "@blueprintjs/core";
import {Select} from "@blueprintjs/select";
import {date} from "../../../model/ICUDateTime";
import dialogueService, {Dialog} from "../../../logic/services/DialogService";

let subscription;
let updateSubscription;

const EventLog = ({campaign}) => {
    let log = useSelector(store => store.campaign.log)
    let role = useSelector(store => store.campaign.role)

    let additionalActions = [{
        id: -1,
        name: "Событие"
    }]

    let [actionText, setActionText] = useState("")
    let [chars, setChars] = useState([])
    let [activeChar, setActiveChar] = useState(additionalActions[0])
    let [scroll, setScroll] = useState(true)
    let [logPage, setLogPage] = useState(0)

    useEffect(() => {
        scroll && animateScroll.scrollToBottom({
            containerId: "screen",
            duration: 0
        })
    }, [log])

    useEffect(() => {
        if (!campaign) return
        eventLogService.getRecords(0)
            .then(res => dispatch(campaignReducerActionsFactory.setLog(res)))
        role === "master" && charManagerService.getAllActiveChars()
            .then(res => setChars(additionalActions.concat(res)))
        setLogPage(0)
    }, [campaign])

    useEffect(() => {
        setTimeout( () => {
            subscription = ws.subscribe("/topic/log/"+campaign.id, frame =>
                eventLogService.append([JSON.parse(frame.body)]));
            updateSubscription = ws.subscribe(`/topic/log/${campaign.id}/updates`, frame =>
                eventLogService.update(JSON.parse(frame.body)))
        }, 1000);
        return () => {
            subscription && subscription.unsubscribe();
            updateSubscription && updateSubscription.unsubscribe()
        }
    }, [])

    let sendAction = () => {
        setActionText("");
        eventLogService.logCharAction(actionText)
    }

    let sendActionMaster = () => {
        if (!activeChar) return
        setActionText("");
        if (activeChar.id === -1) {
            eventLogService.logGenericEvent(actionText)
        } else {
            eventLogService.logCharAction(actionText, activeChar.id)
        }
    }

    let Record = ({record, role}) => {
        return <div key={record.id}
                    onClick={() => dialogueService.setState(Dialog.CampaignChangeRecord, {
                        open: true,
                        context: {...record}
                    })}
                    className={style.eventLogScreenItemCharAction}>
            {record.type.id === "CHAR_ACTION" && (
                <div>
                    {date(record.gameDate).stringShort + ` | `}
                    <b>{record.char.name}</b>
                    {" "}{record.textData}
                </div>
            )}
            {record.type.id === "GENERIC_EVENT" && (
                <div>
                    {date(record.gameDate).stringShort + ` | `}
                    {record.textData}
                </div>
            )}
        </div>
    }

    return (
        <Card elevation={Elevation.TWO}>
            <div className={style.eventLogRoot}>
                <div className={style.eventLogScreen} id={"screen"} onScroll={(e) => {
                    if (e.target.scrollTop === 0) {
                        setScroll(false)
                        eventLogService.getRecords(logPage+1)
                            .then(res => {eventLogService.tail(res); return res})
                            .then(res => {
                                res.length !== 0 && animateScroll.scrollMore(440, {
                                    containerId: "screen",
                                    duration: 0,
                                    isDynamic: true
                                });
                                return res
                            })
                            .then(() => setScroll(true))
                            .then(() => setLogPage(logPage+1))
                            .catch(() => setScroll(true))
                    }
                }}>
                    {[...log].reverse().map(record => <Record record={record} role={role} key={record.id}/>)}
                </div>
                <div style={{marginTop: "10px"}}>
                    { role !== "master"
                        ? <ControlGroup fill>
                            <InputGroup fill placeholder={"Что делает персонаж..."}
                                        onKeyPress={e => e.key === 'Enter' && sendAction()}
                                        value={actionText} onChange={e => setActionText(e.target.value)}/>
                            <Button icon="send-message" intent={Intent.PRIMARY}
                                    onClick={() => sendAction()}/>
                        </ControlGroup>
                        : <ControlGroup fill>
                            <div style={{width: "80px"}}>
                                <Select items={chars}
                                        popoverProps={{fill: true}}
                                        filterable={false}
                                        onItemSelect={item => setActiveChar(item)}
                                        itemRenderer={(item, props) =>
                                            <div key={item.id}
                                                 onClick={props.handleClick}
                                                 className={style2.charManagerListItem}>
                                                {item.name}
                                            </div>
                                        }>
                                    <Button fill>
                                        <div className={style.eventLogScreenAdminBtnText}>
                                            {activeChar
                                                ? activeChar.name
                                                : "Нет"}
                                        </div>
                                    </Button>
                                </Select>
                            </div>
                            <InputGroup fill placeholder={"Текст события..."}
                                        onKeyPress={e => e.key === 'Enter' && sendActionMaster()}
                                        value={actionText} onChange={e => setActionText(e.target.value)}/>
                            <Button icon="send-message" intent={Intent.PRIMARY}
                                    onClick={() => sendActionMaster()}/>
                        </ControlGroup>
                    }
                </div>
            </div>
        </Card>
    )
}

export default EventLog