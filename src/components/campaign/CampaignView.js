import React, {Fragment, useEffect, useState} from "react";
import {useParams, useNavigate} from "react-router";
import LoadingView from "../LoadingView";
import {Container} from 'react-grid-system'
import {useSelector} from "react-redux";
import campaignService from "../../logic/services/CampaignService";
import {Button, Card, ControlGroup, Elevation, HTMLTable, InputGroup, Intent} from "@blueprintjs/core";
import ICUDateTime, {date} from "../../model/ICUDateTime";
import ICUDatePicker from "../common/ICUDatePicker";
import TimeOfDayWidget from "./widgets/TimeOfDayWidget";
import style from "../../css/campaign/CampaignView.module.css"
import timeManagerService from "../../logic/services/campaign/CampaignTimeManagerService"
import charManagerService from "../../logic/services/campaign/CampaignCharManagerService"
import {Select} from "@blueprintjs/select";
import EventLog from "./widgets/EventLog";
import TimeManager from "./widgets/TimeManager";
import CharManager from "./widgets/CharManager";
import ChangeLogRecord from "./widgets/ChangeLogRecord";

const CampaignView = () => {
    let [loading, setLoading] = useState(true)
    let navigate = useNavigate()

    let key = useSelector(store => store.account.key)
    let {campaign, role} = useSelector(store => store.campaign)

    let {id} = useParams()

    useEffect(() => {
        setLoading(true)
        key && id && campaignService.getActiveCharId(id, key)
        id && campaignService.getCampaign(id)
            .then(() => setLoading(false))
            .catch(() => setLoading(false))
        return () => {}
    }, [])

    return loading ? <LoadingView/> : (
        <Fragment>
            <ChangeLogRecord/>
            <Container>
                <TimeManager campaign={campaign} role={role}/>
                {/*<div style={{display: "flex", justifyContent: "center"}}>*/}
                {/*    <DateField value={date} onClose={setCurrentDate}>*/}
                {/*        {date ? date.string : "Выбор даты"}*/}
                {/*    </DateField>*/}
                {/*</div>*/}
                <br/>
                <EventLog campaign={campaign}/>
                <br/>
                <CharManager campaign={campaign}/>
                <br/>
                <Button minimal fill large intent={Intent.WARNING} onClick={() => {
                    setLoading(true)
                    campaignService.setActiveCharId(undefined)
                        .then(() => campaignService.setCampaign(undefined)
                            .then(() => navigate("/campaign/", {replace: false})))
                }}>
                    Выбрать капманию
                </Button>
                <br/>
            </Container>
        </Fragment>
    )
}

export default CampaignView