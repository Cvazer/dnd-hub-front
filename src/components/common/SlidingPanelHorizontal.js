import React from "react";
import * as PropTypes from "prop-types";
import style from '../../css/common/SlidingPanelHorizontal.css'
import cn from 'classnames'

const SlidingPanelHorizontal = ({
                                    rootClass = "",
                                    sliderClass = "",
                                    panelClass = "",
                                    elements = [],
}) => {
    return (
        <div className={cn(style.root, rootClass)}>
            <div className={cn(style.slider, sliderClass)}>
                asd
            </div>
        </div>
    )
}

export class SlidingPanelElement {
    constructor(sectionCallback, panelCallback) {
        this._sectionCallback = sectionCallback;
        this._panelCallback = panelCallback
    }

    getSection() {return this._sectionCallback()}
    getPanel() {return this._panelCallback()}
}

SlidingPanelHorizontal.propTyps = {
    rootClass: PropTypes.string,
    sliderClass: PropTypes.string,
    panelClass: PropTypes.string,
    elements: PropTypes.array
}

export default SlidingPanelHorizontal