import React from 'react'
import {ReactSVG} from "react-svg";

const SvgIcon = ({src, size = 20, fill = "#000000", className = ""}) => {
    return <div className={className}>
        <ReactSVG src={src} beforeInjection={(svg) => {svg.setAttribute('style',
            `width: ${size}px; ` +
            `height: ${size}px; ` +
            `fill: ${fill}`)}}/>
    </div>
}

export default SvgIcon