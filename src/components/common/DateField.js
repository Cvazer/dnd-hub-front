import React, {Fragment, useRef, useState} from "react";
import {Button} from "@blueprintjs/core";
import {Popover2} from "@blueprintjs/popover2";
import ICUDatePicker from "./ICUDatePicker";
import style from "../../css/common/ICUDatePicker.module.css"

const DateField = ({
                       children,
                       intent,
                       fill,
                       rightIcon = "calendar",
                       large,
                       minimal = true,
                       defMonth = "Январь",
                       defYear = 1,
                       defAge = "Силы",
                       defHour = "0",
                       defMinute = "00",
                       defSecond = "00",
                       currentDay,
                       onValidValue,
                       onClose,
                       value
                   }) => {

    let [date, setDate] = useState(value)

    return (
        <Fragment>
            <Popover2 content={<ICUDatePicker defYear={defYear}
                                              defAge={defAge}
                                              defHour={defHour}
                                              defMinute={defMinute}
                                              defSecond={defSecond}
                                              currentDay={currentDay}
                                              onValidValue={(date) => {
                                                  setDate(date)
                                                  onValidValue && onValidValue(date)
                                              }}
                                              value={value}
                                              defMonth={defMonth} />}
                      onClose={() => onClose && onClose(date)}
                      popoverClassName={style.fix}>
                <Button intent={intent} fill={true}
                        large={large} minimal={minimal}
                        rightIcon={rightIcon}>
                    <div style={{transform: "translateY(0px)"}}>
                        {children}
                    </div>
                </Button>
            </Popover2>
        </Fragment>
    )
}

export default DateField