import React, {useEffect, useRef, useState} from "react";
import style from "../../css/common/ICUDatePicker.module.css"
import {HTMLSelect, Icon, InputGroup, Intent, NumericInput} from "@blueprintjs/core";
import ICUDateTime from "../../model/ICUDateTime";
import cn from 'classnames'

const daysOfMonth = [
    {name: "Пн", num: 1},
    {name: "Вт", num: 2},
    {name: "Ср", num: 3},
    {name: "Чт", num: 4},
    {name: "Пт", num: 5},
    {name: "Сб", num: 6},
    {name: "Вс", num: 0}
    ]
const ages = [
    {name: "Силы", num: 0},
    {name: "Драконов", num: 1},
    {name: "Магии", num: 2},
    {name: "Процветания", num: 3},
    {name: "Возрождения", num: 4},
]
const days = [
    ["1", "2", "3", "4", "5", "6", "7"],
    ["8", "9", "10", "11", "12", "13", "14"],
    ["15", "16", "17", "18", "19", "20", "21"],
    ["22", "23", "24", "25", "26", "27", "28"]
]
const months = [
    {name: "Январь", num: 1},
    {name: "Февраль", num: 2},
    {name: "Март", num: 3},
    {name: "Апрель", num: 4},
    {name: "Май", num: 5},
    {name: "Июнь", num: 6},
    {name: "Сол", num: 7},
    {name: "Июль", num: 8},
    {name: "Август", num: 9},
    {name: "Сентябрь", num: 10},
    {name: "Октябрь", num: 11},
    {name: "Ноябрь", num: 12},
    {name: "Декабрь", num: 13}
]

const ICUDatePicker = ({
                           defMonth = "Январь",
                           defYear = 1,
                           defAge = "Силы",
                           defHour = "0",
                           defMinute = "00",
                           defSecond = "00",
                           currentDay,
                           maxWidth = "300px",
                           editable = true,
                           viewMode = false,
                           value,
                           onValidValue = () => {}
                       }) => {
    let [selected, setSelected] = useState(value?value.dayOfMonth+"":currentDay+"")

    let [month, setMonth] = useState(value
        ? months.find(m => m.num === value.month).name
        : defMonth)

    let [age, setAge] = useState(value
        ? ages.find(a => a.num === value.age).name
        : defAge)

    let [year, setYear] = useState(value ? value.year : defYear)
    let [yearInt, setYearInt] = useState(defYear===0?Intent.DANGER:Intent.NONE)

    let [hour, setHour] = useState(value?value.hour+"":defHour+"")
    let [minute, setMinute] = useState(value?value.minute+"":defMinute+"")
    let [second, setSecond] = useState(value?value.second+"":defSecond+"")
    let [hourInt, setHourInt] = useState(Intent.NONE)
    let [minInt, setMinInt] = useState(Intent.NONE)
    let [secInt, setSecInt] = useState(Intent.NONE)

    useEffect(() => {
        if (!value) return
        setSelected(value?value.dayOfMonth+"":currentDay+"")
        setMonth(value
            ? months.find(m => m.num === value.month).name
            : defMonth)
        setAge(value
            ? ages.find(a => a.num === value.age).name
            : defAge)
        setYear(value ? value.year : defYear)
        setYearInt(defYear===0?Intent.DANGER:Intent.NONE)
        setHour(value?value.hour+"":defHour+"")
        setMinute(value?value.minute+"":defMinute+"")
        setSecond(value?value.second+"":defSecond+"")
        setHourInt(Intent.NONE)
        setMinInt(Intent.NONE)
        setSecInt(Intent.NONE)
    }, [value])

    let [flipper, setFlipper] = useState(false)

    const nextMonth = (direction) => {
        let currMonthNum = months.find(m => m.name === month).num
        let next = months.find(m => m.num === (direction > 0
            ? (currMonthNum === 13 ? 1 : currMonthNum + 1)
            : (currMonthNum === 1 ? 13 : currMonthNum - 1))
        )
        setMonth(next.name)
    }

    const testTimeValue = (val) => /^[0-9]+$/.test(val)

    const setTimeValue = (val, setter, intent, max) => {
        testTimeValue(val) ? intent(Intent.NONE) : intent(Intent.DANGER);
        +val <= max ? intent(Intent.NONE) : intent(Intent.DANGER);
        setter(val)
    }

    useEffect(() => save(), [age, month, selected, flipper])

    let constructDate = () => {
        let a = ages.find(e => e.name === age).num;
        let m = months.find(e => e.name === month);
        let doy = ((m.num-1)*28)+(+selected);
        return new ICUDateTime(a, +year, +doy, +hour, +minute, +second)
    }

    let save = () => {
        if (+year < 1) return;
        if (!testTimeValue(hour)) return;
        if (!testTimeValue(minute)) return;
        if (!testTimeValue(second)) return;
        if (!selected) return;
        if (!(days.flat().includes(selected))) return;
        if (hour > 23) return;
        if (minute > 59) return;
        if (second > 59) return;

        onValidValue(constructDate());
    }

    let ref = useRef()

    return (
        <div className={style.root} style={{maxWidth: maxWidth}}>
            <div className={style.header}>
                <div className={style.monthSwitchControl} onClick={() => editable && nextMonth(-1)}>
                    <Icon icon={"chevron-left"}/>
                </div>
                <div className={style.selectors}>
                    <div className={style.monthSelector}>
                        <HTMLSelect fill minimal options={months.map(e => e.name)}
                                    disabled={!editable}
                                    onChange={e => setMonth(e.target.value)}
                                    value={month}/>
                    </div>
                    <div className={style.yearSelector}>
                        <NumericInput className={style.yearSelectorInput} ref={ref}
                                      disabled={!editable}
                                      onClick={(e) => {e.target.select()}}
                                      onKeyPress={e => e.key === 'Enter' && e.target.blur()}
                                      onButtonClick={e => setFlipper(!flipper)}
                                      onBlur={() => setFlipper(!flipper)}
                                      intent={yearInt}
                                      onValueChange={e => {
                                          e < 1
                                              ? setYearInt(Intent.DANGER)
                                              : setYearInt(Intent.NONE)
                                          setYear(e)
                                      }}
                                      value={year} fill minimal/>
                    </div>
                </div>
                <div className={style.monthSwitchControl} onClick={() => editable && nextMonth(1)}>
                    <Icon icon={"chevron-right"}/>
                </div>
            </div>
            <div className={style.headerBodyDivider}/>
            <div className={style.body}>
                <div className={style.bodyHeader}>
                    {daysOfMonth.map(m => <div className={style.bodyHeaderElement} key={m.num}>{m.name}</div>)}
                </div>
                {days.map((row, i) => <div className={style.bodyRow} key={i}>
                    {row.map(v =>
                        <div className={cn(
                                style.bodyRowElement,
                                viewMode
                                    ? selected===v && constructDate().string === value.string && style.selected
                                    : selected===v && style.selected
                             )}
                             key={v}
                             onClick={() => !viewMode && editable && setSelected(v)}>
                        {v}
                        </div>)}
                </div>)}
            </div>
            <div className={style.footer}>
                <div className={style.ageInput}>
                    <HTMLSelect minimal fill options={ages.map(e => e.name)}
                                disabled={!editable}
                                onChange={e => setAge(e.target.value)}
                                value={age}
                                className={style.ageInputSelect}/>
                </div>
                <div className={style.timeInput}>
                    <InputGroup small fill className={style.timeInputField}
                                disabled={!editable || viewMode}
                                onChange={e => setTimeValue(e.target.value, setHour, setHourInt, 23)}
                                onBlur={() => setFlipper(!flipper)}
                                onKeyPress={e => e.key === 'Enter' && e.target.blur()}
                                intent={hourInt}
                                onClick={(e) => {e.target.select()}}
                                value={hour}/>
                    <div className={style.timeInputDivider}>:</div>
                    <InputGroup small fill className={style.timeInputField}
                                disabled={!editable || viewMode}
                                intent={minInt}
                                onClick={(e) => {e.target.select()}}
                                onChange={e => setTimeValue(e.target.value, setMinute, setMinInt, 59)}
                                onBlur={() => setFlipper(!flipper)}
                                onKeyPress={e => e.key === 'Enter' && e.target.blur()}
                                value={minute}/>
                    <div className={style.timeInputDivider}>:</div>
                    <InputGroup small fill className={style.timeInputField}
                                disabled={!editable || viewMode}
                                intent={secInt}
                                onClick={(e) => {e.target.select()}}
                                onChange={e => setTimeValue(e.target.value, setSecond, setSecInt, 59)}
                                onBlur={() => setFlipper(!flipper)}
                                onKeyPress={e => e.key === 'Enter' && e.target.blur()}
                                value={second}/>
                </div>
            </div>
        </div>
    )
}

export default ICUDatePicker