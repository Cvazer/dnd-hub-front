import dialogStyle from "../../css/common/Counter.module.css";
import {Icon, NumericInput} from "@blueprintjs/core";
import React, {useState} from "react";
import * as PropTypes from "prop-types";

const Counter = ({fontSize, iconSize, value, callback, step, minimalValue = 0, maxValue}) => {
    let incCounter = (value, setter, direction) => {
        if (!value.match(/^(|0|[1-9][0-9]{0,8})$/)) return;
        let newVal = +value + ((step || 1) * direction);
        if (+value <= minimalValue && direction < 0) return;
        if (+value >= maxValue && direction > 0 && maxValue) return;
        setter(newVal)
    };

    let setCounter = (value, setter) => {
        if (!((value+"").match(/^(|0|[1-9][0-9]{0,8})$/))) return;
        let newVal = +value
        if (+value <= minimalValue) return;
        if (+value > maxValue && maxValue) return;
        setter(newVal)
    };

    let [input, setInput] = useState(false)

    return (
        <div className={dialogStyle.counterContainer}>
            <div className={dialogStyle.counterControl} onClick={() => incCounter(value+"", callback, -1)}>
                <Icon icon={"remove"} iconSize={iconSize?iconSize:40} className={"bp3-text-muted"}/>
            </div>
            {input
                ? <NumericInput onBlur={() => setInput(false)}
                                buttonPosition="none" fill
                                className={dialogStyle.numInput}
                                defaultValue={value}
                                onValueChange={e => setCounter(e, callback)}/>
                : <div className={dialogStyle.counterValue}
                       style={{fontSize: fontSize?fontSize:"28pt"}}
                       onClick={() => setInput(true)}>
                    {value}
                </div>
            }
            <div className={dialogStyle.counterControl} onClick={() => incCounter(value+"", callback, 1)}>
                <Icon icon={"add"} iconSize={iconSize?iconSize:40} className={"bp3-text-muted"}/>
            </div>
        </div>
    )
};

Counter.protoType = {
    fontSize: PropTypes.string,
    iconSize: PropTypes.number,
    step: PropTypes.number,
    value: PropTypes.string.isRequired,
    minimalValue: PropTypes.number,
    maxValue: PropTypes.number,
    callback: PropTypes.func.isRequired
};

export default Counter