import React from "react";
import PropTypes from 'prop-types'
import style from '../../../css/common/CommonList.module.css'
import cn from 'classnames'

const CommonList = ({items, renderer, keyExtractor, onItemClick,
                        rootClassName, itemClassName, minimal}) => {
    return (
        <div className={cn(minimal ? style.rootMinimal : style.root, rootClassName)}>
            {items.map(e => <div key={keyExtractor(e)}
                                 onClick={onItemClick ? () => {onItemClick(e)} : () => {}}
                                 className={cn(
                                     minimal ? style.itemRootMinimal : style.itemRoot,
                                     itemClassName,
                                     minimal ? "" : "bp3-elevation-1")}
            >
                {renderer(e)}
            </div>)}
        </div>
    )
}

CommonList.propTypes = {
    items: PropTypes.array.isRequired,
    renderer: PropTypes.func.isRequired,
    keyExtractor: PropTypes.func.isRequired,
    onItemClick: PropTypes.func,
    minimal: PropTypes.bool,
    itemClassName: PropTypes.string,
    rootClassName: PropTypes.string,
}

export default CommonList