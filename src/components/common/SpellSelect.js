import React, {useEffect, useState} from "react";
import style from "../../css/common/SpellSelect.module.css"
import cn from "classnames";
import {spellService} from "../../logic/services/SpellService";
import {useBottomScrollListener} from "react-bottom-scroll-listener";
import {InputGroup} from "@blueprintjs/core";
import SpellListItem from "../cards/SpellListItem";

const SpellSelect = ({className, onSpellSelect}) => {
    let [list, setList] = useState([])
    let [page, setPage] = useState(0)

    let [query, setQuery] = useState("")

    useEffect(() => {
        spellService
            .findPage(query, 40, page)
            .then(res => setList(res))
    }, []);
    useEffect(() => {
        page !== 0 && spellService
            .findPage(query, 40, page)
            .then(res => {setList(list.concat(res))})
    }, [page])
    useEffect(() => {
        setPage(0); spellService
            .findPage(query, 40, 0)
            .then(res => {setList(res)})
    }, [query])

    let scrollRef = useBottomScrollListener(() => {setPage(page+1)}, {
        triggerOnNoScroll: false
    })

    return (
        <div className={cn(className, style.root)}>
            <div className={style.controls}>
                <InputGroup value={query} fill
                            autoFocus={true}
                            onChange={e => setQuery(e.target.value)}/>
            </div>
            <div ref={scrollRef} className={style.list}>
                {list.map(spell => <SpellListItem key={spell.id}
                                                  className={style.item}
                                                  spell={spell}
                                                  onClick={() => onSpellSelect && onSpellSelect(spell)}/>)}
            </div>
        </div>
    )
}

export default SpellSelect