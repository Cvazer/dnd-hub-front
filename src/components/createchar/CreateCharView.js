import React, {Fragment, useEffect, useState} from "react";
import {Button, Callout, Card, Divider, Elevation, InputGroup, Intent, Label, PanelStack} from "@blueprintjs/core";
import style from "../../css/createchar/CreateCharView.module.css"
import "../../css/createchar/CreateCharViewFix.css"
import Counter from "../common/Counter";
import {createCharService as service} from '../../logic/services/CreateCharService';
import {MultiSelect, Select} from "@blueprintjs/select";
import {dictValues, textHtml} from "../../App";
import {useNavigate} from "react-router";

export const CreateCharView = () => {
    let [stack, setStack] = useState([
        {
            component: () => <GeneralInfo data={{}} stack={stack} setStack={setStack}/>,
            title: "Общая информация",
        },
    ])

    return (
        <div style={{height: "calc(100vh - 80px)"}}>
            <PanelStack showPanelHeader renderActivePanelOnly={false}
                        className={style.stack} stack={stack} onClose={(p) => {
                            setStack(stack.filter(ps => ps.title !== p.title))
                        }}/>
        </div>
    )
}

const GeneralInfo = ({data, stack, setStack}) => {
    let [lvl, setLvl] = useState(3)
    let [name, setName] = useState("Новый персонаж")
    let [clses, setClses] = useState([])
    let [races, setRaces] = useState([])
    let [bgs, setBgs] = useState([])


    let [cls, setCls] = useState(undefined)
    let [race, setRace] = useState(undefined)
    let [bg, setBg] = useState(undefined)
    let [subCls, setSubCls] = useState(undefined)
    let [subRace, setSubRace] = useState(undefined)

    useEffect(() => {service.fetchBaseCls().then(res => {setClses(res)})}, [])
    useEffect(() => {service.fetchBaseRaces().then(res => {setRaces(res)})}, [])
    useEffect(() => {service.fetchBGs().then(res => {setBgs(res)})}, [])

    let ClsSelectItem = ({item, onClick}) => <div className={style.giClsSelItem} onClick={onClick}>
        {`${item.name} (${item.source.id})`}
    </div>

    return (
        <div className={style.giRoot}>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Уровень</div>
                <Counter value={lvl} callback={setLvl} minimalValue={1} maxValue={20}/>
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Имя</div>
                <InputGroup value={name} onChange={e => setName(e.target.value)}/>
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Класс</div>
                <Select items={clses}
                        popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                        // onActiveItemChange={(item) => setCls(item)}
                        itemPredicate={(query, item) => item.name.toUpperCase().includes(query.toUpperCase())}
                        itemRenderer={(item, props) =>
                            <ClsSelectItem item={item} key={item.id} onClick={props.handleClick}/>
                        } onItemSelect={(item) => setCls(item)}>
                    <Button fill>{cls ? cls.name : "Нет"}</Button>
                </Select>
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Подкласс</div>
                <Select items={cls ? cls.subclasses : []}
                        popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                        // onActiveItemChange={(item) => setSubCls(item)}
                        itemPredicate={(query, item) => item.name.toUpperCase().includes(query.toUpperCase())}
                        itemRenderer={(item, props) =>
                            <ClsSelectItem item={item} key={item.id} onClick={props.handleClick}/>
                        } onItemSelect={(item) => setSubCls(item)}>
                    <Button fill>{subCls ? subCls.name : "Нет"}</Button>
                </Select>
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Раса</div>
                <Select items={races}
                        popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                        // onActiveItemChange={(item) => setRace(item)}
                        itemPredicate={(query, item) => item.name.toUpperCase().includes(query.toUpperCase())}
                        itemRenderer={(item, props) =>
                            <ClsSelectItem item={item} key={item.id} onClick={props.handleClick}/>
                        } onItemSelect={(item) => setRace(item)}>
                    <Button fill>{race ? race.name : "Нет"}</Button>
                </Select>
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Подвид</div>
                <Select items={race ? race.subraces : []}
                        popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                        // onActiveItemChange={(item) => setSubRace(item)}
                        itemPredicate={(query, item) => item.name.toUpperCase().includes(query.toUpperCase())}
                        itemRenderer={(item, props) =>
                            <ClsSelectItem item={item} key={item.id} onClick={props.handleClick}/>
                        } onItemSelect={(item) => setSubRace(item)}>
                    <Button fill>{subRace ? subRace.name : "Нет"}</Button>
                </Select>
            </Label>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Предыстория</div>
                <Select items={bgs}
                        popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                    // onActiveItemChange={(item) => setRace(item)}
                        itemPredicate={(query, item) => item.name.toUpperCase().includes(query.toUpperCase())}
                        itemRenderer={(item, props) =>
                            <ClsSelectItem item={item} key={item.id} onClick={props.handleClick}/>
                        } onItemSelect={(item) => setBg(item)}>
                    <Button fill>{bg ? bg.name : "Нет"}</Button>
                </Select>
            </Label>
            <br/>
            <Button intent={Intent.SUCCESS}
                    minimal large fill
                    rightIcon={"arrow-right"}
                    onClick={() => {
                        service.setGeneralData(
                            data, stack, setStack,
                            cls, subCls, race, subRace,
                            bg, name, lvl, () => setStack(stack.concat({
                                component: () => <FeaturesInfo data={data} stack={stack} setStack={setStack}/>,
                                title: "Спец. информация",
                            }))
                        )
                    }}>
                Далее
            </Button>
        </div>
    )
}

const FeaturesInfo = ({data, stack, setStack}) => {
    let [skills, setSkills] = useState(dictValues("SkillDict"))
    let [langs, setLangs] = useState(dictValues("LangDict"))
    let [weapons, setWeapons] = useState(dictValues("BaseWeaponDict"))
    let [armor, setArmor] = useState(dictValues("BaseArmorDict"))
    let [tools, setTools] = useState([
        ...dictValues("ToolsDict").map(t => {return {...t, type: "", cat: "TOOLS"}}),
        ...dictValues("ArtisanToolsDict").map(t => {return {...t, type: "Artisan", cat: "ARTISAN_TOOLS"}}),
        ...dictValues("GamingSetDict").map(t => {return {...t, type: "Gaming set", cat: "GAMING_SET"}}),
        ...dictValues("MusicalInstrumentDict").map(t => {return {...t, type: "Musical instrument", cat: "MUSICAL_INSTRUMENT"}}),
    ])

    let bgMeta = JSON.parse(data.bg.meta);
    let skillsProf = data.cls.skillsProf;

    let [selectedSkills, setSelectedSkills] = useState([
        ...skills.filter(s => bgMeta.skills.fixed.includes(s.id))
    ])

    let [selectedTools, setSelectedTools] = useState([
        ...tools.filter(t => data.cls.toolsProf.fixed.map(e => e.id).includes(t.id))
    ])

    let [selectedLangs, setSelectedLangs] = useState([
        ...langs.filter(l => bgMeta.languages.fixed.includes(l.id))
    ])

    let [selectedWeapons, setSelectedWeapons] = useState([
        ...weapons.filter(w => data.cls.weaponProf.map(ww => ww.id).includes(w.id))
    ])

    let [selectedArmor, setSelectedArmor] = useState([
        ...armor.filter(w => data.cls.armorProf.map(ww => ww.id).includes(w.id))
    ])

    let SkillSelectItem = ({item, onClick}) => {
        let isCls = skillsProf.list.map(s => s.id).includes(item.id);
        let isBg = bgMeta.skills.fixed.includes(item.id)
            || bgMeta.skills.choice.list && bgMeta.skills.choice.list.includes(item.id);
        let css = style.fiSkillSelItem
        if (isCls) {css = style.fiSkillSelItemCls}
        if (isBg) {css = style.fiSkillSelItemBg}
        if (isBg && isCls) {css = style.fiSkillSelItemBoth}

        return (
            <div className={css} onClick={onClick}>{item.name}</div>
        )
    }

    let ToolsSelectItem = ({item, onClick}) =>
        <div className={style.fiSkillSelItem} onClick={onClick}>
            {item.name}{item.type === "" ? "" : ` (${item.type})`}
        </div>

    let LangSelectItem = ({item, onClick}) =>
        <div className={style.fiSkillSelItem} onClick={onClick}>
            {item.name} ({item.type.toLowerCase().capitalize()})
        </div>

    let GenericSelectItem = ({item, onClick}) =>
        <div className={style.fiSkillSelItem} onClick={onClick}>{item.name}</div>

    let skillTagProps = (tag) => {
        let isCls = skillsProf.list.map(s => s.id).includes(tag.key)
        let isBg = bgMeta.skills.fixed.includes(tag.key)
            || bgMeta.skills.choice.list && bgMeta.skills.choice.list.includes(tag.key);
        return {
            minimal: !isCls && !isBg,
            intent: isBg && isCls ? Intent.WARNING : isBg ? Intent.PRIMARY : isCls ? Intent.SUCCESS : Intent.NONE
        }
    }

    let toolsTagProps = (tag) => {return {minimal: true}}

    let SkillTagItem = ({item}) => <div>{item.name}</div>

    let traits = data.race.traits
        .concat([data.bg.trait])
        .concat(data.cls.traits.filter(t => t.lvl <= data.lvl).map(t => t.trait) || [])
        .concat(data.subRace ? data.subRace.traits : [])
        .concat(data.subCls ? data.subCls.traits.filter(t => t.lvl <= data.lvl).map(t => t.trait) : [])

    return (
        <div className={style.fiRoot}>
            <Callout title={"Владения, навыки и способности"} intent={Intent.PRIMARY}>
                Персонаж получает определенный набор владений, навыков и способностей от класса, расы и предыстории.
                В определенных случаях игрок может выбрать из списка вариантов некоторое количество владений.
            </Callout>
            <br/>
            <Callout>
                В списке ниже представлены навыки, которые может получить персонаж.
                Туда уже добавлены все навыки, которые игроку не нужно выбирать.
                Чаще всего от класса персонажу дается на выбор несколько навыков из ограниченного списка.
                {/*<i> Выберите <b>{skillsProf.count}</b> отмеченных зеленым цветом навыка из списка.</i>*/}
                <br/>
                <br/>
                Может возникнуть ситуация, когда один и тот же навык дается сразу из нескольких источников
                (на выбор или статически). Например навык "Магия" может даваться на выбор из класса "Wizard"
                и статически из предыстории "Sage". В таких случаях система автоматически добавит этот навык в
                список как выбраный навык от класса. Несмотря на то, что этот навык будет считаться
                выбранным, он не должен учитываться в вашем счетчике выбранных навыков от класса.
                Если такая ситуация возникла, просто выберите еще один навык от класса.
            </Callout>
            <br/>
            <Callout intent={Intent.SUCCESS}>
                Навыки на выбор от класса отмечены <i>зеленым</i> цветом. Навыки на выбор от
                предыстории отмечены <i>синим</i> цветом. Навыки на выбор,
                попадающие и в тот и в другой списки, отмечены <i>оранжевым</i> цветом.
                <br/>
                <br/>
                Вдобавок к уже добавленным навыкам:
                <br/>
                Нужно выбрать <b>{skillsProf.count}</b> навыка от класса.
                {bgMeta.skills.choice.list && <span>
                    <br/>
                    Нужно выбрать <b>{bgMeta.skills.choice.count}</b> навыка от предыстории.
                </span>}
            </Callout>
            <br/>
            <Label style={{marginBottom: "2px"}}>
                <div className={"bp3-text-muted bp3-text-small"}>Список навыков</div>
            </Label>
            <MultiSelect fill
                         popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                         selectedItems={selectedSkills}
                         itemRenderer={(i, p) => <SkillSelectItem key={i.id} item={i} onClick={p.handleClick}/>}
                         onItemSelect={i => {setSelectedSkills([...selectedSkills, i])}}
                         items={skills.filter(s => !selectedSkills.map(ss => ss.id).includes(s.id))}
                         itemPredicate={(q, i) => i.name.toUpperCase().includes(q.toUpperCase())}
                         tagInputProps={{
                             onRemove: v => {setSelectedSkills(selectedSkills.filter(s => s.id !== v.key))},
                             tagProps: t => {return skillTagProps(t)}
                         }}
                         resetOnSelect={true}
                         tagRenderer={(t) => <SkillTagItem key={t.id} item={t}/>}
            />
            <br/>
            <Callout>
                Иногда определенные способности от рассы, класа или предыстории дают владения теми или иными навыками.
                Если такие способности дают владение навыком, это будет отображено в их описании.
                Такие ситуации сложно обработать системой автоматически.
                <i> Внимательно просмотрите все способности и добавьте все недостающие навыки в список.</i>
            </Callout>
            <br/>
            <Label style={{marginBottom: "2px"}}>
                <div className={"bp3-text-muted bp3-text-small"}>Способности</div>
            </Label>
            <Card elevation={Elevation.TWO}>
                <div className={style.traitsRoot}>
                    {traits.map((trait, index) => <div className={style.traitsCard} key={trait.id}>
                        <h3>{trait.name}</h3>
                        <div dangerouslySetInnerHTML={{__html: textHtml(trait.descr)}}/>
                        {index !== traits.length-1 && <Divider/>}
                    </div>)}
                </div>
            </Card>
            <br/>
            <br/>
            {data.cls.toolsProf.choicePrompt && <Fragment>
                <Callout>{data.cls.toolsProf.choicePrompt}</Callout>
                <br/>
            </Fragment>}
            <Label style={{marginBottom: "2px"}}>
                <div className={"bp3-text-muted bp3-text-small"}>Список инструментов</div>
            </Label>
            <MultiSelect fill
                         popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                         selectedItems={selectedTools}
                         itemRenderer={(i, p) => <ToolsSelectItem key={i.id} item={i} onClick={p.handleClick}/>}
                         onItemSelect={i => {setSelectedTools([...selectedTools, i])}}
                         items={tools.filter(s => !selectedTools.map(ss => ss.id).includes(s.id))}
                         itemPredicate={(q, i) => (i.name.toUpperCase()+i.type.toUpperCase()).includes(q.toUpperCase())}
                         tagInputProps={{
                             onRemove: v => {setSelectedTools(selectedTools.filter(s => s.id !== v.key))},
                             tagProps: t => {return toolsTagProps(t)}
                         }}
                         resetOnSelect={true}
                         tagRenderer={(t) => <SkillTagItem key={t.id} item={t}/>}
            />
            <br/>
            <Callout>
                Изучите описание расы и способности персонажа. Определите языки, которыми он владеет.
                <i> Добавьте их в список.</i>
            </Callout>
            <br/>
            <Card elevation={Elevation.TWO}>
                <div className={style.traitsRoot}>
                    <div dangerouslySetInnerHTML={{__html: textHtml(data.race.descr)}}/>
                    {data.subRace && <div dangerouslySetInnerHTML={{__html: textHtml(data.subRace.descr)}}/>}
                </div>
            </Card>
            <br/>
            <Label style={{marginBottom: "2px"}}>
                <div className={"bp3-text-muted bp3-text-small"}>Список языков</div>
            </Label>
            <MultiSelect fill
                         popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                         selectedItems={selectedLangs}
                         itemRenderer={(i, p) => <LangSelectItem key={i.id} item={i} onClick={p.handleClick}/>}
                         onItemSelect={i => {setSelectedLangs([...selectedLangs, i])}}
                         items={langs.filter(s => !selectedLangs.map(ss => ss.id).includes(s.id))}
                         itemPredicate={(q, i) => i.name.toUpperCase().includes(q.toUpperCase())}
                         tagInputProps={{
                             onRemove: v => {setSelectedLangs(selectedLangs.filter(s => s.id !== v.key))},
                             tagProps: t => {return toolsTagProps(t)}
                         }}
                         resetOnSelect={true}
                         tagRenderer={(t) => <SkillTagItem key={t.id} item={t}/>}
            />
            <br/>
            <Callout>
                Изучите описание предыстории и способности персонажа. Определите языки, которыми он владеет.
                <i> Добавьте их в список.</i>
            </Callout>
            <br/>
            <Card elevation={Elevation.TWO}>
                <div className={style.traitsRoot}>
                    <div dangerouslySetInnerHTML={{__html: textHtml(data.bg.descr)}}/>
                </div>
            </Card>
            <br/>
            <Label style={{marginBottom: "2px"}}>
                <div className={"bp3-text-muted bp3-text-small"}>Список языков</div>
            </Label>
            <MultiSelect fill
                         popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                         selectedItems={selectedLangs}
                         itemRenderer={(i, p) => <LangSelectItem key={i.id} item={i} onClick={p.handleClick}/>}
                         onItemSelect={i => {setSelectedLangs([...selectedLangs, i])}}
                         items={langs.filter(s => !selectedLangs.map(ss => ss.id).includes(s.id))}
                         itemPredicate={(q, i) => i.name.toUpperCase().includes(q.toUpperCase())}
                         tagInputProps={{
                             onRemove: v => {setSelectedLangs(selectedLangs.filter(s => s.id !== v.key))},
                             tagProps: t => {return toolsTagProps(t)}
                         }}
                         resetOnSelect={true}
                         tagRenderer={(t) => <SkillTagItem key={t.id} item={t}/>}
            />
            <br/>
            <Callout>
                Изучите описания расы, предыстории и способностей персонажа. Определите владения оружием и броней.
                <i> Добавьте их в соответствующие списки.</i>
            </Callout>
            <br/>
            <Label style={{marginBottom: "2px"}}>
                <div className={"bp3-text-muted bp3-text-small"}>Список оружия</div>
            </Label>
            <MultiSelect fill
                         popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                         selectedItems={selectedWeapons}
                         itemRenderer={(i, p) => <GenericSelectItem key={i.id} item={i} onClick={p.handleClick}/>}
                         onItemSelect={i => {setSelectedWeapons([...selectedWeapons, i])}}
                         items={weapons.filter(s => !selectedWeapons.map(ss => ss.id).includes(s.id))}
                         itemPredicate={(q, i) => i.name.toUpperCase().includes(q.toUpperCase())}
                         tagInputProps={{
                             onRemove: v => {setSelectedWeapons(selectedWeapons.filter(s => s.id !== v.key))},
                             tagProps: t => {return toolsTagProps(t)}
                         }}
                         resetOnSelect={true}
                         tagRenderer={(t) => <SkillTagItem key={t.id} item={t}/>}
            />
            <br/>
            <Label style={{marginBottom: "2px"}}>
                <div className={"bp3-text-muted bp3-text-small"}>Список брони</div>
            </Label>
            <MultiSelect fill
                         popoverProps={{popoverClassName: "giClsSelectPopFix"}}
                         selectedItems={selectedArmor}
                         itemRenderer={(i, p) => <GenericSelectItem key={i.id} item={i} onClick={p.handleClick}/>}
                         onItemSelect={i => {setSelectedArmor([...selectedArmor, i])}}
                         items={armor.filter(s => !selectedArmor.map(ss => ss.id).includes(s.id))}
                         itemPredicate={(q, i) => i.name.toUpperCase().includes(q.toUpperCase())}
                         tagInputProps={{
                             onRemove: v => {setSelectedArmor(selectedArmor.filter(s => s.id !== v.key))},
                             tagProps: t => {return toolsTagProps(t)}
                         }}
                         resetOnSelect={true}
                         tagRenderer={(t) => <SkillTagItem key={t.id} item={t}/>}
            />
            <br/>
            <Button intent={Intent.SUCCESS}
                    minimal large fill
                    rightIcon={"arrow-right"}
                    onClick={() => {
                        service.setFeaturesData(
                            data, stack, setStack,
                            selectedSkills, selectedWeapons,
                            selectedLangs, selectedTools, selectedArmor,
                            () => setStack(stack.concat({
                                component: () => <MiscInfo data={data} stack={stack} setStack={setStack}/>,
                                title: "Доп. информация",
                            }))
                        )
                    }}>
                Далее
            </Button>
            <br/>
        </div>
    )
}

const MiscInfo = ({data, stack, setStack}) => {
    let [str, setStr] = useState(10)
    let [dex, setDex] = useState(10)
    let [con, setCon] = useState(10)
    let [int, setInt] = useState(10)
    let [wis, setWis] = useState(10)
    let [cha, setCha] = useState(10)

    let [hpMax, setHpMax] = useState(0)

    let navigate = useNavigate()

    return (
        <div className={style.giRoot}>
            <Callout intent={Intent.PRIMARY}>
                Установите значения характеристик персонажа.
            </Callout>
            <br/>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Сила</div>
                <Counter value={str} callback={setStr} minimalValue={1} maxValue={20}/>
            </Label>
            <br/>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Ловкость</div>
                <Counter value={dex} callback={setDex} minimalValue={1} maxValue={20}/>
            </Label>
            <br/>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Телосложение</div>
                <Counter value={con} callback={setCon} minimalValue={1} maxValue={20}/>
            </Label>
            <br/>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Интелект</div>
                <Counter value={int} callback={setInt} minimalValue={1} maxValue={20}/>
            </Label>
            <br/>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Мудрость</div>
                <Counter value={wis} callback={setWis} minimalValue={1} maxValue={20}/>
            </Label>
            <br/>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Харизма</div>
                <Counter value={cha} callback={setCha} minimalValue={1} maxValue={20}/>
            </Label>
            <br/>
            <Callout intent={Intent.PRIMARY}>
                Установите значения хитов.
            </Callout>
            <br/>
            <Label>
                <div className={"bp3-text-muted bp3-text-small"}>Максимальные хиты</div>
                <Counter value={hpMax} callback={setHpMax} minimalValue={0}/>
            </Label>
            <Button intent={Intent.SUCCESS}
                    minimal large fill
                    rightIcon={"tick"}
                    onClick={() => {
                        service.setMiscData(
                            str, dex, con, int, wis, cha, data, hpMax,
                            () => {
                                service.send(data)
                                navigate("/overview/", {replace: false})
                            }
                        )
                    }}>
                Готово
            </Button>
        </div>
    )
}
