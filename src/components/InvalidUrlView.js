import React from 'react';
import {Button, Icon, Spinner} from "@blueprintjs/core";
import style from "../css/LoadingView.module.css"

const InvalidUrlView = () => <div className={style.root}>
    <div className={style.container}>
        <div>
            <Icon icon={"error"} iconSize={64} className={"bp3-text-muted"}/>
            <h2>Что-то пошло не так</h2>
            <p>Такой страницы нет, либо она в данный момент недоступна. Возможно отвалился интернет
                или параметры адреса битые</p>
        </div>
    </div>
</div>;

export default InvalidUrlView