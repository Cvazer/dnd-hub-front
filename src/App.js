import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/popover2/lib/css/blueprint-popover2.css";
import "@blueprintjs/table/lib/css/table.css";
import 'normalize.css';

import React, {Fragment, useEffect, useState} from 'react';
import DrawerContainer from "./components/header/DrawerContainer";
import HeaderContainer from "./components/header/HeaderContainer";
import {useSelector} from "react-redux";
import InitialView from "./components/InitialView";
import {setConfiguration} from 'react-grid-system';
import {Toaster} from "@blueprintjs/core";
import {Position} from "@blueprintjs/core/lib/cjs/common/position";
import {Navigate, Route, Routes} from "react-router";
import {marked} from 'marked'
import './css/index.css'
import accountService from './logic/services/AccountService'
import LoadingView from "./components/LoadingView";
import Overview from "./components/overview/Overview";
import AdminItemsView from "./components/admin/AdminItemsView";
import {appService as service} from './logic/services/AppService'
import {CreateCharView} from "./components/createchar/CreateCharView";
import ItemOverview from "./components/overview/items/ItemOverview";
import TraitView from "./components/overview/traits/TraitView";
import style from './css/App.module.css'
import SettingsView from "./components/settings/SettingsView";
import AdminView from "./components/admin/AdminView";
import ws from "./logic/websocket/WSService";
import {dispatch} from "./store/ConfigureStore";
import {AccountReducerAction} from "./store/reducers/AccountReducer";
import CampaignSelectView from "./components/campaign/CampaignSelectView";
import CampaignView from "./components/campaign/CampaignView";
import AdminCampaignsView from "./components/admin/AdminCampaignsView";
import AdminAccountsView from "./components/admin/AdminAccountsView";
import GlossaryView from "./components/campaign/GlossaryView";
import "core-js/stable";
import "regenerator-runtime/runtime";
import FlowLanding from "./components/flow/FlowLanding";
import AdminScrappingView from "./components/admin/scrapping/AdminScrappingView";

setConfiguration({ defaultScreenClass: 'xs', gridColumns: 12 });

export const AppToaster = Toaster.create({
    className: "recipe-toaster",
    position: Position.TOP,
});

let accountEventSub;

const App = () => {
    const [isOpen, setIsOpen] = useState(false);
    const key = useSelector(store => store.account.key);
    const spacer = useSelector(store => store.account.spacer);
    const [isLoadingKey, setIsLoadingKey] = useState(true);

    useEffect(() => {
        !key && accountService
            .setAccount(localStorage.getItem("dnd-hub:key"))
            .then(() => setIsLoadingKey(false), () => setIsLoadingKey(false))
        if (!localStorage.getItem("dnd-hub:spacer")) localStorage.setItem("dnd-hub:spacer", spacer)
        if (localStorage.getItem("dnd-hub:spacer") !== spacer)
            dispatch({
                type: AccountReducerAction.SpacerSet,
                payload: +localStorage.getItem("dnd-hub:spacer")
            })
    }, [])

    useEffect(() => {
        ws.connect(localStorage.getItem("dnd-hub:key"), frame => setTimeout(() => {
            accountEventSub = ws.subscribe("/topic/accountEvents/"+key, frame => {
                console.log(frame)
            })
        }, 500))
    }, [key])
    useEffect(() => {service.loadDict()})

    if (isLoadingKey) return <LoadingView/>

    return key
        ? <Fragment>
            <DrawerContainer isOpen={isOpen} setIsOpen={setIsOpen}/>
            <HeaderContainer setIsOpen={setIsOpen}/>
            <div className={style.outer}>
                <div style={spacer>0?{maxWidth: spacer+"px"}:{}} className={style.fixer}>
                    <Routes>
                        <Route exact path="/" element={<Navigate replace to="/overview"/>}/>
                        <Route path="/overview/item/:id" element={<ItemOverview/>}/>
                        <Route path="/overview/trait/:id" element={<TraitView/>}/>
                        <Route path="/overview/*" element={<Overview/>}/>
                        <Route exact path="/campaign" element={<CampaignSelectView/>}/>
                        <Route exact path="/campaign/:id" element={<CampaignView/>}/>
                        <Route path="/campaign/:id/glossary" element={<GlossaryView/>}/>
                        <Route path="/settings" element={<SettingsView/>}/>
                        <Route path="/create-char" element={<CreateCharView/>}/>
                        <Route path="/flow/:flowId" element={<FlowLanding/>}/>
                        <Route path="*" element={<div/>}/>
                    </Routes>
                </div>
            </div>
            <Routes>
                <Route path="/admin/items" element={<AdminItemsView/>}/>
                <Route path="/admin/campaigns" element={<AdminCampaignsView/>}/>
                <Route path="/admin/accounts" element={<AdminAccountsView/>}/>
                <Route path="/admin/scrapping" element={<AdminScrappingView/>}/>
                <Route path="/admin" element={<AdminView/>}/>
                <Route path="*" element={<div/>}/>
            </Routes>
          </Fragment>
        : <InitialView/>
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}



export const dict = (name) => { return service.dict(name) }
export const dictValues = (name) => { return service.dictValues(name) }
export const text = (text) => { return service.text(text) }
export const textHtml = (t) => {return marked(text(t)) }

export default App;