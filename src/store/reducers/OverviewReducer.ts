import {ReducerAction} from "./RootReducer";
import {CharModel} from "../../logic/dao/CharModel";

interface OverviewState {
    char?: CharModel
    items?: any
    notes?: any
}
export enum OverviewReducerAction {
    CharOverviewSet = 'overview/char/set',
    CharOverviewSetField = 'overview/char/set/field',
    TraitsOverviewSet = 'overview/traits/set',
    ProfsOverviewSet = 'overview/profs/set',
    ItemsOverviewSet = 'overview/items/set',
    ItemsOverviewSetItem = 'overview/items/set/count',
    NotesSet = 'overview/notes/set',
}

//REDUCER
export const overviewReducer = (state: OverviewState = {}, action: ReducerAction<OverviewReducerAction, any>) => {
    if (action.type === OverviewReducerAction.CharOverviewSet) {
        let payload = action.payload as CharModel
        return {...state, char: payload}
    }
    if (action.type === OverviewReducerAction.CharOverviewSetField) {
        let payload = action.payload as SetFieldPayload
        return {...state, char: {...state.char, [payload.name]: payload.value}}
    }
    if (action.type === OverviewReducerAction.ItemsOverviewSet) {
        return {...state, items: action.payload}
    }
    if (action.type === OverviewReducerAction.TraitsOverviewSet) {
        return {...state, traits: action.payload}
    }
    if (action.type === OverviewReducerAction.ProfsOverviewSet) {
        return {...state, profs: action.payload}
    }
    if (action.type === OverviewReducerAction.NotesSet) {
        return {...state, notes: action.payload}
    }
    if (action.type === OverviewReducerAction.ItemsOverviewSetItem) {
        let payload = action.payload as SetItemCount
        return {...state, items: state.items.map((inv: any) => {
                if (inv.id !== payload.invId) return inv;
                return {...inv, items: inv.items.map((item: any) => {
                        if (item.id !== payload.itemId) return item
                        return {...item, count: payload.count}
                    })}
            })}
    }
    return state
}

export class SetItemCount {
    invId: number;
    itemId: number;
    count: number

    constructor(invId: number, itemId: number, count: number) {
        this.invId = invId;
        this.itemId = itemId;
        this.count = count;
    }
}

export class SetFieldPayload {
    name: string;
    value: any;

    constructor(name: string, value: any) {
        this.name = name;
        this.value = value;
    }
}