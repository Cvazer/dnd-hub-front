import {ReducerAction} from "./RootReducer";

const initial = {
    "overview/hp": { open: false },
    "overview/ac": { open: false },
    "overview/init": { open: false },
    "overview/speed": { open: false },
    "overview/exp": { open: false },
    "overview/money": { open: false },
    "overview/hd": { open: false, context: undefined },
    "overview/attack": { open: false, context: undefined },
    "overview/stat": { open: false, context: undefined },
    "overview/note/view": { open: false, context: undefined },
    "overview/note/edit": { open: false, context: undefined },
    "overview/counter/edit": { open: false, context: undefined },
    "inventory/item/change": { open: false, context: undefined },
    "inventory/item/add": { open: false, context: undefined },
    "inventory/item/move": { open: false, context: undefined },
    "traits/filter": { open: false, context: undefined },
    "traits/feature/add": { open: false, context: undefined },
    "campaign/log/record/change": { open: false, context: undefined },
    "prof/add": { open: false, context: undefined },
    // money: {open: false},
    // action: {open: false, action: undefined},
    // stat: {open: false, stat: undefined},
    // die: {open: false, die: undefined}
}

export enum DialogReducerAction {
    SetDialogState = "dialog/state/set"
}

export function dialogReducer(state: any = initial, action: ReducerAction<DialogReducerAction, any>) {
    if (action.type === DialogReducerAction.SetDialogState){
        let payload = action.payload as SetStatePayload
        return {...state, [payload.dialog]: payload.state}
    }
    return state;
}

export class SetStatePayload {
    dialog: string;
    state: any;

    constructor(dialog: string, state: any) {
        this.dialog = dialog;
        this.state = state;
    }
}