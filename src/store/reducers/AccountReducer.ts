import {ReducerAction} from "./RootReducer";
import Origin from "../../logic/common/Origin";

export interface TraitsFilterState {
    origin: Origin,
    showHidden: boolean
}

export interface AccountReducerState {
    traitsFilter: TraitsFilterState,
    key?: string,
    spacer: number,
}

export const initialState: AccountReducerState = {
    traitsFilter: {
        origin: { id: "ALL", name: "Все" },
        showHidden: false
    },
    spacer: 480
}

export enum AccountReducerAction {
    AccountSet = 'account/set',
    SpacerSet = 'spacer/set',
    TraitsFilterSet = 'filter/traits/set',
}

//REDUCER
export const accountReducer = (state = initialState, action: ReducerAction<AccountReducerAction, any>) => {
    if (action.type === AccountReducerAction.AccountSet){
        let payload = action.payload as AccountSetPayload
        return { ...state, key: payload.key, name: payload.name, role: payload.role }
    }
    if (action.type === AccountReducerAction.TraitsFilterSet){
        return { ...state, traitsFilter: action.payload }
    }
    if (action.type === AccountReducerAction.SpacerSet){
        return { ...state, spacer: action.payload }
    }
    return state;
}

const accountReducerActionFactory = {
    accountSet: (key: string, name: string, role: string): ReducerAction<AccountReducerAction, AccountSetPayload> => {
        return {
            type: AccountReducerAction.AccountSet,
            payload: {
                key: key,
                name: name,
                role: role
            }
        }
    }
}

//PAYLOADS
export interface AccountSetPayload {
    key: string;
    name: string;
    role: string;
}

export default accountReducerActionFactory
