import {ReducerAction} from "./RootReducer";

export const initialState = {};
export enum DictReducerAction {
    DictSetAll = 'dict/set/all'
}

//REDUCER
export const dictReducer = (state = initialState, action: ReducerAction<DictReducerAction, any>) => {
    if (action.type === DictReducerAction.DictSetAll){
        let payload = action.payload as DictSetAllPayload
        return { ...payload.dict }
    }
    return state;
}

//PAYLOADS
export class DictSetAllPayload {
    dict: any

    constructor(dict: any) {
        this.dict = dict;
    }
}