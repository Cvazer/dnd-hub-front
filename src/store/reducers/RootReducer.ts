import {combineReducers} from 'redux'
import {overviewReducer} from "./OverviewReducer";
import {accountReducer} from "./AccountReducer";
import {dialogReducer} from "./DialogReducer";
import {dictReducer} from "./DictReducer";
import {campaignReducer} from "./CampaignReducer";

export interface ReducerAction<ActionType, PayloadType> {
    type: ActionType
    payload: PayloadType
}

export const rootReducer = combineReducers({
    account: accountReducer,
    overview: overviewReducer,
    dialog: dialogReducer,
    dict: dictReducer,
    campaign: campaignReducer
});
