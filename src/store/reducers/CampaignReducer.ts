import {ReducerAction} from "./RootReducer";
import ICUDateTime from "../../model/ICUDateTime";

export interface Campaign {
    id: number,
    name: string,
    currentDateTime: ICUDateTime
}

interface CampaignReducerState {
    campaign?: number
    activeCharId?: number
    log: Array<any>
    role: string
}

export enum CampaignReducerAction {
    ActiveCharIdSet = 'campaign/activeCharId/set',
    RoleSet = 'campaign/role/set',
    LogSet = 'campaign/log/set',
    LogRecordUpdate= 'campaign/log/record/update',
    LogAppend = 'campaign/log/append',
    LogTail = 'campaign/log/tail',
    CampaignSet = 'campaign/set'
}

export const initialState: CampaignReducerState = {
    role: "player",
    log: []
}

//REDUCER
export const campaignReducer = (state: CampaignReducerState = initialState,
                                action: ReducerAction<CampaignReducerAction, any>) => {
    if (action.type === CampaignReducerAction.ActiveCharIdSet) {return {...state, activeCharId: action.payload}}
    if (action.type === CampaignReducerAction.CampaignSet) {return {...state, campaign: action.payload}}
    if (action.type === CampaignReducerAction.RoleSet) {return {...state, role: action.payload}}
    if (action.type === CampaignReducerAction.LogSet) {return {...state, log: action.payload}}
    if (action.type === CampaignReducerAction.LogAppend) {return {...state, log: action.payload.concat(state.log)}}
    if (action.type === CampaignReducerAction.LogTail) {return {...state, log: state.log.concat(action.payload)}}
    if (action.type === CampaignReducerAction.LogRecordUpdate)
    {return {...state, log: state.log.map((e: any) => e.id === action.payload.id ? action.payload : e)}}
    return state
}

const campaignReducerActionsFactory = {
    setCampaign: (campaign?: Campaign): ReducerAction<CampaignReducerAction, any> => { return {
            type: CampaignReducerAction.CampaignSet,
            payload: campaign
    } },
    setActiveCharId: (id?: number): ReducerAction<CampaignReducerAction, any> => { return {
        type: CampaignReducerAction.ActiveCharIdSet,
        payload: id
    } },
    setRole: (role: string): ReducerAction<CampaignReducerAction, string> => { return {
        type: CampaignReducerAction.RoleSet,
        payload: role
    } },
    setLog: (log: Array<any>): ReducerAction<CampaignReducerAction, Array<any>> => { return {
        type: CampaignReducerAction.LogSet,
        payload: log
    } },
    appendLog: (rec: Array<any>): ReducerAction<CampaignReducerAction, any> => { return {
        type: CampaignReducerAction.LogAppend,
        payload: rec
    } },
    tailLog: (rec: Array<any>): ReducerAction<CampaignReducerAction, any> => { return {
        type: CampaignReducerAction.LogTail,
        payload: rec
    } },
    update: (rec: any): ReducerAction<CampaignReducerAction, any> => { return {
        type: CampaignReducerAction.LogRecordUpdate,
        payload: rec
    } }
}

export default campaignReducerActionsFactory